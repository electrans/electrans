# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from trytond.model import ModelSQL

from . import address
from . import asset
from . import purchase
from . import purchase_amendment
from . import purchase_request
from . import quality
from . import party
from . import product
from . import production
from . import stock
from . import res
from . import nonconformity
from . import label
from . import log
from . import inventory
from . import ir
from . import attachment
from . import work
from . import company


def register():
    Pool.register(
        address.Address,
        address.Sale,
        asset.Asset,
        attachment.Attachment,
        company.Location,
        company.Company,
        inventory.CreateInventoriesStart,
        inventory.Inventory,
        inventory.InventoryLine,
        ir.SequenceType,
        ir.Sequence,
        label.CustomLabelCreateStart,
        nonconformity.Nonconformity,
        nonconformity.NonconformityLine,
        nonconformity.ShipmentInReturn,
        party.Party,
        party.PartyAlternativeReport,
        product.Category,
        product.PriceListAmountStart,
        product.PriceListLine,
        product.Product,
        product.ProductConfiguration,
        product.ProductQualityTemplate,
        product.ProductTemplateQualityTemplate,
        product.ProductTemplateSpecifications,
        product.Template,
        production.BOM,
        production.BOMInput,
        production.OpenBOMTreeStart,
        production.Operation,
        production.OperationWorkCenterChangeStart,
        production.Production,
        production.ProductionElectransInternalStart,
        production.ProductionElectransModifyFromLocationStart,
        production.ProductionResetWizardStart,
        production.ProductionSerialNumberSplitStart,
        production.Route,
        production.OneStepDoProductionStart,
        production.ProductionInternalShipment,
        purchase.ApprovalRequest,
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.PurchaseRequisition,
        purchase.PurchaseRequisitionLine,
        purchase_amendment.Amendment,
        purchase_amendment.AmendmentLine,
        purchase_amendment.PurchaseLine,
        purchase_request.AddToPurchaseAskPurchase,
        purchase_request.AddToPurchaseQuotationAskQuotation,
        purchase_request.PurchaseRequest,
        purchase_request.Quotation,
        purchase_request.QuotationLine,
        purchase_request.SplitPurchaseRequestStart,
        quality.QualitativeTemplateLine,
        quality.QuantitativeTemplateLine,
        quality.TemplateLine,
        quality.TestLine,
        quality.Configuration,
        res.Role,
        res.User,
        stock.AddComponentBreakdownStart,
        stock.Configuration,
        stock.ConfigurationLocation,
        stock.FillToLocationStart,
        stock.LocateProductsStart,
        stock.Location,
        stock.LocationLeadTime,
        stock.Lot,
        stock.LotTrace,
        stock.ModifyToLocationStart,
        stock.ShipmentIn,
        stock.ShipmentInReturn,
        stock.ShipmentInternal,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        stock.SplitMoveStart,
        stock.StockMove,
        work.ProjectExpense,
        module='electrans', type_='model')
    Pool.register(
        label.CustomLabel,
        product.PriceListAmount,
        production.BomExcel,
        production.OneStepDoProduction,
        production.OpenBOMTree,
        production.OpenElectransShipmentInternal,
        production.OperationWorkCenterChange,
        production.ProductionElectransInternal,
        production.ProductionElectransModifyFromLocation,
        production.ProductionSerialNumberSplit,
        production.ProductionResetWizard,
        purchase_request.AddToPurchase,
        purchase_request.AddToPurchaseQuotation,
        purchase_request.CreatePurchase,
        purchase_request.SplitPurchaseRequest,
        purchase_request.CreatePurchaseRequestQuotation,
        purchase_request.PurchaseRequestQuotationGenerateLinePositions,
        stock.AddComponentBreakdown,
        stock.FillLocationProducts,
        stock.FillToLocation,
        stock.LocateProducts,
        stock.ModifyToLocation,
        stock.SplitMove,
        stock.SplitShipment,
        stock.ShipmentOutMerge,
        purchase.HandleInvoiceException,
        purchase.HandleShipmentException,
        production.SplitProduction,
        module='electrans', type_='wizard')
    Pool.register(
        inventory.BlindCountReport,
        inventory.InventoryLotLabels,
        inventory.InventoryProductLabels,
        label.CustomLabelReport,
        label.LotLabel,
        label.ProductClientLabel,
        label.ProductLabel,
        label.ShipmentPackageReport,
        production.BOMReport,
        production.BomExcelReport,
        production.ProductionLabelReport,
        production.ProductionLocationReport,
        production.ProductionProductReport,
        production.ProductionReport,
        production.ProductionXlsReport,
        quality.QualityTemplateInternalReport,
        quality.QualityTemplateReport,
        quality.QualityTestInternalReport,
        quality.QualityTestReport,
        stock.ShipmentInternalReport,
        stock.ShipmentInternalReportOrderByLocation,
        stock.ShipmentInternalReportOrderByProduct,
        stock.ShipmentInternalReportOutgoingMovesOrderByProduct,
        module='electrans', type_='report')
    Pool.register_mixin(
        log.LogMixin, ModelSQL, module='electrans')
