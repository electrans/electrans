# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['Address', 'Sale']


class Address(metaclass=PoolMeta):
    __name__ = 'party.address'
    contact = fields.Char('Contact')
    full_address_with_party = fields.Function(fields.Text('Full Address with Party'),
        'get_full_address_with_party')

    def get_full_address_with_party(self, name):
        with Transaction().set_context(address_with_party=True):
            pool = Pool()
            Address = pool.get('party.address')
            if self:
                self = Address(self.id)
            return self.full_address


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    address_with_attn = fields.Function(fields.Text('Full Invoice Address with Attention'),
        'get_address_with_attn')

    def get_address_with_attn(self, name):
        with Transaction().set_context(address_attention_party=self.invoice_contact):
            pool = Pool()
            Address = pool.get('party.address')
            if self.address:
                self.address = Address(self.address.id)
                return self.address.full_address
