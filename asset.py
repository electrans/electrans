# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

__all__ = ['Asset']


class Asset(metaclass=PoolMeta):
    __name__ = 'asset'

    lot = fields.Many2One('stock.lot', 'Lot',
            states = {
                'readonly': True,
            },
            domain = [
                ('product', '=', Eval('product')),
            ],
            depends=['product'],
            help='Serial number (lot) used for movements')
    party_number = fields.Char('Party Number', required=False,
        help='Number given to the item by party')
    location = fields.Function(fields.Char('Location'),
        'get_location')

    def get_location(self, name):
        Move = Pool().get('stock.move')
        moves = Move.search(['AND',
                ('product', '=', self.product),
                ('lot', '=', self.code)
            ], order=[('effective_date', 'DESC')], limit=1)
        if moves:
            move, = moves
            if move.to_location is not None:
                if move.to_location.warehouse is not None:
                    PartyProductionWarehouse = Pool().get(
                        'party.party.production_warehouse')
                    parties_production = PartyProductionWarehouse.search(
                        [('production_warehouse', '=', move.to_location.warehouse.id)],
                        limit=1)
                    if parties_production:
                        record, = parties_production
                        return record.party.name if record.party else None
        return

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        tool = (
            'tool',
            'Tool')
        if tool not in cls.type.selection:
            cls.type.selection.append(tool)

        # Currently it isn't allowed to inventory an asset so products must be goods instead of assets :(
        # in order to set/get the location of the asset
        #cls.product.domain = [('type', '=', 'assets')]

    @classmethod
    def create(cls, vlist):
        Lot = Pool().get('stock.lot')
        Product = Pool().get('product.product')

        newlot = Lot()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('lot'):
                newlot.product = Product(values.get('product'))
                newlot.save()
                values['lot'] = newlot.id
        assets = super(Asset, cls).create(vlist)
        for asset in assets:
            newlot.number = asset.code
            newlot.save()
        return assets

    def get_code_readonly(self, name):
        return False
