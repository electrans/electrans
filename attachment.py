# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Attachment(metaclass=PoolMeta):
    __name__ = 'ir.attachment'

    # this field is used to know if the attachment is the main one of the model
    main = fields.Boolean('Main')

    @classmethod
    def __setup__(cls):
        super(Attachment, cls).__setup__()

    @classmethod
    def delete(cls, attachments):
        atts = [att for att in attachments if str(att.resource)[:15] == 'account.invoice' and att.main]
        if atts:
            for att in atts:
                if att.resource.state != 'draft':
                    raise UserError(gettext('electrans.delete_cancel'))
        super(Attachment, cls).delete(attachments)
