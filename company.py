# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Location', 'Company']


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    company = fields.Many2One('company.company', 'Company')


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    warehouses = fields.One2Many('stock.location', 'company', 'Warehouses',
                                 add_remove=[('type', '=', 'warehouse')]
                                 )
    