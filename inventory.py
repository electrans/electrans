# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.i18n import gettext
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.pool import Pool, PoolMeta
from trytond.model import fields, ModelView
from trytond.transaction import Transaction
from trytond.model import Workflow
from trytond.model.exceptions import ValidationError

__all__ = ['BlindCountReport', 'InventoryLotLabels', 'InventoryProductLabels', 'InventoryLine', 'Inventory',
           'CreateInventoriesStart']


class Inventory(metaclass=PoolMeta):
    __name__ = 'stock.inventory'

    @classmethod
    def __setup__(cls):
        super(Inventory, cls).__setup__()
        cls.lines.order = [('id', 'DESC')]

    @staticmethod
    def default_empty_quantity():
        return 'keep'

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def confirm(cls, inventories):
        pool = Pool()
        Date = pool.get('ir.date')
        Product = pool.get('product.product')
        moves = []
        stock_error = []
        for inventory in inventories:
            # From each inventory take lines that the quantity is not "empty/blank"
            lines = [line for line in inventory.lines if line.quantity is not None]
            if lines:
                # In "products_by_location" query pass all products from inventory
                with Transaction().set_context(stock_assign=True, stock_date_end=Date.today()):
                    reserved_products_qty = Product.products_by_location(
                        [inventory.location.id],
                        grouping=('product', 'lot'),
                        grouping_filter=([line.product.id for line in lines],))

                for line in lines:
                    # If not consumable Product
                    if not line.product.consumable:
                        # Check if new quantity is enough to complete the reserved delivery notes
                        if line.lot is None:
                            reserved_in = (line.inventory.location.id, line.product.id, None)
                        else:
                            reserved_in = (line.inventory.location.id, line.product.id, line.lot.id)
                        reserved_qty = line.expected_quantity - reserved_products_qty[reserved_in]
                        if reserved_qty > line.quantity:
                            stock_error.append(f"{line.lot.rec_name if line.lot else '...'} - {line.product.code}")
                        move = line.get_move()
                        if move:
                            moves.append(move)
                if len(stock_error) > 0:
                    error_str = "\n".join(error for error in stock_error)
                    raise ValidationError(
                        gettext('electrans.invalid_inventory_quantity', lot_errors=error_str,))
        super(Inventory, cls).confirm(inventories)


class InventoryLine(metaclass=PoolMeta):
    __name__ = 'stock.inventory.line'

    no_moves_since_last_inventory = fields.Function(
        fields.Boolean(
            "No moves since the last inventory",
            help="True if there have not been stock moves of the product-lot in the location since last inventory."),
        'get_no_moves_since_last_inventory')

    def get_no_moves_since_last_inventory(self, name):
        '''
        Return if there is no moves since the last inventory
        '''
        pool = Pool()
        Move = pool.get('stock.move')
        loc = self.inventory.location

        # Get moves for the given product (implicit by lot), lot and location.
        moves = Move.search([
            ('lot', '=', self.lot),
            ['OR',
             [('from_location', '=', loc)],
             [('to_location', '=', loc)]],
            ('state', '=', 'done'),
            ('write_date', '!=', None)],
            order=[('write_date', 'DESC')],
            limit=1)
        # I only need the last one
        # If there is no one, return True
        if not moves:
            return True
        else:
            # If the last move has an inventory line as an origin, return True
            last_move = moves[0]
            if last_move.origin and isinstance(last_move.origin, self.__class__):
                return True
            else:
                # Get inventory lines for the given product (implicit by lot),
                # lot and location.
                # NOTE: Sometimes inventory lines write_date
                # is null, so it is better to use inventory write_date.
                # NOTE: Sometimes the inventory line quantity is null when users
                # want to let it unchanged.
                inventory_lines = self.search([
                    ('lot', '=', self.lot),
                    ('inventory.location', '=', loc),
                    ('inventory.state', '=', 'done')],
                    order=[('inventory.write_date', 'DESC')],
                    limit=1)
                # I only need the last one
                # If there is no one, return False (because there are moves
                # but no inventory lines)
                if not inventory_lines:
                    return False
                else:
                    # Return True if there is no moves after last inventory line
                    # NOTE: Remember that it is better to use the inventory write_date
                    # instead of inventory line write_date.
                    last_inventory_line = inventory_lines[0]
                    return last_move.write_date < last_inventory_line.inventory.write_date


class BlindCountReport(JasperReport):
    __name__ = 'electrans.blind_count.jreport'


class InventoryLotLabels(JasperReport):
    __name__ = 'inventory.lot.labels'

    @classmethod
    def execute(cls, ids, data):
        '''
        It's used to print labels. Depending the model that is executed from,
        it returns diferent data.
        '''
        pool = Pool()
        if 'model' in data:
            ids_lot = []
            if data['model'] == 'stock.inventory':
                InventoryLine = pool.get('stock.inventory.line')
                lines = InventoryLine.search([
                    ('inventory', 'in', ids)])
                lines.sort(key=lambda l: (l.inventory, l.product.code, l.lot))
                ids_lot = [l.lot.id for l in lines]
            elif data['model'] == 'stock.move':
                Move = pool.get('stock.move')
                for move in Move.browse(ids):
                    if move.lot:
                        ids_lot.append(move.lot.id)
            elif data['model'] == 'production':
                Production = pool.get('production')
                for prod in Production.browse(ids):
                    for line in prod.outputs:
                        if line.lot:
                            ids_lot.append(line.lot.id)
            elif data['model'] == 'stock.shipment.internal':
                ids_lot = cls.get_lots(ids, 'stock.shipment.internal')
            elif data['model'] == 'stock.shipment.in':
                ids_lot = cls.get_lots(ids, 'stock.shipment.in')
            elif data['model'] == 'stock.shipment.out':
                ShipmentOut = pool.get('stock.shipment.out')
                for out in ShipmentOut.browse(ids):
                    ids_lot = [move.lot.id for move in out.inventory_moves if move.lot]
            return super(InventoryLotLabels, cls).execute(ids_lot, data)

    @staticmethod
    def get_lots(ids, str_class):
        ids_lot = []
        class_pull = Pool().get(str_class)
        for record in class_pull.browse(ids):
            if str_class == 'stock.shipment.in':
                for move in record.incoming_moves:
                    if move.lot:
                        ids_lot.append(move.lot.id)
            else:
                for move in record.moves:
                    if move.lot:
                        ids_lot.append(move.lot.id)
        return ids_lot


class InventoryProductLabels(JasperReport):
    __name__ = 'inventory.product.labels'

    @classmethod
    def execute(cls, ids, data):
        InventoryLine = Pool().get('stock.inventory.line')
        lines = InventoryLine.search([
            ('inventory', 'in', ids)])
        lines.sort(key=lambda l: (l.inventory, l.product.code))
        ids = [l.product.id for l in lines]
        return super(InventoryProductLabels, cls).execute(ids, data)


class CreateInventoriesStart(metaclass=PoolMeta):
    __name__ = 'stock.inventory.create.start'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()
