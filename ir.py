# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import Unique, fields


class SequenceType(metaclass=PoolMeta):
    __name__ = 'ir.sequence.type'

    @classmethod
    def __setup__(cls):
        super(SequenceType, cls).__setup__()
        cls.name.translate = False


class Sequence(metaclass=PoolMeta):
    __name__ = 'ir.sequence'

    related_products = fields.Function(
        fields.One2Many('product.template', None, 'Products'),
        'get_related_products')

    @classmethod
    def __setup__(cls):
        super(Sequence, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('uniq', Unique(t, t.name), 'electrans.unique_sequence_name'),
        ]
        # TODO
        # cls.prefix.states['readonly'] = And(Eval('code') == 'stock.lot',
        #                                 Bool(Eval('name')))
        cls.name.translate = False

    @fields.depends('name', 'sequence_type')
    def on_change_with_prefix(self, name=None):
        if (self.name and self.sequence_type and self.sequence_type.name == 'Lot'):
            return self.name + '/'

    def get_related_products(self, name):
        pool = Pool()
        TemplateLotSequence = pool.get('product.template.lot_sequence')
        sequences = TemplateLotSequence.search([('lot_sequence', '=', self.id)])
        return [s.template.id for s in sequences]
