# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import Pool
from trytond.modules.jasper_reports.jasper import JasperReport
from datetime import datetime
from trytond.wizard import (Wizard, StateView, StateReport, Button)

__all__ = ['LotLabel', 'ProductLabel', 'ProductClientLabel']


class LotLabel(JasperReport):
    __name__ = 'lot.label'


class ProductLabel(JasperReport):
    __name__ = 'product.label'

    @classmethod
    def execute(cls, ids, data):
        if data.get('model', '') == 'stock.shipment.out':
            ShipmentOut = Pool().get('stock.shipment.out')
            data['ids'] = ids = [
                move.product.id for shipment in ShipmentOut.browse(ids) for move in shipment.outgoing_moves]
        return super(ProductLabel, cls).execute(ids, data)


class ProductClientLabel(JasperReport):
    __name__ = 'product.client.label'

    @classmethod
    def execute(cls, ids, data):
        Shipment = Pool().get('stock.shipment.out')
        records = []
        for s in Shipment.browse(ids):
            if s.customer:
                party = s.customer.trade_name
                effective_date = s.effective_date
                effective_date = datetime.strftime(effective_date, "%d/%m/%y") if effective_date else ""
            for move in s.inventory_moves:
                temp = move.product.template
                sale = ""
                quantity = move.quantity
                uom = move.uom.symbol if move.uom else ""
                if move.origin:
                    if str(move.origin).split("'")[0][:9]=='sale.line':
                        if move.origin.sale:
                            sale = move.origin.sale.reference

                if temp:
                    if temp.salable and temp.product_customers:
                        for customer in temp.product_customers:
                            if customer.party.code == s.customer.code:
                                records.append({
                                    'move_id': move.id,
                                    'client_code': customer.code,
                                    'name': customer.name,
                                    'sale': sale,
                                    'quantity': quantity,
                                    'uom': uom,
                                    'party': party,
                                    'effective_date': effective_date
                                })

        data = {
            'name': 'product.client.label',
            'model': 'stock.move',
            'data_source': 'records',
            'records': records,
            'output_format': 'pdf',
        }
        return super(ProductClientLabel, cls).execute(
            ids, data)


class ShipmentPackageReport(JasperReport):
    __name__ = 'stock.shipment.package.electrans'

    @classmethod
    def execute(cls, ids, data):
        model = data['model']
        Shipment = Pool().get(model)
        records = []
        for ship in Shipment.browse(ids):
            full_address = ship.delivery_address.full_address if ship.delivery_address else ""
            packages = ship.number_packages if ship.number_packages else 1
            weight = ship.weight
            code = ship.number
            if model == 'stock.shipment.out':
                party = ship.customer.rec_name if ship.customer else ""
            else:
                party = ship.delivery_address.party.rec_name if ship.delivery_address else ""
            note = ship.comment if ship.comment else ""

            for i in range(packages):
                records.append({
                    'party': party,
                    'note': "Notas: " + note if note else "",
                    'packages': packages,
                    'actual_package': i+1,
                    'weight': weight,
                    'code': code,
                    'full_address': full_address,
                    'carrier': ship.carrier.rec_name if ship.carrier else ""
                })

        data = {
                'name': 'stock.shipment.package.electrans',
                'model': model,
                'data_source': 'records',
                'records': records,
                'output_format': 'pdf',
            }
        return super(ShipmentPackageReport, cls).execute(ids, data)


class CustomLabelReport(JasperReport):
    __name__ = 'custom_label.report'

    @classmethod
    def execute(cls, ids, data):
        records = []
        # if model exists in data, it's because it comes from internal shipment
        if 'model' in data:
            InternalShipment = Pool().get('stock.shipment.internal')
            for id in data['ids']:
                location = InternalShipment(id).to_location
                records.append({'description': location.code if location else ""})
        else:
            records.append(data)

        return super(CustomLabelReport, cls).execute(ids, {
            'name': 'custom_label.report',
            'data_source': 'records',
            'records': records
        })


class CustomLabel(Wizard):
    'Create Label Wizard'
    __name__ = 'custom_label.create'

    start = StateView('custom_label.create.start',
            'electrans.create_label_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok',
                default=True)
            ])
    print_ = StateReport('custom_label.report')

    def do_print_(self, action):
        data = {
            'description': self.start.description
            }
        return action, data

    def transition_print_(self):
        return 'end'


class CustomLabelCreateStart(ModelView):
    'Create Label'
    __name__ = 'custom_label.create.start'

    description = fields.Text('Description')
