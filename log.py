import os
import sys
import logging
import sentry_sdk
from sentry_sdk import capture_exception
from sentry_sdk.integrations.trytond import TrytondWSGIIntegration
from trytond.exceptions import TrytonException

EXCLUDED_MODELS = [
    'ir.session',
    'ir.session.wizard',
    'ir.ui.view_tree_width',
    'ir.ui.view_tree_state',
    'ir.queue'
]
EXCLUDED_NAMES = (
    'trytond.security',
    'trytond.modules.ldap_authentication.res',
)
SENTRY_DSN = os.getenv("SENTRY_DSN")
logger = logging.getLogger(__name__)


class LogMixin:
    __slots__ = ()

    @classmethod
    def delete(cls, records):
        if cls.__name__ not in EXCLUDED_MODELS:
            for record in records:
                logger.info("Deleted: %s ID %s - %s by %s" % (
                    cls.__name__,
                    record.id,
                    record.rec_name,
                    record._user if record._user else 'Unknown'))
        super().delete(records)


class LogHandler(logging.Handler):

    def __init__(self):
        super().__init__()
        if sys.argv:
            arg = sys.argv[0]
            if 'trytond-worker' in arg or 'trytond-cron' in arg:
                sentry_sdk.init(
                    dsn=SENTRY_DSN,
                    send_default_pii=True,
                    traces_sample_rate=1.0,
                    profiles_sample_rate=1.0,
                    integrations=[
                        TrytondWSGIIntegration(),
                    ],
                )

    def emit(self, record):
        if record.name in EXCLUDED_NAMES or not record.exc_info:
            return
        if isinstance(record.exc_info, tuple) and len(record.exc_info) > 1:
            exception = record.exc_info[1]
            if issubclass(exception.__class__, TrytonException):
                return
        if sys.argv:
            arg = sys.argv[0]
            if exception and 'trytond-worker' in arg or 'trytond-cron' in arg:
                capture_exception(exception)

logging.root.addHandler(LogHandler())
