#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval
from trytond.pool import Pool, PoolMeta

__all__ = ['Nonconformity', 'NonconformityLine', 'ShipmentInReturn']


class Nonconformity(ModelSQL, ModelView):
    'Nonconformity'
    __name__ = 'nonconformity'
    _rec_name = 'description'

    number = fields.Char('Sequence', required=True)
    product = fields.Many2One('product.product', 'Product', required=True)
    lines = fields.One2Many('nonconformity.line', 'nonconformity', 'Lines',
        domain=[('lot.product', '=', Eval('product'))],
        depends=['product'])
    description = fields.Text('Description', required=True)
    resolution_proposal = fields.Text('Resolution proposal', size=None)
    supplier = fields.Many2One('party.party', 'Supplier', required=True)


class NonconformityLine(ModelSQL, ModelView):
    'Nonconformity Line'
    __name__ = 'nonconformity.line'
    nonconformity = fields.Many2One('nonconformity', 'Nonconformity',
         required=True)
    lot = fields.Many2One('stock.lot', 'Lot',
        ondelete='CASCADE', required=True)
    quantity = fields.Integer('Quantity', required=True)
        #TODO: readonly if lot is serial_number
        #readonly=Eval('lot.product.template.serial_number'))
    number = fields.Function(fields.Char('Sequence', required=True), 'get_number', searcher='search_number')

    def get_rec_name(self, name):
        return '[' + self.number + '] ' + self.nonconformity.description[:50] + '...'

    @staticmethod
    def default_quantity():
        return 1

    @classmethod
    def get_number(cls, nonconformitieslines, _):
        res = {}
        for nonconformityline in nonconformitieslines:
            res[nonconformityline.id] = nonconformityline.nonconformity.number
        return res

    @classmethod
    def search_number(cls, _, clause):
        return [('nonconformity.number', '=', clause[2])]


class ShipmentInReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    @classmethod
    def get_supplier(cls, shipments, name):
        res = {}
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        NonConformityLine = pool.get('nonconformity.line')
        for shipment in shipments:
            party = None
            if shipment.moves:
                origin = None
                for move in shipment.moves:
                    if move.origin:
                        origin = move.origin
                if origin:
                    if isinstance(origin, PurchaseLine):
                        party = origin.purchase.party.id
                    elif isinstance(origin, NonConformityLine):
                        party = origin.nonconformity.supplier.id
            res[shipment.id] = party
        return res
