# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Party', 'PartyAlternativeReport']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    supplier_code = fields.Char('Supplier Code',
        help='The code that our company has as a supplier for this customer.')
    # ID field to import parties from Industry (sincronization)
    industry_code = fields.Char('Industry Code')
    location = fields.Many2One('stock.location', "Location")


class PartyAlternativeReport(metaclass=PoolMeta):
    __name__ = 'party.alternative_report'

    @classmethod
    def __setup__(cls):
        super(PartyAlternativeReport, cls).__setup__()
        option = ('stock.shipment.out', 'Shipment Out')
        if option not in cls.model_name.selection:
            cls.model_name.selection.append(option)
