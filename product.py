# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateTransition, \
    StateAction
from trytond.i18n import gettext
from decimal import Decimal, InvalidOperation

__all__ = ['ProductConfiguration', 'Template', 'Product',
           'ProductTemplateQualityTemplate', 'ProductQualityTemplate',
           'ProductTemplateSpecifications', 'Category']


STATES = {
    'readonly': ~Eval('active', True)}
DEPENDS = ['active']
UNIQUE_STATES = STATES.copy()
UNIQUE_STATES.update({
        'invisible': ~Eval('unique_variant', False)})


class ProductConfiguration(metaclass=PoolMeta):
    __name__ = 'product.configuration'
    host = fields.Char('Host')
    user = fields.Char('User')
    password = fields.Char('password')
    database = fields.Char('Database')
    product_category = fields.Many2One('product.category', 'Category')
    infoal_host = fields.Char('Host')
    infoal_user = fields.Char('User')
    infoal_password = fields.Char('password')
    infoal_database = fields.Char('Database')
    industrialization_email = fields.Many2One(
        'party.contact_mechanism', 'Industrialization Department Email')


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    from_infoal = fields.Boolean('From Infoal', readonly=True)
    infoal_code = fields.Char('Infoal Code')
    quality_templates = fields.Many2Many('product.template-quality.template',
        'product_template', 'quality_template', 'Quality Templates',
        states=STATES, depends=DEPENDS)
    customer_shipment_description = fields.Text('Customer Shipment Description')
    specifications = fields.One2Many('product.template.specifications',
        'template', 'Specifications')
    description = fields.Function(fields.Char('Description'),
        'get_description', searcher='search_description')
    reference = fields.Function(fields.Char("Reference", states=UNIQUE_STATES,
        depends=DEPENDS + ['unique_variant']),
        'get_reference', searcher='search_reference')# setter='set_code')

    # Altium properties
    library_path = fields.Char('Library Path')
    library_ref = fields.Char('Library Ref')
    footprint_path = fields.Char('Footprint Path')
    footprint_ref = fields.Char('Footprint Ref')
    help_url = fields.Char('Help URL')
    value = fields.Char('Value')
    parameter1 = fields.Char('Parameter 1')
    parameter2 = fields.Char('Parameter 2')
    parameter3 = fields.Char('Parameter 3')
    parameter4 = fields.Char('Parameter 4')
    boms = fields.Function(
        fields.One2Many('product.product-production.bom', None, 'Boms'),
        'get_boms')
    production_lead_times = fields.Function(
        fields.One2Many('production.lead_time', None, 'Lead Times'),
        'get_production_lead_times')
    higher_list_price = fields.Function(fields.Numeric('Higher List Price',
        help="The higher unit price ever sold."),
        'get_higher_list_price')

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls.name.translate = False
        cls.account_category.depends.update({'type'})
        cls.account_category.domain += [('type', 'like', Eval('type'))]

    @classmethod
    def copy(cls, templates, default=None):
        res = []
        default.setdefault('active', True)
        default.setdefault('list_price', 0)
        default.setdefault('library_path', None)
        default.setdefault('library_ref', None)
        default.setdefault('footprint_path', None)
        default.setdefault('footprint_ref', None)
        default.setdefault('help_url', None)
        default.setdefault('value', None)
        default.setdefault('parameter1', None)
        default.setdefault('parameter2', None)
        default.setdefault('parameter3', None)
        default.setdefault('parameter4', None)
        default.setdefault('product_customers', None)
        default.setdefault('customer_shipment_description', '')
        default.setdefault('product_suppliers', None)
        default.setdefault('lot_required', [
            'supplier', 'customer', 'lost_found', 'storage', 'production'])
        # Keep the code with the suffix (2) instead of setting it to blank
        for template in templates:
            default['code'] = '{} ({})'.format(template.code, 2)
            res += super(Template, cls).copy([template], default)
        return res

    @classmethod
    def default_taxes_category(cls):
        return True

    @classmethod
    def default_lot_required(cls):
        return ['supplier', 'customer', 'lost_found', 'storage', 'production']

    def get_description(self, name):
        if self.unique_variant:
            return self.products and self.products[0].description or None

    @classmethod
    def search_description(cls, name, clause):
        return [
            ('unique_variant', '=', True),
            ('products.description',) + tuple(clause[1:]),
        ]

    @fields.depends('type')
    def on_change_with_account_category(self):
        return None

    def get_reference(self, name):
        if self.unique_variant:
            with Transaction().set_context(active_test=False):
                return self.products and self.products[0].reference or None

    @classmethod
    def search_reference(cls, name, clause):
        return [
            ('unique_variant', '=', True),
            ('products.reference',) + tuple(clause[1:]),
        ]

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR', super(Template, cls).search_rec_name(name, clause),
                # Search again by code because the product module is removing
                # the initial wildcard
                [('products.code',) + tuple(clause[1:])],
                # Search by reference and manufacturer_code too.
                [('products.reference',) + tuple(clause[1:])],
                [('products.manufacturer_code',) + tuple(clause[1:])]
                ]

    @staticmethod
    def order_reference(tables):
        pool = Pool()
        Product = pool.get('product.product')
        table, _ = tables[None]
        product_table = tables.get('product')
        if product_table is None:
            product = Product.__table__()
            product_table = {
                None: (product, (product.template == table.id) &
                    table.unique_variant),
                }
            tables['product'] = product_table
        table, _ = product_table[None]
        return [table.code]

    def get_boms(self, _):
        if self.unique_variant:
            product = Pool().get('product.product').search([
                ('template', '=', self.id)])
            return [bom.id for bom in product[0].boms] if product else None

    def get_production_lead_times(self, name):
        production_lead_times = []
        products = Pool().get('product.product').search([
            ('template', '=', self.id)])
        for p in products:
            production_lead_times += [l.id for l in p.production_lead_times]
        return production_lead_times

    def get_higher_list_price(self, name):
        SaleLine = Pool().get('sale.line')
        Product = Pool().get('product.product')
        sale_line = False
        if self.unique_variant:
            products = Product.search([('template', '=', self.id)])
            if products:
                sale_line = SaleLine.search([
                    ('product', '=', products[0]),
                    ('sale.state', 'in', ['processing', 'done'])],
                    order=[('unit_price', 'DESC')],
                    limit=1)
        return sale_line[0].unit_price if sale_line else None


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    quality_templates = fields.Many2Many('product.product-quality.template',
        'product', 'quality_template', 'Quality Templates', states=STATES,
        depends=DEPENDS)
    # TODO _parent_product depends stock.move
    # moves = fields.One2Many('stock.move', 'product', 'Moves')
    # purchase_requests = fields.One2Many('purchase.request', 'product',
    #     'Purchase Requests')
    reference = fields.Char('Reference')
    label_text = fields.Char('Label Text', size=45,
        help="This text will replace the name of the product in the labels "
        "in case to be filled in")

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls.name.translate = False
        cls.description.translate = False
        cls.boms.states['required'] = Eval('phantom')
        cls.phantom.help = "If checked, the BOM of this product won't create a separate production, but it will be added to the production of the higher-level BOM"
        # To deactivate the full text search so don't
        # need to write the full word to match record
        cls.description.search_full_text = False

    @classmethod
    def copy(cls, products, default=None):
        if default is None:
            default = {}
        not_copy = {
            # TODO _parent_product depends stock.move
            # 'moves': None,
            # 'purchase_requests': None,
            'cost_price': 0,
            'label_text': None,
            'product_suppliers': None,
            'phantom': False,
            }
        default.update(not_copy)
        res = super(Product, cls).copy(products, default)
        return res

    @classmethod
    def delete(cls, products):
        pool = Pool()
        OrderPoint = pool.get('stock.order_point')
        product_ids = [product.id for product in products]
        orders = OrderPoint.search([('product', 'in', product_ids)])
        OrderPoint.delete(orders)
        super(Product, cls).delete(products)

    def get_rec_name(self, name):
        rec_name = super(Product, self).get_rec_name(name)
        split_name = rec_name.split('] ')
        ref = self.reference if self.reference else self.manufacturer_code if self.manufacturer_code else None
        if ref:
            rec_name = split_name[0] + '] ' + ref + ' ' + split_name[1] if len(split_name) == 2 \
                else ref + ' ' + rec_name
        return rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR', super(Product, cls).search_rec_name(name, clause),
                # Search again by code because the product module is removing the initial wildcard
                [('code',) + tuple(clause[1:])],
                # Search by reference and manufacturer_code too.
                [('reference',) + tuple(clause[1:])],
                [('manufacturer_code',) + tuple(clause[1:])]
                ]

    def lot_is_required(self, from_, to):
        if not Transaction().context.get('check_lot_required', True):
            return
        return super(Product, self).lot_is_required(from_, to)

    @classmethod
    def get_purchase_product_unit_price(cls, request):
        PurchaseLine = Pool().get('purchase.line')
        PurchaseRequisitionLine = Pool().get('purchase.requisition.line')
        last_line = PurchaseLine.get_last_line(request.product, request.party)
        if request.unit_price:
            gross_unit_price = request.unit_price
        elif isinstance(request.origin,
                        PurchaseRequisitionLine) and request.origin.unit_price:
            gross_unit_price = request.origin.unit_price
        elif last_line and last_line.gross_unit_price:
            gross_unit_price = last_line.gross_unit_price
        else:
            gross_unit_price = Decimal('0.0')
        return gross_unit_price

    @classmethod
    def get_purchase_product_discount(cls, request):
        PurchaseLine = Pool().get('purchase.line')
        last_line = PurchaseLine.get_last_line(request.product, request.party)
        return last_line.discount if last_line else Decimal('0.0')

    @classmethod
    def _domain_moves_cost(cls):
        domain = super()._domain_moves_cost()
        # To exclude the moves of a product without cost_price when showing the records
        # of the product cost price history and when recomputing the cost_price of a product
        domain = [domain, ('cost_price', '!=', None)]
        return domain


class ProductTemplateQualityTemplate(ModelSQL):
    'Product Template - Quality Template'
    __name__ = 'product.template-quality.template'
    _table = 'product_template_quality_template_rel'
    product_template = fields.Many2One('product.template', 'Template',
        ondelete='CASCADE', required=True)
    quality_template = fields.Many2One('quality.template', 'Quality Template',
        ondelete='CASCADE', required=True)


class ProductQualityTemplate(ModelSQL):
    'Product - Quality Template'
    __name__ = 'product.product-quality.template'
    _table = 'product_product_quality_template_rel'
    product = fields.Many2One('product.product', 'Product', ondelete='CASCADE',
        required=True)
    quality_template = fields.Many2One('quality.template', 'Quality Template',
        ondelete='CASCADE', required=True)


class ProductTemplateSpecifications(ModelSQL, ModelView):
    'Template Specifications'
    __name__ = 'product.template.specifications'
    template = fields.Many2One('product.template', 'Template',
         ondelete='CASCADE', required=True)
    code = fields.Char('Code', required=True)
    name = fields.Text('Name')
    edition = fields.Numeric('Edition', digits=(16, 1), required=True)


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    type = fields.Function(fields.Char('Type'),
                      'get_type',  searcher='search_type')

    def get_type(self, name):
        if 'Bienes' in self.rec_name:
            return 'goods'
        elif 'Servicios' in self.rec_name:
            return 'service'
        elif 'Activos fijos' in self.rec_name:
            return 'assets'
        return ' '

    @classmethod
    def search_type(cls, name, clause):
        rec_name = ''
        if clause[2]=='goods':
            rec_name = 'Bienes'
        elif clause[2]=='service':
            rec_name = 'Servicios'
        elif clause[2]=='assets':
            rec_name = 'Activos fijos'
        return [('rec_name', clause[1], rec_name + '%')]


class PriceListLine(metaclass=PoolMeta):
    __name__ = 'product.price_list.line'

    position = fields.Char('Position')

    @staticmethod
    def default_formula():
        return ''

    @staticmethod
    def default_quantity():
        return 1


class PriceListAmountStart(ModelView):
    'Product Price List Amount Start'
    __name__ = 'product.price_list_amount.start'

    price_lists_lines = fields.One2Many('product.price_list.line', None,
        'Price lists lines', required=True)
    percentage = fields.Numeric("Percentage", digits=(1, 3), required=True)


class PriceListAmount(Wizard):
    'Product Price List Amount Wizard'
    __name__ = 'product.price_list_amount'

    start = StateTransition()
    ask_price_list = StateView('product.price_list_amount.start',
        'electrans.price_list_percentage_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Apply', 'apply_', 'tryton-ok', default=True)
            ])
    apply_ = StateAction('electrans.act_product_price_list_amount')

    def transition_start(self):
        return 'ask_price_list'

    def default_ask_price_list(self, fields):
        lines = []
        for record in self.records:
            for line in record.lines:
                lines.append(line.id)
        return {
            'price_lists_lines': lines
        }

    def do_apply_(self, action):
        ProductPriceListLine = Pool().get('product.price_list.line')
        to_save = []
        for line in self.ask_price_list.price_lists_lines:
            try:
                formula = Decimal(str(line.formula))
            except InvalidOperation:
                raise UserError(gettext(
                    'electrans.price_list_line_formula_not_numeric',
                    product=line.product.code,
                    price_list=line.price_list.name))
            formula *= (Decimal(1.0) + self.ask_price_list.percentage)
            line.formula = line.price_list.company.currency.round(formula)
            to_save.append(line)
        ProductPriceListLine.save(to_save)

        return 'end'
