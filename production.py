# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields, sequence_ordered, ModelSQL
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, Button, StateAction, StateView, StateTransition, StateReport
from trytond.pyson import Eval, Bool, PYSONEncoder, Id
from trytond.transaction import Transaction
from trytond.modules.jasper_reports.jasper import JasperReport
from decimal import Decimal
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
from trytond.modules.stock.exceptions import MoveFutureWarning
import datetime
import math

_ZERO = Decimal('0.0')

__all__ = ['BOM', 'BOMInput', 'OpenBOMTree', 'Operation', 'Route', 'Production',
    'ProductionReport', 'ProductionProductReport', 'ProductionLocationReport',
    'BOMReport', 'ProductionElectransInternalStart', 'ProductionElectransInternal',
    'OpenElectransShipmentInternal', 'ProductionSerialNumberSplit', 'ProductionSerialNumberSplitStart']


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'

    version_number = fields.Integer('Version Number')
    changelog = fields.Text('Changes description')
    document = fields.Char('Document')
    doc_min_edition = fields.Char('Document edition')
    product = fields.Function(fields.Many2One('product.product', 'Product'),
        'get_product', searcher='search_product')

    @classmethod
    def __setup__(cls):
        super(BOM, cls).__setup__()
        cls.name.translate = False

    def get_product(self, name):
        pool = Pool()
        Bom = pool.get('product.product-production.bom')

        if self.master_bom:
            relations = Bom.search([('bom', '=', self.master_bom.id)], limit=1)
            return relations[0].product.id if relations and relations[0].product else None

    @classmethod
    def search_product(cls, name, clause):
        pool = Pool()
        Bom = pool.get('product.product-production.bom')
        rels = Bom.search([(('product',) + tuple(clause[1:]))])
        p_ids = [rel.bom.id if rel.bom else 0 for rel in rels]
        return [('id', 'in', p_ids)]


class BOMInput(sequence_ordered(), metaclass=PoolMeta):
    __name__ = 'production.bom.input'

    # map distribution field mapping by Industry
    map_distribution = fields.Char('Map Distribution')

    @classmethod
    def create(cls, vlist):
        # Check if a product is repeated in the same bom inputs
        pool = Pool()
        Product = pool.get('product.product')
        vlist = [x.copy() for x in vlist]
        _inputs = []
        for vals in vlist:
            if cls.search([('product', '=', vals.get('product')),
                           ('bom', '=', vals.get('bom'))]) or \
                           (vals.get('product'), vals.get('bom')) in _inputs:
                raise UserError(gettext(
                        'electrans.duplicated_production_bom_product',
                        product=Product(vals.get('product')).rec_name))
            _inputs.append((vals.get('product'), vals.get('bom')))
        return super(BOMInput, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        # Check if a product is repeated in the same bom inputs
        pool = Pool()
        Product = pool.get('product.product')
        actions = iter(args)
        for _inputs, values in zip(actions, actions):
            if values.get('product') and _inputs and _inputs[0].product \
            and values.get('product') != _inputs[0].product.id:
                input_ids = [i.bom.id for i in _inputs if i.bom]
                if cls.search([('bom', 'in', input_ids),
                               ('product', '=', values.get('product'))]):
                    raise UserError(gettext(
                        'electrans.duplicated_production_bom_product',
                        product=Product(values.get('product')).rec_name))
        return super(BOMInput, cls).write(*args)


class OpenBOMTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open'

    def default_start(self, fields):
        default = super(OpenBOMTree, self).default_start(fields)
        if not default.get('quantity'):
            default['quantity'] = 1
        return default


class Operation(metaclass=PoolMeta):
    __name__ = 'production.operation'
    quality_tests = fields.Function(fields.One2Many('quality.test',
            None, 'Quality Tests'),
        'get_quality_tests', setter='set_quality_tests')
    lot_attributes = fields.Function(fields.Dict('stock.lot.attribute',
        'Attributes'), 'get_lot_attributes', setter='set_lot_attributes')
    lot = fields.Function(fields.Char('Lot'), 'on_change_with_lot',
        searcher='search_lot')
    drawing = fields.Function(fields.Many2One('production.drawing', 'Drawing'),
        'on_change_with_drawing')
    drawing_lines = fields.Function(fields.One2Many('production.drawing.line',
        'production', 'Drawing Positions', states={
            'invisible': ~Bool(Eval('drawing')),
        }), 'on_change_with_drawing_lines')
    drawing_image = fields.Function(fields.Binary('Drawing Image', states={
            'invisible': ~Bool(Eval('drawing')),
        }), 'on_change_with_drawing_image')
    deadline = fields.Date(
        "Deadline",
        states={'readonly': Eval('state').in_(['running', 'done'])},
        depends=['state'])
    reference = fields.Function(fields.Char('Reference'),
                                'get_reference', searcher='search_reference')

    @classmethod
    def __setup__(cls):
        super(Operation, cls).__setup__()
        cls._order = [('deadline', 'ASC NULLS FIRST')]
        cls.quality_tests.size = 1 # no active new button to add new quality tests

    def get_quality_tests(self, name):
        QualityTest = Pool().get('quality.test')

        lots = [o.lot for o in self.production.outputs if o.lot]
        stock_lots = ['stock.lot,' + str(l.id) for l in lots]
        tests = QualityTest.search([
            ('document', 'in', stock_lots),
            ])
        return [t.id for t in tests]

    @classmethod
    def set_quality_tests(cls, operations, name, value):
        QualtiyTest = Pool().get('quality.test')

        test_ids = []
        for v in value:
            if v[0] == 'write':
                test_ids.append(v[1][0])
        quality_tests = QualtiyTest.browse(test_ids)

        to_write = []
        for v in value:
            if v[0] == 'write':
                id_ = v[1][0]
                for test in quality_tests:
                    if id_ == test.id:
                        break
                to_write.extend(([test], v[2]))

        if to_write:
            QualtiyTest.write(*to_write)

    def get_lot_attributes(self, name):
        lots = [o.lot for o in self.production.outputs if o.lot]

        attributes = {}
        for l in lots:
            if l.attributes:
                attributes.update(l.attributes)
        return attributes

    @classmethod
    def set_lot_attributes(cls, operations, name, value):
        Lots = Pool().get('stock.lot')

        lots = []
        for op in operations:
            for output in op.production.outputs:
                if output.lot:
                    lots.append(output.lot)

        if lots:
            Lots.write(lots, {'attributes': value})

    @fields.depends('production', '_parent_production.outputs')
    def on_change_with_lot(self, name=None):
        if self.production:
            lots = [o.lot.number for o in self.production.outputs if o.lot]
            return ', '.join(lots)

    @classmethod
    def search_lot(cls, name, clause):
        operations = cls.search([
            ('production.outputs.lot.number',) + tuple(clause[1:])])
        if operations:
            return [('id', 'in', [o.id for o in operations])]

    @fields.depends('production', '_parent_production.drawing')
    def on_change_with_drawing(self, name=None):
        if self.production and self.production.drawing:
            return self.production.drawing.id

    @fields.depends('production', '_parent_production.drawing_lines')
    def on_change_with_drawing_lines(self, name=None):
        if self.production and self.production.drawing_lines:
            return list(self.production.drawing_lines)

    @fields.depends('drawing')
    def on_change_with_drawing_image(self, name=None):
        return self.drawing.image if self.drawing else None

    def get_reference(self, name):
        return self.production.reference

    @classmethod
    def search_reference(cls, name, clause):
        return [
            ('production.reference',) + tuple(clause[1:]),
        ]

    @classmethod
    def wait(cls, operations):
        transaction = Transaction()
        # Don't pass operations to wait when running the production, only when button is pressed
        if not transaction.context.get('from_production_run'):
            super(Operation, cls).wait(operations)


class OperationWorkCenterChange(Wizard):
    'Operation Work Center Change'
    __name__ = 'production.operation.work_center_change'

    start = StateView('production.operation.work_center_change.start',
                      'electrans.operation_work_center_change_start_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Confirm', 'work_center_change_start', 'tryton-ok', default=True),
                      ])
    work_center_change_start = StateTransition()

    def default_start(self, fields):
        defaults={}
        if self.record.state != 'waiting':
            raise UserError(gettext('electrans.invalid_operation_state'))
        defaults['work_center_category'] = self.record.work_center_category.id
        return defaults

    def transition_work_center_change_start(self):
        """
        This function changes the work centers of the selected operations at the same time
        """
        pool = Pool()
        Operation = pool.get('production.operation')
        for operation in self.records:
            operation.work_center = self.start.work_center
        Operation.save(self.records)
        return 'end'


class OperationWorkCenterChangeStart(ModelView):
    'Operation Work Center Change'
    __name__ = 'production.operation.work_center_change.start'

    operation = fields.Many2One('production.operation', 'Operation')
    work_center_category = fields.Many2One('production.work_center.category',
                                           'Work Center Category', required=True)
    work_center = fields.Many2One('production.work_center', 'Work Center',
                                   depends=['work_center_category'], domain=[
                                ('category', '=', Eval('work_center_category'),
                                 )])


class Route(metaclass=PoolMeta):
    __name__ = 'production.route'

    def get_rec_name(self, name):
        name = super(Route, self).get_rec_name(name)
        if self.operations:
            name += '[' + ' - '.join(
                [o.rec_name for o in self.operations]) + ']'
        return name


class ProductionInternalShipment(ModelSQL):
    'Production - Internal Shipment'
    __name__ = 'production-stock.shipment.internal'
    production = fields.Many2One('production', 'Production',
                                 ondelete='CASCADE',  required=True)
    shipment = fields.Many2One('stock.shipment.internal', 'Shipment',
                               ondelete='CASCADE',  required=True)


class Production(metaclass=PoolMeta):
    __name__ = 'production'
    order_inputs = fields.Function(fields.One2Many('stock.move',
            'production_input', 'Order Inputs'),
        'get_order_inputs')
    product_inputs = fields.One2Many('stock.move', 'production_input',
        'Product Inputs', order=[('product', 'ASC')],
        readonly=True)
    location_inputs = fields.One2Many('stock.move', 'production_input',
        'Location Inputs', order=[('from_location', 'ASC')],
        readonly=True)
    import_log = fields.Text('Import Log')
    csv_file = fields.Binary('Csv File', filename='filename')
    filename = fields.Char('Filename')
    supply_shipments = fields.Many2Many('production-stock.shipment.internal',
                                 'production', 'shipment', 'Supply Shipments',
                                 states={
                                     'invisible': ~Eval('supply_shipments'),
                                 },
                                 readonly=True)
    total_labor_time = fields.Function(
        fields.TimeDelta("Total Labor Time"),
        'get_total_labor_time')
    update_outputs_cost_price = fields.Boolean(
        "Update Outputs Cost Price",
        states={'readonly': ~Eval('state').in_(
            ['request', 'draft']) | Eval('production_resets')},
        depends=['state', 'production_resets'])

    def get_total_labor_time(self, name=None):
        # Compute total time dedicated to a production
        pool = Pool()
        Uom = pool.get('product.uom')
        hour_uom, = Uom.search([('symbol', '=', 'h')])
        total_labor_time = sum(
            Uom.compute_qty(
                op.work_center_category.uom, op.total_quantity, hour_uom)
            for op in self.operations)
        return datetime.timedelta(hours=total_labor_time)

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        # change reaonly destination_warehouse from production_subcontract
        cls.destination_warehouse.readonly = False
        dw_readonly = Eval('state').in_(['cancelled', 'done'])
        if cls.destination_warehouse.states.get('readonly'):
            previous_readonly = cls.destination_warehouse.states.get('readonly')
            dw_readonly = dw_readonly | previous_readonly
        cls.destination_warehouse.states['readonly'] = dw_readonly

        cls.planned_date.states['required'] = True
        if cls.destination_warehouse.depends and 'state' in cls.destination_warehouse.depends:
            cls.destination_warehouse.depends.append('state')
        else:
            cls.destination_warehouse.depends = ['state']
        cls._order = [
            ('effective_date', 'ASC'),
            ('planned_date', 'ASC'),
            ('number', 'ASC'),
            ] + cls._order
        cls.inputs.order = [('id', 'DESC')]
        cls.outputs.order = [('id', 'DESC')]
        cls._buttons.update({
            'clean_lots': {
                'invisible': Eval('state') != 'draft'
            },
            'import_csv_file': {
                'readonly': ~Eval('csv_file')
            }
        })
        cls._buttons['reset_wizard']['invisible'] &= (~Id('electrans',
        'group_reset_production').in_(
        Eval('context', {}).get('groups', [])) | ~Eval(
        'state').in_(['running', 'done']))

    def check_cost(self):
        # avoid this check because it could be little differences because of
        # rounding problems
        return

    # Because Move.on_change_with_unit_price_required override to allow Null in
    # moves, it is needed to assign a value (_ZERO) before calling super.set_cost
    @classmethod
    def set_cost(cls, productions):
        pool = Pool()
        Move = pool.get('stock.move')
        productions = [p for p in productions if p.update_outputs_cost_price]
        moves = []
        for production in productions:
            for output in production.outputs:
                if output.unit_price is None:
                    output.unit_price = _ZERO
                    moves.append(output)
        Move.save(moves)
        return super(Production, cls).set_cost(productions)

    def get_rec_name(self, name):
        name = super(Production, self).get_rec_name(name)
        if self.product:
            name = '%s %s' % (name, self.product.rec_name)
        if self.state == 'assigned':
            name = '* %s' % name
        return name

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Production, cls).search_rec_name(name, clause)
        return ['OR',
            domain,
            ('product',) + tuple(clause[1:]),
            ]

    def get_order_inputs(self, name):
        # Return inputs according the context (order by product, location...)
        context = Transaction().context

        if context.get('product_inputs'):
            return [i.id for i in self.product_inputs]
        if context.get('location_inputs'):
            return [i.id for i in self.location_inputs]
        else:
            return [i.id for i in self.inputs]

    def create_output_lots(self):
        '''Create Quality Tests when the lot in output products is created'''
        QualityTest = Pool().get('quality.test')

        created_lots = super(Production, self).create_output_lots()

        # get quality tests by lot has been created
        # (production workflow: run -> wait -> run)
        lots = [o.lot for o in self.outputs if o.lot]
        stock_lots = ['stock.lot,' + str(l.id) for l in lots]
        test_lots = [t.document.id for t in QualityTest.search([
            ('document', 'in', stock_lots),
            ]) if t.document.__name__ == 'stock.lot']

        to_create = []
        for output in self.outputs:
            if not output.lot:
                continue
            if output.lot.id in test_lots:
                continue
            product = output.lot.product
            templates = product.quality_templates
            templates += product.template.quality_templates
            if templates:
                test = QualityTest()
                test.document = output.lot
                test.templates = templates
                to_create.append(test._save_values)

        if to_create:
            tests = QualityTest.create(to_create)
            QualityTest.apply_templates(tests)
        return created_lots

    @classmethod
    def run(cls, productions):
        pool = Pool()
        Operation = pool.get('production.operation')
        Product = pool.get('product.product')
        Move = pool.get('stock.move')
        Shipment = pool.get('stock.shipment.internal')
        Configuration = pool.get('stock.configuration')
        Warning = pool.get('res.user.warning')
        # Used to not passing operations to wait
        with Transaction().set_context(from_production_run=True):
            super(Production, cls).run(productions)
        to_write = []
        for production in productions:
            # create internal shipment to return material to workshop if is needed
            locations = set([move.from_location for move in production.inputs])
            for location in locations:
                if location.movable:
                    leftovers_location = location.warehouse.leftovers_location.id \
                        if location.warehouse.leftovers_location else Configuration(1).leftovers_location.id
                    pending_moves = Move.search(
                        [['OR', ('from_location', '=', location), ('to_location', '=', location)],
                         ('state', 'not in', ['done', 'cancelled'])], limit=1)
                    if not pending_moves:
                        to_create = []
                        for key, value in list(Product.products_by_location([location.id]).items()):
                            if bool(value) and value > 0:
                                product = Product(key[1])
                                to_create.append({'product': product, 'quantity': value, 'from_location': location,
                                                  'to_location': leftovers_location, 'uom': product.default_uom})
                        if to_create:

                            key = 'location_%s_%s' % (location.id, production.id)
                            if Warning.check(key):
                                raise UserWarning(key, gettext('electrans.create_internal_shipment',
                                                  location=location.name))
                            Shipment.create([{'from_location': location, 'to_location': leftovers_location,
                                              'moves': [('create', to_create)]}])
                        else:
                            key = 'location_%s_%s' % (location.id, production.id)
                            if Warning.check(key):
                                raise UserWarning(key, gettext('electrans.deactivate_location', location=location.name))
                            # if the location don't have more stock and there are not pending moves, it is deactivated
                            location.active = False
                            location.save()

            # save lot attributes in production operations
            if not production.outputs or not production.operations:
                continue

            lot = production.outputs[0].lot
            if not lot or not lot.attribute_set:
                continue

            attributes = {a.name: None for a in lot.attribute_set.attributes}
            for operation in production.operations:
                if operation.lot_attributes:
                    continue
                values = {
                    'lot_attributes': attributes,
                    }
                to_write.extend(([operation], values))
        if to_write:
            Operation.write(*to_write)

    @classmethod
    def copy(cls, productions, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['product_inputs'] = None
        default['location_inputs'] = None
        default['supply_shipments'] = None
        return super(Production, cls).copy(productions, default=default)

    @classmethod
    @ModelView.button
    def clean_lots(cls, productions):
        pool = Pool()
        Move = pool.get('stock.move')
        Warning = pool.get('res.user.warning')

        to_clean = []
        to_warn = []
        for production in productions:
            if production.state != 'draft':
                continue
            for pinput in production.inputs:
                if pinput.lot:
                    if pinput.state == 'draft':
                        to_clean.append(pinput)
                    else:
                        to_warn.append(pinput)

        if to_clean:
            Move.write(to_clean, {'lot': None})
        if to_warn:
            key = 'inputs_done%s' % (productions[0].id)
            if Warning.check(key):
                raise UserWarning(
                    key, gettext('electrans.production_input_with_lot_done',
                        inputs=',\n'.join(x.product.rec_name for x in to_warn)))

    @classmethod
    def _get_internal_shipment(cls, from_location, to_location,
                               productions=None):
        ShipmentInternal = Pool().get('stock.shipment.internal')

        shipment = ShipmentInternal()
        shipment.from_location = from_location
        shipment.to_location = to_location
        shipment.moves = []
        if productions:
            shipment.productions = productions
        return shipment

    @classmethod
    def _get_internal_move(cls, from_location, to_location, production_input):
        Move = Pool().get('stock.move')
        return Move(
            from_location=from_location,
            to_location=to_location,
            product=production_input.product,
            # TODO: Support lots
            quantity=production_input.uom.round(production_input.quantity),
            uom=production_input.uom,
            origin=production_input if production_input else None,
            )

    @classmethod
    def create_internal_shipments(
        cls, productions, to_location, split_by_planned_start_date):
        """Create internal shipments grouping locations and products"""
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')

        internal_grouping = {}
        # In this loop we are defining the key to group the
        # productions and group them by the key in a dict and
        # for each group, 1 Internal Shipment will be created
        for production in productions:
            if production.state in ('cancelled', 'done'):
                continue

            if to_location is None:
                to_location = production.warehouse.storage_location
            from_location = to_location.provisioning_location_used
            to_location = to_location.storage_location or to_location

            if not from_location:
                raise UserError(gettext(
                    'electrans.missing_provisioning_location',
                    warehouse=to_location.rec_name))

            if split_by_planned_start_date:
                key = (
                    from_location, to_location, production.planned_start_date)
            else:
                key = (from_location, to_location)
            if key in internal_grouping:
                internal_grouping[key].append(production)
            else:
                internal_grouping[key] = [production]

        to_create = []
        production_moves_from_update = []
        # The main loop, that we have the productions grouped by the key
        for k, productions in list(internal_grouping.items()):
            from_location = k[0]
            to_location = k[1]
            planned_start_dates = [p.planned_start_date for p in productions
                                   if p.planned_start_date]

            # get product qty in to location
            product_ids = set()
            for production in productions:
                for _input in production.inputs:
                    if _input.state == 'draft':
                        product_ids.add(_input.product.id)

            # Create the Internal Shipment of the current grouped productions
            # Only with from and to_location, no moves yet
            shipment = cls._get_internal_shipment(
                from_location,
                to_location,
                productions=productions)
            if planned_start_dates:
                planned_start_date = min(planned_start_dates)
                shipment.planned_start_date = planned_start_date
                shipment.planned_date = planned_start_date

            # Loop to add the moves of the shipment
            for production in productions:
                exclude_consumables = production.warehouse.exclude_consumables_picking
                for _input in production.inputs:
                    if (_input.state != 'draft' or exclude_consumables
                            and _input.product.template.consumable):
                        continue

                    production_moves_from_update.append(_input)
                    move = cls._get_internal_move(
                        from_location, to_location, _input)
                    shipment.moves += (move,)

            # to create new internal shipment
            if shipment.moves:
                to_create.append(shipment._save_values)

        # change from_location production moves to pallet (save)
        Move.write(production_moves_from_update, {
            'from_location': to_location})

        return ShipmentInternal.create(to_create)

    # OQA 04/06/18: This function, check if csv file, refers to the production
    # selected, and modify/create the moves that customer specified.
    @classmethod
    @ModelView.button
    def import_csv_file(cls, records):
        for record in records:
            pool = Pool()
            Move = pool.get('stock.move')
            Lot = pool.get('stock.lot')
            Uom = pool.get('product.uom')
            Location = pool.get('stock.location')
            data = str(record.csv_file).split('\n')
            delimiter = ';'
            record.import_log = ""
            # Check if production in state draft or waiting, and check if the production number and the quantity, is
            # the same in csv file
            if record.state in ["draft", "waiting"] and record.number in [' ', data[3].split(delimiter)[-2]] and \
                    record.quantity == float(data[3].split(delimiter)[1].replace(',', '.')):
                data = data[7:]
                for row in data:
                    cols = row.split(delimiter)
                    if len(cols) == 6:
                        location_id = Location.search([('code', '=', cols[2])]) if cols[2] else None
                        location_domain = ('from_location', '=', location_id[0].id) if location_id else ()

                        uom = Uom.search([('name', '=', cols[3])]) if cols[3] else None
                        uom_domain = ('uom', '=', uom[0]) if uom else ()
                        domain = [('production_input', '=', record.id),
                                  ('product.number', '=', cols[0]), uom_domain,
                                  ('quantity', '=', float(cols[5].replace(',', '.'))),
                                  location_domain, ('lot', '=', cols[4])]

                        move = Move.search(domain)
                        if move:
                            record.import_log += cols[0] + '\t' + cols[4] + '\t -->  no ha sido modificado.\n'
                        else:
                            del domain[-1]
                            move = Move.search(domain)
                            if move:
                                lot = Lot.search([('number', '=', cols[4]), ('product', '=', cols[0])])
                                if lot:
                                    Move.write(move, {'lot': lot[0].id})
                                    record.import_log += cols[0] + '\t' + cols[
                                        4] + '\t --> Se ha modificado el lote.\n'
                                else:
                                    record.import_log += cols[0] + '\t' + cols[
                                        4] + '\t --> No se ha encontrado el lote.\n'
                            else:
                                del domain[-1]
                                if Move.search(domain):
                                    record.import_log += cols[0] + '\t' + cols[
                                        4] + '\t --> La ubicacion no concuerda.\n'
                                else:
                                    del domain[-1]
                                    if Move.search(domain):
                                        record.import_log += cols[0] + '\t' + cols[
                                            4] + '\t --> La cantidad no concuerda.\n'
                                    else:
                                        record.import_log += cols[0] + '\t' + cols[
                                            4] + '\t --> No se ha encontrado el movimiento.\n'
                    # if len(cols) > 6, the costumer, created different moves from one, so we have to
                    # modify the existent one and create others for evey 2 extra columns more. (Every 2 extra columns,
                    # refer the lot and the quantity of a new move).
                    elif len(cols) > 6:
                        if len(cols[4:]) % 2 == 0:
                            contador = 1
                            cantidad_total = 0
                            lines = []
                            while contador < len(cols[4:]):
                                cantidad_total += float(cols[4:][contador].replace(',', '.'))
                                lines.append({'lot': cols[4:][contador - 1],
                                              'quantity': float(cols[4:][contador].replace(',', '.'))})
                                contador += 2

                            location_id = Location.search([('code', '=', cols[2])]) if cols[2] else None
                            location_domain = ('from_location', '=', location_id[0].id) if location_id else ()
                            domain = [('production_input', '=', record.id), ('product.number', '=', cols[0]),
                                      ('quantity', '=', cantidad_total), location_domain]
                            move = Move.search(domain)
                            if move:
                                for line in lines:
                                    if line['lot']:
                                        lot_number = line['lot']
                                        lot = Lot.search([('number', '=', line['lot']), ('product', '=', cols[0])])
                                        if lot:
                                            line['lot'] = lot[0]
                                        else:
                                            record.import_log += cols[0] + '\t' + line[
                                                'lot'] + '\t --> lote no encontrado.\n'
                                    else:
                                        line['lot'] = None
                                        lot_number = ''
                                    if line == lines[0]:
                                        mensaje = cols[0] + '\t' + lot_number + '\t --> '
                                        if move[0].lot and line['lot'] and move[0].lot.number != str(
                                                line['lot'].number):
                                            mensaje += 'Se ha modificado el lote: ' + str(move[0].lot.number) + '.'

                                        if move[0].quantity != line['quantity']:
                                            mensaje += 'La cantidad ha pasado a ser de ' + str(
                                                move[0].quantity) + ' a ' + str(line['quantity'])
                                        Move.write(move, line)
                                        record.import_log += mensaje + '\n'
                                    else:
                                        Move.copy(move, line)
                                        record.import_log += cols[
                                                                 0] + '\t' + lot_number + '\t --> Se ha creado un nuevo movimiento.\n'
                            else:
                                lot_number = lines[0]['lot'] if lines else None
                                del domain[-1]
                                move = Move.search(domain)
                                if move:
                                    record.import_log += cols[
                                                             0] + '\t' + lot_number + '\t --> La ubicacion no concuerda.\n'
                                else:
                                    del domain[-1]
                                    if Move.search(domain):
                                        record.import_log += cols[
                                                                 0] + '\t' + lot_number + '\t --> La cantidad no concuerda.\n'
                                    else:
                                        record.import_log += cols[
                                                                 0] + '\t' + lot_number + '\t --> No se ha encontrado el movimiento\n'
            else:
                record.import_log += "La produccion en la que se encuentra, no coincide con la del archivo o bien, no" \
                                     "se encuentra en estado borrador/en espera."
            record.save()
        return 'end'

    @classmethod
    def process_purchase_request(cls, productions):
        """
        Set the max purchase line delivery date store to the production
        internal shipment and its moves.
        """
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        super(Production, cls).process_purchase_request(productions)
        for production in productions:
            if (not production.incoming_shipment
                    or not production.purchase_request):
                continue
            purchase_line = production.purchase_request.purchase_line
            if purchase_line and purchase_line.delivery_date_store:
                planned_date = purchase_line.delivery_date_store
                production.incoming_shipment.planned_date = planned_date
                production.incoming_shipment.planned_start_date = planned_date
                production.incoming_shipment.save()
                ShipmentInternal.wait([production.incoming_shipment])
    
    def _get_incoming_shipment_move(self, output, from_location, to_location):
        # TODO: It should be done in production_subcontract module.
        move = super(Production, self)._get_incoming_shipment_move(
            output, from_location, to_location)
        move.origin = self.purchase_request.purchase_line if \
            self.purchase_request else None
        return move

    @classmethod
    def compute_request(cls, product, warehouse, quantity, date, company, 
            order_point=None):
        Bom = Pool().get('production.bom')
        ProductBom = Pool().get('product.product-production.bom')
        bom_from_recalculation_needs_wizard = False
        context = Transaction().context
        production = super(Production, cls).compute_request(
            product, warehouse, quantity, date, company, order_point)
        # If bom is in context cause comes from recalculation_needs wizard use that bom
        if 'bom' in context and context['bom']:
            production.bom = context['bom']
            bom_from_recalculation_needs_wizard = True
        if production.bom:
            bom_versions = [bom.id for bom in production.bom.bom_versions]
            bom = Bom.search([
                ('id', 'in', bom_versions),
                ('active_version', '=', True)])
            production.bom = bom[0].id if bom else None
            if production.bom and bom_from_recalculation_needs_wizard:
                # We remake the transactions that have been made by other modules that are not ours,
                # using the bom that didn't come from the recalculation needs wizard
                production.subcontract_product = production.bom.subcontract_product
                # Search for the relation bom linked to the product to reset the route and operations
                if production.bom.master_bom:
                    linked_product_bom = ProductBom.search([('bom', '=', production.bom.master_bom)])
                    if linked_product_bom and linked_product_bom[0].route:
                        production.route = linked_product_bom[0].route
                        production.set_operations()
        return production

    @fields.depends('route', 'bom', 'planned_date')
    def on_change_bom(self):
        # Add the production route automatically when selecting the bom
        ProductProduction = Pool().get('product.product-production.bom')
        if not self.route and self.bom and self.bom.master_bom:
            bom = ProductProduction.search([('bom', '=', self.bom.master_bom)])
            self.route = bom[0].route.id if bom and bom[0].route else None
            self.on_change_route()
        super(Production, self).on_change_bom()

    @fields.depends('planned_date')
    def on_change_route(self):
        super(Production, self).on_change_route()
        for operation in self.operations:
            operation.deadline = self.planned_date

    @fields.depends('planned_date', 'operations')
    def on_change_planned_date(self):
        for operation in self.operations:
            operation.deadline = self.planned_date

    @classmethod
    @ModelView.button
    def create_purchase_request(cls, productions):
        Product = Pool().get('product.product')
        Request = Pool().get('purchase.request')
        requests = []
        super(Production, cls).create_purchase_request(productions)
        employee = Transaction().context.get('employee')
        for production in productions:
            purchase_request = production.purchase_request
            if purchase_request:
                purchase_request.requester = employee
                purchase_request.manual_delivery_date = production.planned_start_date
                purchase_request.unit_price = Product.get_purchase_product_unit_price(purchase_request)
                requests.append(purchase_request)
        if requests:
            Request.save(requests)

    def _split_production(self, number, quantity, uom, input2qty, output2qty):
        # in self, we have the original production
        # that function returns a copy of the original prod
        new_production = super(Production, self)._split_production(
            number, quantity, uom, input2qty, output2qty)
        new_production.supply_shipments = self.supply_shipments
        new_production.destination_warehouse = self.destination_warehouse
        new_production.save()
        self._split_operations(self.operations, new_production)
        # if input move product is serial_number split in moves of 1 qty each
        self._split_serial_number_inputs(new_production)
        return new_production

    def _split_serial_number_inputs(self, new_production):
        for _input in new_production.inputs:
            if _input.product and _input.product.serial_number and \
                    _input.quantity > 1.0:
                _input.split(1.0, _input.uom, _input.quantity)

    # This function is called everytime the wizard is creating a new production
    def _split_operations(self, operations_to_split, new_production):
        pool = Pool()
        Production = pool.get('production')
        Operation = pool.get('production.operation')
        Line = pool.get('production.operation.tracking')
        to_save = []

        # Search productions with self.number and '-' at the end followed by
        # any string (those are the productions generated by the wizard)
        # The original production is not renamed with the '-XX' until the end,
        # so this search will only return the productions which are being
        # created by the wizard
        pattern = str(self.number) + '-'
        productions = Production.search([('number', 'like', pattern+'%')])

        first_split = True
        if len(productions) > 1:
            first_split = False

        # Factor to determine the amount of quantity for each operation line
        # Only used in first splitted production, all the others will just take
        # the same values calculed in first call
        factor = new_production.quantity / self.quantity

        # delete all empty operations that have been copied
        # to recreate them with the correct quantity
        Operation.delete(new_production.operations)

        # First time the function is called, we iterate over self.operations
        # From second time to the last one, we just take the first generated
        # production values
        for operation in operations_to_split if first_split else productions[0].operations:
            # copy the operations and link them to the new prod, keeping state
            new_operation, = Operation.copy([operation], {
                'production': new_production.id,
                'state': operation.state,
            })

            # copy the lines of each operation of the original prod and
            # link them to the new prod, modifying the quantity
            for line in operation.lines:
                Line.copy([line], {
                    'operation': new_operation.id,
                    'quantity': (
                        round(line.quantity * factor, 2)
                        if first_split else round(line.quantity, 2)
                    ),
                })
            to_save.append(new_operation)

        # The original production needs a different logic to calculate its
        # value after splits, we do it once at the first time [APP-5693]
        if first_split:
            self._set_original_production_values(new_production)

        Operation.save(to_save)
        Production.save([new_production])

    def _set_original_production_values(self, production):
        """
        Sets the total quantity of the original production when executing the
        split wizard [APP-5693]
        :param self: original production
        :param production: generated production from the wizard
        """
        Line = Pool().get('production.operation.tracking')
        to_write = []

        # Gets the 'count' param of the split wizard from the context
        count = Transaction().context.get('count')

        # res_productions is the number of productions the wizard will
        # generate following its quantity
        res_productions = math.trunc(self.quantity / production.quantity)
        if count and count < res_productions:
            res_productions = count

        # new_quantity contains the decimal part of res_productions that is
        # lost doing math.trunc(), which its the quantity of the original
        # production
        new_quantity = self.quantity - (res_productions * production.quantity)
        # if its 0, means that is an exact split, all productions will have
        # exactly the same quantity
        if new_quantity == 0:
            new_quantity = production.quantity

        # Factor to determine the % to apply on the original production
        # operation lines based on its new quantity
        factor = new_quantity / self.quantity
        for x in self.operations:
            for line in x.lines:
                to_write.extend(([line], {
                    'quantity': round(line.quantity * factor, 2)}))
        if to_write:
            Line.write(*to_write)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        pool = Pool()
        Move = pool.get('stock.move')
        ShipmentInternal = pool.get('stock.shipment.internal')
        # When planned_start_date is modified in production, modify the supply shipments and the inputs of the productions
        # Also the moves of the supply shipments in the case that has origin as one input of the production
        for productions, values in zip(actions, actions):
            # If planned_start_date is modified will mean that only 1 production can arrive in productions list
            if 'planned_start_date' in values:
                modified_production_planned_start_date = values.get('planned_start_date')
                for production in productions:
                    Move.write([_input for _input in production.inputs], {
                        'planned_date': modified_production_planned_start_date,
                    })
                    Move.write([
                        m for s in production.supply_shipments
                        for m in s.moves if m.origin in production.inputs and m.state in ['draft', 'assigned']], {
                        'planned_date': modified_production_planned_start_date,
                    })
                    supply_shipments = []
                    for shipment in production.supply_shipments:
                        if shipment.state in ['request', 'draft', 'waiting', 'assigned']:
                            shipment.planned_date = modified_production_planned_start_date
                            shipment.planned_start_date = shipment.on_change_with_planned_start_date()
                            supply_shipments.append(shipment)
                    ShipmentInternal.save(supply_shipments)
        super(Production, cls).write(*args)

    @classmethod
    def done(cls, productions):
        pool = Pool()
        Move = pool.get('stock.move')
        to_save = []
        # Set unit_price a value cause when the move is passed to done, the operations
        # cost are added up to the unit_price of it, so of it's None it throws error
        for production in productions:
            for output in production.outputs:
                if not output.production_output.update_outputs_cost_price:
                    output.unit_price = Decimal('0.0')
                    to_save.append(output)
        if to_save:
            Move.save(to_save)
        super(Production, cls).done(productions)
        # The operations modify the unit_price of the outputs so set it to None
        for output in to_save:
            output.unit_price = None
        if to_save:
            Move.save(to_save)

    @staticmethod
    def default_update_outputs_cost_price():
        return True


class ProductionResetWizardStart(metaclass=PoolMeta):
    __name__ = 'production.reset.wizard.start'

    @classmethod
    def __setup__(cls):
        super(ProductionResetWizardStart, cls).__setup__()
        try:
            # make operations editable in the wizard
            # to decide cancelling operations or not
            cls.operations.readonly = False
        except KeyError:
            pass


class ProductionResetWizard(metaclass=PoolMeta):
    __name__ = "production.reset.wizard"

    def transition_reset(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Location = pool.get('stock.location')

        if self.record and self.record.state in ('running', 'done'):
            if self.record.outputs:
                for output in self.record.outputs:
                    if output.state == 'done':
                        if output.lot:
                            condition = ('lot', '=', output.lot),
                        else:
                            condition = ('product', '=', output.product),

                        post_moves = Move.search([
                            condition,
                            ('write_date', '>', output.write_date),
                            ('state', 'in', ('done', 'assigned')),
                        ])
                        if post_moves:
                            raise UserError(gettext(
                                'electrans.existent_later_moves',
                                production=self.record.number,
                                lot=output.lot.number if output.lot else '-'))

            locations_deactivated = Location.search([
                ('id', 'in', [_input.from_location.id for _input in
                    self.record.inputs if _input.state == "done" and
                    _input.from_location]),
                ('active', '=', False),
            ])

            if locations_deactivated:
                Location.write(locations_deactivated, {
                    'active': True
                })
            super(ProductionResetWizard, self).transition_reset()
        return 'end'


class ProductionReport(JasperReport):
    __name__ = 'production.report.electrans'


class ProductionXlsReport(JasperReport):
    __name__ = 'production.xls.report.electrans'


class ProductionProductReport(JasperReport):
    __name__ = 'production.product.report.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(product_inputs=True):
            return super(ProductionProductReport, cls).execute(ids, data)


class ProductionLocationReport(JasperReport):
    __name__ = 'production.location.report.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(location_inputs=True):
            return super(ProductionLocationReport, cls).execute(ids, data)


class BOMReport(JasperReport):
    __name__ = 'production.bom.report.electrans'


class ProductionElectransInternalStart(ModelView):
    'Production Electrans Internal Start'
    __name__ = 'production.electrans.internal.start'

    create_pallet = fields.Boolean('Create Pallet')
    to_location = fields.Many2One('stock.location', 'To Location',
                             domain=[
                                 ('type', 'in', ('warehouse', 'view', 'storage')),
                             ],
                             states={
                                 'invisible': Eval('create_pallet', False)
                             })
    split_by_planned_start_date = fields.Boolean('Split by planned start date')

    @classmethod
    def default_create_pallet(cls):
        return True


class ProductionElectransInternal(Wizard):
    'Production Electrans Internal'
    __name__ = 'production.electrans.internal'
    start = StateView('production.electrans.internal.start',
        'electrans.create_internal_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_internal', 'tryton-ok', default=True),
            ])
    create_internal = StateAction('stock.act_shipment_internal_form')

    def do_create_internal(self, action):
        pool = Pool()
        Location = pool.get('stock.location')
        Production = pool.get('production')
        productions = self.records

        if self.start.create_pallet:
            pallet = {}
            warehouse = productions[0].warehouse
            pallet['parent'] = warehouse.pallet_zone
            if not pallet['parent'] or not warehouse.pallet_sequence:
                raise UserError(gettext('electrans.no_movable_location_configuration'))
            pallet['name'] = pallet['code'] = warehouse.pallet_sequence.get()
            pallet['movable'] = True
            pallet['type'] = 'storage'
            pallet['provisioning_location'] = warehouse.pallet_zone.provisioning_location_used.id

            to_location, = Location.create([pallet])
        else:
            to_location = self.start.to_location
        split_by_planned_start_date = self.start.split_by_planned_start_date
        shipments = Production.create_internal_shipments(
            productions, to_location, split_by_planned_start_date)
        if shipments:
            data = {'res_id': [s.id for s in shipments]}
            if len(shipments) == 1:
                action['views'].reverse()
            return action, data


class OpenElectransShipmentInternal(Wizard):
    'Open Electrans Shipment Internal'
    __name__ = 'production.electrans.open_internal'
    start_state = 'open_'
    open_ = StateAction('stock.act_shipment_internal_form')

    def do_open_(self, action):
        pool = Pool()
        Internal = pool.get('stock.shipment.internal')

        domain = ['OR']
        for p in self.records:
            domain.append(('productions', '=', p.id))
        shipment_ids = [s.id for s in Internal.search(domain)]

        encoder = PYSONEncoder()
        action['pyson_domain'] = encoder.encode([('id', 'in', shipment_ids)])
        action['pyson_search_value'] = encoder.encode([])
        return action, {}


class ProductionSerialNumberSplit(Wizard):
    'Production Serial Number Split'
    __name__ = 'production.serial_number_split'

    start = StateView('production.serial_number_split_start',
        'electrans.production_serial_number_split_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Split', 'split', 'tryton-ok', default=True),
            ])
    split = StateTransition()

    def transition_split(self):
        """
        Split production into two.
        The original one, will contain as outputs, the serial numbers selected
        on wizard "ProductionSerialNumberSplitStart"
        And the other one, will contain the rest.
        The inputs will be accord with the percent of the outputs selected.
        """
        Production = Pool().get('production')
        Move = Pool().get('stock.move')
        p1 = self.record
        outputs = p1.outputs

        # To split the production we use the split function defined in module
        # production_split.
        p1.split(self.start.production_quantity -
                 len(self.start.serial_numbers), self.start.uom, count=1)
        serial_numbers = [number.id for number in self.start.serial_numbers]

        p2, = Production.search([(
            'number', '=', p1.number[:-1] + str(int(p1.number[-1])+1))])

        # Change the outputs of both productions to keep the selected ones in
        # the wizard, to the original production.
        for output in outputs:
            if output.id in serial_numbers:
                output.production_output = p1.id
            else:
                output.production_output = p2.id
        Move.save(outputs)
        return 'end'


class ProductionSerialNumberSplitStart(ModelView):
    'Production Serial Number Split Start'
    __name__ = 'production.serial_number_split_start'

    quantity = fields.Function(fields.Integer('Selected Quantity'),
                               'on_change_with_quantity')
    production_quantity = fields.Float('Total Quantity', readonly=True)
    serial_numbers = fields.One2Many('stock.move', 'serial_number_split', 'Serial Numbers',
                                     add_remove=[('production_output', 'in', Eval('context', {}).get('active_ids')),
                                                 ('lot', '!=', None)])
    uom = fields.Many2One('product.uom', 'Uom', readonly=True)

    @fields.depends('serial_numbers')
    def on_change_with_quantity(self, name=None):
        return 0 if not self.serial_numbers else len(self.serial_numbers)

    @staticmethod
    def default_production_quantity():
        active_id = ProductionSerialNumberSplitStart.get_active_id()
        return active_id.quantity if active_id else None

    @staticmethod
    def default_uom():
        active_id = ProductionSerialNumberSplitStart.get_active_id()
        return active_id.uom.id if active_id else None

    @staticmethod
    def get_active_id():
        if Transaction().context.get('active_ids'):
            Production = Pool().get('production')
            production = Production.browse(Transaction().context['active_ids'])
            if production and production[0].uom:
                return production[0]


class ProductionElectransModifyFromLocationStart(ModelView):
    'Production Electrans Internal Start'
    __name__ = 'production.electrans.from_location.start'

    warehouse = fields.Many2One('stock.location', 'Warehouse')
    location = fields.Many2One('stock.location', 'Location',
        domain=[('parent', 'child_of', Eval('warehouse'))],
        required=True,
        depends=['warehouse'])

    @staticmethod
    def default_warehouse():
        Production = Pool().get('production')
        active_id = Transaction().context.get('active_id')
        if active_id:
            prod = Production(active_id)
            return prod.warehouse.id if prod and prod.warehouse else None


class ProductionElectransModifyFromLocation(Wizard):
    'Production Electrans Internal'
    __name__ = 'production.electrans.from_location'

    start = StateView('production.electrans.from_location.start',
        'electrans.production_from_location_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Modify', 'modify', 'tryton-ok', default=True),
            ])
    modify = StateTransition()

    def transition_modify(self):
        pool = Pool()
        Move = pool.get('stock.move')
        to_modify = []
        for production in self.records:
            for _input in production.inputs:
                if _input.state == 'draft':
                    to_modify.append(_input)
        Move.write(to_modify, {'from_location': self.start.location.id})
        return 'end'


class OpenBOMTreeStart(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open.start'

    show_weight = fields.Boolean(
        "Show Weight",
        help="If marked, weight is shown instead of the amount.")


class BomExcelReport(JasperReport):
    __name__ = 'production_bom_excel.report'

    @classmethod
    def execute(cls, ids, data):
        def get_bom_data(boms, lvl=1, show_weight=False, no_stock_count=None):
            def get_last_unit_cost_price(last_prod_line):
                """ To get the unit price of a given last purchase line
                    of a product and if that unit price doesn't have uom
                    with factor 1, make a conversion to get it """
                pool = Pool()
                Uom = pool.get('product.uom')
                last_l_prod = last_prod_line and last_prod_line.product
                last_line_factor_not_default = (
                        last_prod_line and last_prod_line.unit
                        and last_prod_line.unit.factor != 1)
                prod_uom_factor_is_default = (
                        last_prod_line and last_l_prod.default_uom and
                        last_l_prod.default_uom.factor == 1)
                # if unit is not the one with
                # factor 1 recalculate last_unit_price
                if last_line_factor_not_default:
                    # if the product default uom is factor 1
                    # else search for the uom that is factor 1
                    if prod_uom_factor_is_default:
                        to_uom = last_prod_line.product.default_uom
                    else:
                        to_uom = Uom.search([
                            ('factor', '=', 1),
                            ('category', '=', last_prod_line.unit.category)])
                        to_uom = to_uom[0] if to_uom else None

                    last_unit_cost_price = Uom.compute_price(
                        last_prod_line.unit, last_prod_line.unit_price, to_uom)
                else:
                    last_unit_cost_price = last_prod_line.unit_price \
                        if last_prod_line else 0
                return last_unit_cost_price

            # If show_weight is True, unit and total weight are calculated
            Uom = pool.get('product.uom')
            PurchaseLine = pool.get('purchase.line')
            InvoiceLine = pool.get('account.invoice.line')
            # TODO: get the uom from the configuration
            weight_uom = Uom(3)
            total_import = 0
            last_total_import = 0
            records = []
            for bom in boms:
                last_date = None
                record = {}
                record['show_weight'] = show_weight
                record['no_stock_count'] = no_stock_count
                if no_stock_count is not None and not no_stock_count:
                    record['input_stock'] = bom['input_stock']
                    record['output_stock'] = bom['output_stock']
                    record['current_stock'] = bom['current_stock']
                product = Product(bom['product'])
                uom = Uom(bom['uom'])
                record['name'] = '.  ' * lvl + product.name
                record['code'] = product.code
                product_prod = product.id
                record['uom'] = uom.symbol
                record['quantity'] = Decimal(str(bom['quantity']))

                records.append(record)
                # if the record contains children, his cost is
                # only the subcontract product cost
                if bom['childs']:
                    aux = Bom(bom['bom']) if bom['bom'] else None
                    record['document'] = aux.document if aux else ""
                    record['bom'] = aux.rec_name if aux else ""
                    if bom['subcontract_product']:
                        sdc = Product(bom['subcontract_product'])
                        record['subcontract_product'] = sdc.rec_name
                        if sdc.cost_price:
                            record['subcontract_cost_price'] = sdc.cost_price
                            record['import'] = (sdc.cost_price *
                                                record['quantity'])
                            total_import += record['import']
                            record['last_import'] = (sdc.cost_price *
                                                     record['quantity'])
                            last_total_import += record['last_import']

                    record['have_childs'] = True
                    child_record, childs_import, childs_last_import = \
                        get_bom_data(bom['childs'], lvl + 1, show_weight, no_stock_count)
                    records.extend(child_record)
                    total_import += childs_import
                    last_total_import += childs_last_import
                else:
                    if show_weight and product.weight:
                        record['unit_weight'] = Uom.compute_qty(
                            product.weight_uom, product.weight, weight_uom)
                        record['weight'] = Uom.compute_qty(
                            product.weight_uom, product.weight *
                            bom['quantity'], weight_uom)
                    else:
                        if product.cost_price:
                            record['unit_cost_price'] = product.cost_price
                            if product.purchasable:
                                last_line = PurchaseLine.get_last_line(
                                    product_prod, 'from_bom_excel')
                                record['last_unit_cost_price'] = \
                                    get_last_unit_cost_price(last_line)
                                # Get last purchase or invoice date
                                if isinstance(last_line, InvoiceLine):
                                    last_date = (
                                        last_line.invoice.invoice_date
                                        if last_line.invoice else None)
                                elif isinstance(last_line, PurchaseLine):
                                    last_date = (
                                        last_line.purchase.purchase_date
                                        if last_line.purchase else None)
                                record['last_date'] = (
                                    last_date.strftime('%d/%m/%Y')
                                    if last_date else None)
                                record['last_import'] = \
                                    record['last_unit_cost_price'] * \
                                    Decimal(bom['quantity'])
                                last_total_import += record['last_import']

                            record['import'] = \
                                product.cost_price * Decimal(bom['quantity'])
                            total_import += record['import']

                    record['have_childs'] = False
            return records, total_import, last_total_import

        pool = Pool()
        Product = pool.get('product.product')
        Bom = pool.get('production.bom')
        DocEdition = pool.get('production.document.edition')
        child_records, total, last_total = get_bom_data(
            data['bom_tree'], show_weight=data['show_weight'],
            no_stock_count=data['no_stock_count'] if 'no_stock_count' in data else None)

        bom = Bom(data['bom_tree'][0]['bom']) if \
            data['bom_tree'] and data['bom_tree'][0]['bom'] else None
        document_edition = DocEdition(bom.production_doc_edition) if \
            bom and bom.production_doc_edition else None
        header = {'product': data['header']['product'],
                  'warehouse': data['header']['warehouse'],
                  'date': data['header']['date'], 'total_import': total,
                  'last_total_import': last_total,
                  'document': document_edition.rec_name
                  if document_edition else ''}
        records = child_records
        records[0].update(header)
        return super(BomExcelReport, cls).execute(ids, {
            'name': 'production_bom_excel.report',
            'data_source': 'records',
            'records': records,
        })


class BomExcel(Wizard, metaclass=PoolMeta):
    __name__ = 'production.bom.excel'

    start = StateView('production.bom.tree.open.start',
        'production.bom_tree_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'print_', 'tryton-ok', True),
            ])

    print_ = StateReport('production_bom_excel.report')

    # TODO: It's the same code as default_start on production.bom.tree.open but I don't know if there is aa way to call
    #  it, because the class only extend from Wizard.
    def default_start(self, fields):
        pool = Pool()
        Product = pool.get(Transaction().context['active_model'])
        Location = pool.get('stock.location')
        Date = pool.get('ir.date')
        defaults = {}
        if Transaction().context['active_model'] == 'product.product':
            product = Product(Transaction().context['active_id'])
        else:
            product = Product(Transaction().context['active_id']).products[0]
        defaults['category'] = product.default_uom.category.id
        if getattr(self.start, 'uom', None):
            defaults['uom'] = self.start.uom.id
        else:
            defaults['uom'] = product.default_uom.id
        defaults['product'] = product.id
        if getattr(self.start, 'bom', None):
            defaults['bom'] = self.start.bom.id
        elif product.boms:
            defaults['bom'] = product.boms[0].id
        defaults['quantity'] = getattr(self.start, 'quantity', 1)
        defaults['date'] = Date.today()
        warehouse = Location.search([('code', '=', 'WH')])
        defaults['warehouses'] = [x.id for x in warehouse] if warehouse else []
        return defaults

    def do_print_(self, action):
        pool = Pool()
        BomTree = pool.get('production.bom.tree.open.tree')
        with Transaction().set_context(
                tree_date=self.start.date, stock_date_end=self.start.date,
                locations=[self.start.warehouses[0].id], planning=True):
            tree = BomTree.tree(
                self.start.bom.bom, self.start.product,
                self.start.quantity, self.start.uom)
        header = {
            'warehouse': self.start.warehouses[0].name,
            'date': self.start.date,
            'product': self.start.product.rec_name}
        data = {
            'header': header,
            'bom_tree': tree['bom_tree']
            }
        data['show_weight'] = self.start.show_weight
        return action, data

    def transition_print_(self):
        return 'end'


class ProductionLabelReport(JasperReport):
    __name__ = 'production.label'


class SplitProduction(metaclass=PoolMeta):
    'Split Production'
    __name__ = 'production.split'

    def transition_split(self):
        Production = Pool().get('production')
        with Transaction().set_context(count=self.start.count):
            for operation in self.record.operations:
                if operation.state == 'running':
                    raise UserError(gettext('electrans.operations_running'))
            if not self.record.number:
                Production.set_number([self.record])
            return super(SplitProduction, self).transition_split()


class OneStepDoProductionStart(ModelView):
    'Production One Step Do Start'
    __name__ = 'production.one_step_do.start'
    productions_number = fields.Char('Productions Number', readonly=True)


class OneStepDoProduction(Wizard):
    'Productions One Step Do'
    __name__ = 'production.one_step_do'
    start = StateView('production.one_step_do.start',
                      'electrans.production_one_step_do_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Do', 'one_step_do', 'tryton-ok',
                                 default=True),
                      ])
    one_step_do = StateTransition()

    def default_start(self, fields):
        defaults = {}
        defaults['productions_number'] = ', '.join([record.number for
                                                    record in self.records])
        return defaults

    def transition_one_step_do(self):
        pool = Pool()
        Date = pool.get('ir.date')
        Warning = pool.get('res.user.warning')
        today_cache = {}

        def in_future(move):
            if move.company not in today_cache:
                with Transaction().set_context(company=move.company.id):
                    today_cache[move.company] = Date.today()
            today = today_cache[move.company]
            if move.effective_date and move.effective_date > today:
                return move

        pool = Pool()
        Production = pool.get('production')
        to_assign = []
        to_run = []
        to_do = []
        if self.records:
            for production in self.records:
                moves = [move for move in production.inputs
                         + production.outputs if move.state != 'cancelled']
                future_moves = sorted(filter(in_future, moves))
                if future_moves:
                    names = ', '.join(m.rec_name for m in future_moves[:5])
                    if len(future_moves) > 5:
                        names += '...'
                    warning_name = Warning.format(
                        'effective_date_future', future_moves)
        # Show that warning now  if there are moves with future effective date
        # because we skip all the warnings that could be shown
        # when passing through the production states
                    if Warning.check(warning_name):
                        raise MoveFutureWarning(
                            warning_name, gettext(
                                'stock.msg_move_effective_date_in_the_future',
                                moves=names))
                if production.state == 'waiting':
                    to_assign.append(production)
                elif production.state == 'assigned':
                    to_run.append(production)
                elif production.state == 'running':
                    to_do.append(production)
            with Transaction().set_context({'_skip_warnings': True}):
                Production.assign_try(to_assign)
                to_run += [p for p in to_assign if p.state == 'assigned']
                Production.run(to_run)
                Production.done(to_run + to_do)
        return 'end'
