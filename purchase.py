#TODO: [APP-954] Mejoras varias en purchase_line
# - Unificar históricos de líneas de compras y líneas de compra
# - Revisar fecha confirmada compra (Rosa la pone en movimientos) vs fecha requerida
# - Valorar añadir pestañas (dominios) a la vista tree
# - Otros TODOs en el código

# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.pool import Pool, PoolMeta
from trytond.config import config
from trytond.model import ModelView, Workflow, fields
import datetime
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.ir.attachment import AttachmentCopyMixin
from trytond.modules.purchase.purchase import get_shipments_returns, search_shipments_returns
from trytond.modules.purchase_request.purchase import process_request

DIGITS = config.getint('digits', 'unit_price_digits', 4)

__all__ = ['Purchase', 'PurchaseLine', 'PurchaseRequisition',
            'PurchaseRequisitionLine', 'ApprovalRequest',
            'HandleInvoiceException', 'HandleShipmentException']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    shipments_internals = fields.Function(fields.One2Many('stock.shipment.internal', None, 'Internals Shipments'),
                                          'get_shipments_internals', searcher='search_shipments_internals')

    get_shipments_internals = get_shipments_returns('stock.shipment.internal')
    search_shipments_internals = search_shipments_returns('stock.shipment.internal')
    internal_notes = fields.Text("Internal Notes")
    internal_notes_oneline = fields.Function(
        fields.Char('Internal Notes'),
        'get_internal_notes_oneline',
        searcher='search_internal_notes_oneline')
    comment_oneline = fields.Function(
        fields.Char('Comment'),
        'get_comment_oneline',
        searcher='search_comment_oneline')
    user_phone = fields.Function(
        fields.Char("Create User phone"),
        'get_user_phone')

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        try:
            cls._buttons['quote']['pre_validate'].remove(('purchase_date', '!=', None))
        except ValueError:
            print("******Remove obsolete code at:")
            import traceback
            print(traceback.format_exc().splitlines())

        cancelled = ('cancelled', 'Cancelled')
        if cancelled not in cls.shipment_state.selection:
            cls.shipment_state.selection.append(cancelled)
        cls.approval_group.states['required'] = Eval('state') == 'quotation'
        cls.invoice_state.selection.extend([('posted', 'Posted')])
        cls.warehouse.required = True

    @staticmethod
    def default_purchase_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def search_rec_name(cls, name, clause):
        res = super(Purchase, cls).search_rec_name(name, clause)
        return ['OR',
                res,
                ('party',) + tuple(clause[1:])
                ]

    @classmethod
    @ModelView.button
    @Workflow.transition('quotation')
    def quote(cls, purchases):
        PurchaseLine = Pool().get('purchase.line')
        super(Purchase, cls).quote(purchases)
        for purchase in purchases:
            purchase.check_for_quotation()
            if not purchase.purchase_date:
                purchase.purchase_date = datetime.date.today()
            # assign sequence number to each line
            pos = 1
            for line in purchase.lines:
                if line.type == 'line':
                    line.sequence = pos
                    pos += 1
            PurchaseLine.save(purchase.lines)
        cls.save(purchases)

    @classmethod
    @ModelView.button
    def process(cls, purchases):
        transaction = Transaction()
        pool = Pool()
        Move = pool.get('stock.move')
        PurchaseLine = pool.get('purchase.line')
        # Do not create moves when the line have 'do_not_move' check to True
        if not transaction.context.get('skip', None):
            super(Purchase, cls).process(purchases)
            purchase_ids = [p.id for p in purchases]
            lines = PurchaseLine.search([('purchase', 'in', purchase_ids), ('do_not_move', '=', True)])
            if lines:
                # When a move is deleted, is executed the purchase process method again, and the moves get recreated so
                # I use the context to skip it
                with transaction.set_context(skip=True):
                    Move.delete(Move.search([('origin', 'in', [str(l) for l in lines]), ('state', '=', 'draft')]))
            # Save line state if has changed
            for purchase in purchases:
                lines_to_save = []
                for line in purchase.lines:
                    state = line.get_line_state()
                    if line.line_state != state:
                        line.line_state = state
                        lines_to_save.append(line)
                PurchaseLine.save(lines_to_save)

    def get_shipment_state(self):
        if self.moves and all(m.state == 'cancelled' for m in self.moves) and \
                not any(l.moves_exception for l in self.lines):
            return 'cancelled'

        return super(Purchase, self).get_shipment_state()

    def _get_approval_request(self):
        request = super(Purchase, self)._get_approval_request()
        request.untaxed_amount = self.untaxed_amount
        request.party = self.party.id
        return request

    def get_invoice_state(self):
        '''
        Return the invoice state for the purchase.
        '''
        state = super(Purchase, self).get_invoice_state()
        if state == 'waiting':
            skip_ids = set(x.id for x in self.invoices_ignored)
            skip_ids.update(x.id for x in self.invoices_recreated)
            invoices = [i for i in self.invoices if i.id not in skip_ids]
            if invoices:
                if all(i.state == 'posted' for i in invoices):
                    return 'posted'
        return state

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    @process_request
    def draft(cls, purchases):
        super(Purchase, cls).draft(purchases)

    def get_user_phone(self, name=None):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        phone = ""
        if user and user.employee and user.employee.party:
            contact = user.employee.party.contact_mechanism_get(types='phone')
            phone = contact.value if contact else ""
        return phone

    def get_internal_notes_oneline(self, name=None):
        # Remove line break of the internal notes that starts with it
        notes = self.internal_notes[1:] if self.internal_notes and \
            self.internal_notes[:1] == '\n' else self.internal_notes
        return notes.replace('\n', '. ') if notes else ''

    @classmethod
    def search_internal_notes_oneline(cls, name, clause):
        return [('internal_notes',) + tuple(clause[1:])]

    def get_comment_oneline(self, name=None):
        return self.comment.replace('\n', '. ') if self.comment else ''

    @classmethod
    def search_comment_oneline(cls, name, clause):
        return [('comment',) + tuple(clause[1:])]


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    line_state = fields.Selection([
            ('draft', 'Draft'),
            ('quotation', 'Quotation'),
            ('confirmed', 'Confirmed'),
            ('cancelled', 'Cancelled'),
            ('processing', 'Processing'),
            ('done', 'Done'),
            ('waiting', 'Waiting'),
            ('received', 'Received'),
            ('waiting to process!', 'Waiting to process!'),
            ('waiting to invoice', 'Waiting to invoice'),
            ('invoiced', 'Invoiced'),
            ('', "Unknown")],
            "Line State",
            help="Purchase state (Draft/Quotation/Confirmed/Cancelled)"
                 "in case of Processed, shipment state (Waiting/Invoiced/Received/Cancelled)")
    shipment_state = fields.Function(
        fields.Selection([
            ('none', 'None'),
            ('waiting', 'Waiting'),
            ('received', 'Received'),
            ('partially received', 'Partially received'),
            ('exception', 'Exception'),
            ], "Shipment State"),
        'get_shipment_state')
    invoice_state = fields.Function(
        fields.Selection([
            ('none', 'None'),
            ('waiting', 'Waiting'),
            ('posted', 'Posted'),
            ('paid', 'Paid'),
            ('exception', 'Exception'),
            ], "Invoice State"),
        'get_invoice_state')
    # The field quantity already exists, but I want to show it without decimals, so I create a new one
    ordered_quantity = fields.Function(
        fields.Float("Ordered Quantity", digits=(16, 0)),
        'get_ordered_quantity')
    # The field unit already exists, but I want to show only its symbol, so I create a new one
    ordered_quantity_unit = fields.Function(
        fields.Char("Ordered Quantity Unit"),
        'get_ordered_quantity_unit')
    received_quantity = fields.Function(
        fields.Float("Received Quantity", digits=(16, 0)),
        'get_in_state_quantity')
    pending_quantity = fields.Function(
        fields.Float("Pending Quantity", digits=(16, 0)),
        'get_in_state_quantity')
    canceled_quantity = fields.Function(
        fields.Float("Cancelled Quantity", digits=(16, 0)),
        'get_in_state_quantity')
    invoiced_quantity = fields.Function(
        fields.Float("Invoiced Quantity", digits=(16, 0)),
        'get_in_state_quantity')
    # The field quantity already exists, but I want to show it with 4 decimals, so I create a new one
    unit_price_4decs = fields.Function(
        fields.Numeric("Unit Price", digits=(16, 4)),
        'get_unit_price_4decs')
    purchase_supplier = fields.Function(
        fields.Char("Purchase Supplier"),
        'get_purchase_supplier',
        searcher='search_purchase_supplier')
    # When the purchase product is a service, the product to be delivered
    # TODO: Implement searcher
    # TODO: What if there are more than one (rare but it could be)
    delivery_good = fields.Function(
        fields.Many2One(
            'product.product', "Delivery Good",
            states={'invisible': (Eval('product') == Eval('delivery_good')) | (~Bool(Eval('delivery_good')))},
            depends=['product', 'delivery_good']),
        'get_delivery_good')
    invoice_unit_price = fields.Function(
        fields.Numeric(
            "Invoice Unit Price",
            digits=(16, 4)),
        'get_invoice_unit_price')
    # If True, the movement is not created
    do_not_move = fields.Boolean(
        "Do not move",
        states={
            'invisible': Eval('product_type') == 'service',
            'readonly': Eval('purchase_state') != 'draft'},
        depends=['product_type', 'purchase_state'])
    product_type = fields.Function(
        fields.Char("Product Type"),
        'on_change_with_product_type')

    @classmethod
    def __setup__(cls):
        super(PurchaseLine, cls).__setup__()
        if not cls.product.help:
            cls.product.help = "Good or service purchased"
        cls.delivery_date.states['invisible'] = False
        cls.delivery_date_store.states['invisible'] = Eval('type') != 'line'
        cls.delivery_date_edit.states['invisible'] = True

    def get_rec_name(self, name):
        description_first_line = self.description \
            if self.description.find('\n') == -1 \
            else self.description[:self.description.find('\n')] + '...'
        return '{} Pos.{}: {}{} {}'.format(
            self.purchase.number if self.purchase else '',
            self.sequence if self.sequence else '',
            self.quantity if self.quantity else '',
            self.unit.symbol if self.unit else '',
            description_first_line
            )

    @classmethod
    def search_rec_name(cls, name, clause):
        res = super(PurchaseLine, cls).search_rec_name(name, clause)
        return ['OR',
                res,
                ('product',) + tuple(clause[1:])]

    @classmethod
    def default_delivery_date_edit(cls):
        return True

    def update_price_to_last_purchase(self):
        """
        Update product's price to last purchase price if there's no
        product_supplier price or product price list defined
        """
        pool = Pool()
        ProductPriceListLine = pool.get('product.price_list.line')
        if self.product:
            price_list_product = ProductPriceListLine.search(
                [('price_list', '=',
                  self.purchase.party.purchase_price_list
                  if self.purchase and self.purchase.party
                  else None),
                 ('product', '=', self.product), ])
            if (self.get_product_supplier() or
                    (price_list_product and self.unit_price and
                     self.gross_unit_price)):
                return

            last_line = self.get_last_line(
                self.product, self.purchase.party if self.purchase else None)

            if last_line:
                self.gross_unit_price = last_line.gross_unit_price
                self.discount = last_line.discount
            else:
                self.gross_unit_price = Decimal('0.0')
                self.discount = Decimal(0)
            self.update_prices()

    @fields.depends('note', 'product', 'description')
    def on_change_product(self):
        super(PurchaseLine, self).on_change_product()
        self.update_price_to_last_purchase()

    def on_change_quantity(self):
        super(PurchaseLine, self).on_change_quantity()
        return self.update_price_to_last_purchase()

    def get_product_supplier(self):
        for ps in self.product.template.product_suppliers:
            if ps.product == self.product.template \
                    and ps.party == self.purchase.party \
                    and ps.prices:
                return True
        return False

    @classmethod
    def get_last_line(cls, product, party):
        """
        Returns the last line modified
        it can be a purchase line or an invoice line
        """
        PLine = Pool().get('purchase.line')
        ILine = Pool().get('account.invoice.line')

        if party == 'from_bom_excel':
            p_line_search = PLine.search([
                ('product', '=', product),
                ('type', '=', 'line'),
                ('purchase.state', '=', 'processing')],
                order=[('purchase.purchase_date', 'DESC')],
                limit=1)
            invoice_line_search = ILine.search([
                ('product', '=', product),
                ('invoice_type', '=', 'in'),
                ('type', '=', 'line'),
                ('invoice.state', 'in', ['posted', 'paid'])],
                order=[('invoice.invoice_date', 'DESC')],
                limit=1)
        else:
            p_line_search = PLine.search([
                ('product', '=', product),
                ('purchase.party', '=', party),
                ('type', '=', 'line'),
                ('purchase.state', '=', 'processing')],
                order=[('purchase.purchase_date', 'DESC')],
                limit=1)
            invoice_line_search = ILine.search([
                ('product', '=', product),
                ('party', '=', party),
                ('invoice_type', '=', 'in'),
                ('type', '=', 'line'),
                ('invoice.state', 'in', ['posted', 'paid'])],
                order=[('invoice.invoice_date', 'DESC')],
                limit=1)
        last_purchase_line = p_line_search[0] if p_line_search else []
        last_invoice_line = invoice_line_search[0] if invoice_line_search else []
        # Return invoice_line if is more recent or the same as the
        # purchase_line or there is not purchase_line
        if last_invoice_line and (
                not last_purchase_line or last_invoice_line.invoice.invoice_date >=
                last_purchase_line.purchase.purchase_date):
            return last_invoice_line
        # Return purchase_line if is more recent than the invoice_line or
        # there is not invoice_line
        elif last_purchase_line and (
                not last_invoice_line or last_purchase_line.purchase.purchase_date >
                last_invoice_line.invoice.invoice_date):
            return last_purchase_line
        return

    def get_line_state(self, name=None):
        if self.type != 'line':
            return ''
        if self.purchase.state != 'processing':
            return self.purchase.state
        if self.product and \
                (self.product.type in ('goods', 'assets') or (self.product.type == 'service' and self.moves)):
            invoice_state = self.get_invoice_state()
            if self.canceled_quantity + self.received_quantity + self.pending_quantity < self.quantity:
                if invoice_state in ['posted', 'paid']:
                    return 'invoiced'
                elif invoice_state == 'waiting':
                    return 'waiting to invoice'
                else:
                    return 'waiting to process!'
            elif self.pending_quantity > 0:
                return 'waiting'
            elif self.received_quantity > 0:
                if self.invoiced_quantity >= self.received_quantity or (invoice_state in ['posted', 'paid']):
                    return 'invoiced'
                else:
                    return 'received'
            elif self.canceled_quantity > 0:
                return 'cancelled'
            else:
                return ''  # Impossible case!
        else:  # type=='service' and no moves
            if self.invoiced_quantity < self.canceled_quantity:
                return 'waiting to process!'
            if self.invoiced_quantity < self.quantity:
                return 'waiting to invoice'
            else:
                return 'invoiced'

    def get_shipment_state(self, name):
        """Return the shipment state for the purchase line"""
        pool = Pool()
        Move = pool.get('stock.move')
        moves = Move.search([('origin', '=', str(self))])
        if moves:
            if self.moves_exception:
                return 'exception'
            elif self.moves_progress == 1:
                return 'received'
            elif self.pending_quantity and self.pending_quantity < self.ordered_quantity:
                return "partially received"
            else:
                return 'waiting'
        return 'none'

    def get_move_done(self, name):
        """ if the purchase line have a related delivery good, check the related moves state instead of the
        remaining quantity, because it is always 0."""
        res = super(PurchaseLine, self).get_move_done(name)
        if res and self.delivery_good:
            if not all(move.state == 'done' for move in self.moves):
                res = False
        return res

    def get_invoice_state(self, name=None):
        """Return the invoice state for the purchase line"""
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        if self.purchase:
            invoice_lines = InvoiceLine.search([('origin', '=', str(self))])
            if invoice_lines and all(line.invoice for line in invoice_lines):
                skip_ids = set(x.id for x in self.purchase.invoices_ignored)
                skip_ids.update(x.id for x in self.purchase.invoices_recreated)
                invoices = [l.invoice for l in invoice_lines if l.invoice and (l.invoice.id not in skip_ids)]
                if any(i.state == 'cancelled' for i in invoices):
                    return 'exception'
                elif all(i.state == 'posted' for i in invoices):
                    return 'posted'
                elif all(i.state == 'paid' for i in invoices):
                    return 'paid'
                else:
                    return 'waiting'
        return 'none'

    def get_ordered_quantity(self, name):
        return self.quantity

    def get_ordered_quantity_unit(self, name):
        if self.unit:
            return self.unit.symbol

    def get_unit_price_4decs(self, name):
        return self.unit_price

    def get_in_state_quantity(self, name):
        state = ''
        pool = Pool()
        Uom = pool.get('product.uom')
        quantity = 0
        if name == 'invoiced_quantity':
            for i in self.invoice_lines:
                if i.invoice and i.invoice.state in ['posted', 'paid']:
                    if self.unit and i.unit and self.unit.category.id == i.unit.category.id:
                        quantity += Uom.compute_qty(i.unit, i.quantity, self.unit)
                    else:
                        quantity += i.quantity
            return quantity
        elif name == 'pending_quantity':
            state = 'draft'
        elif name == 'canceled_quantity':
            state = 'cancelled'
        elif name == 'received_quantity':
            state = 'done'
        for m in self.moves:
            if m.state == state:
                if self.unit and m.uom and self.unit.category.id == m.uom.category.id:
                    quantity += Uom.compute_qty(m.uom, m.quantity, self.unit)
                else:
                    quantity += m.quantity
        return quantity

    def get_purchase_supplier(self, name):
        return self.purchase.party.name

    def get_delivery_good(self, name):
        pool = Pool()
        if self.product and self.product.type == 'service':
            Request = pool.get('purchase.request')
            requests = Request.search([
                ('purchase_line.id', '=', str(self.id))
            ], limit=1)
            if requests:
                req, = requests
                Production = pool.get('production')
                productions = Production.search([
                    ('purchase_request.id', '=', str(req.id))
                ], limit=1)
                if productions:
                    f, = productions
                    if f.outputs:
                        return f.outputs[0].product.id
        return

    # OQA 18/04/18: every time we search by received_quantity or pending_quantity it calls search_field_state_quantity function
    @classmethod
    def search_purchase_supplier(cls, name, clause):
        return [('purchase.party.name',) + tuple(clause[1:])]

    def get_invoice_unit_price(self, name):
        InvoiceLine = Pool().get('account.invoice.line')
        invoice_lines = InvoiceLine.search([
            ('origin', '=', 'purchase.line,' + str(self.id)),
            ('invoice', '!=', None)
        ])
        # ], order=['write_date','DESC')], limit=1)
        # I do this way because write_date can be null and in this case I have to take
        # create_date (and I don't know how to do a coalesce in search function)
        invoice_lines.sort(key=lambda x: (x.write_date or x.create_date), reverse=True)
        if invoice_lines:
            return invoice_lines[0].unit_price
        return

    @fields.depends('product')
    def on_change_with_product_type(self, name=None):
        if self.product and self.product.template:
            return self.product.template.type

    def _get_invoice_line_quantity(self):
        """If the do_not_move field is True, the quantity to invoice have to be
        the whole purchase line quantity because the product is never coming"""
        return self.quantity if self.do_not_move else \
            super(PurchaseLine, self)._get_invoice_line_quantity()

    @classmethod
    def update_purchase_line(cls, request, line):
        Product = Pool().get('product.product')
        # Do not update prices when the request have a linked purchase request
        # quotation
        if not request.best_quotation_line:
            line.gross_unit_price = Product.get_purchase_product_unit_price(
                request)
            line.discount = Product.get_purchase_product_discount(request)
            line.update_prices()
        line.delivery_date_edit = True
        line.delivery_date_store = request.manual_delivery_date


class PurchaseRequisition(AttachmentCopyMixin, metaclass=PoolMeta):
    __name__ = 'purchase.requisition'

    purpose = fields.Char('Purpose', states={
        'readonly': Eval('state') != 'draft'})
    comment = fields.Text('Comment', states={
        'readonly': Eval('state') != 'draft'},
        help="This comment will be in the purchase request.")

    @classmethod
    @ModelView.button
    def wait(cls, requisitions):
        super().wait(requisitions)
        cls.approve(requisitions)
        cls.proceed(requisitions)
        cls.process(requisitions)
    
    @classmethod
    def get_resources_to_copy(cls, name):
        return {
            'purchase.request'
            }

    def copy_resources_to(self, target):
        pool = Pool()
        PurchaseRequest = pool.get('purchase.request')
        Attachment = pool.get('ir.attachment')
        
        # Drag purchase requisition attachment to purchase request automatically
        to_save = []
        for record in getattr(self, 'attachments'):
            if isinstance(target, PurchaseRequest):
                record.copy_to_resources = [target.__name__]
                to_save.append(record)
        Attachment.save(to_save)
        super().copy_resources_to(target)


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'

    comment = fields.Text('Comment', states={
        'readonly': Eval('purchase_requisition_state') != 'draft'},
        help="This comment will be in the purchase request.")

    def get_rec_name(self, name):
        if self.product and self.requisition:
            return '%s @ %s' % (self.product.rec_name, self.requisition.rec_name)
        return super(PurchaseRequisitionLine, self).get_rec_name(name)

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('requisition',) + tuple(clause[1:]),
            ('product',) + tuple(clause[1:])]

    def compute_request(self):
        request = super().compute_request()
        if request:
            if self.supplier and self.supplier != request.party:
                request.party = self.supplier
            request.manual_delivery_date = request.supply_date
            request.supply_date = None
            request.purpose = self.requisition.purpose
            request.requester = self.requisition.employee
            request.comment = (
                self.requisition.comment + "\n" + self.comment
                if self.requisition.comment else self.comment)
            self.requisition.copy_resources_to(request)
        return request


class ApprovalRequest(metaclass=PoolMeta):
    __name__ = 'approval.request'

    untaxed_amount = fields.Numeric('Untaxed Amount', readonly=True)
    party = fields.Many2One('party.party', 'Purchase Supplier', readonly=True)
    # Return True when the current employee belong to the approval group.
    user_access = fields.Function(fields.Boolean('User Right'), 'get_user_access', searcher='search_user_access')

    def get_user_access(self, name):
        employee = Transaction().context.get('employee')
        return True if self.group and employee in [
            user.employee.id for user in self.group.users] else False

    @classmethod
    def search_user_access(cls, name, clause):
        """
        Return a domain that search all the approval requests with a group where the logged user belong to
        """
        pool = Pool()
        User = pool.get('res.user')
        Group = pool.get('approval.group')
        employee_groups = []
        if 'employee' in Transaction().context:
            employee = Transaction().context.get('employee', -1)
            user = User.search(['employee', '=', employee])
            user = (user[0]) if user else -1
            groups = Group.search([()])
            employee_groups = [group.id if user in group.users else None for group in groups]
            clause[1] = 'in' if clause[1] == '=' else 'not in'
        return [('group', 'in', employee_groups)]


class HandleInvoiceException(metaclass=PoolMeta):
    __name__ = 'purchase.handle.invoice.exception'

    def default_ask(self, fields):
        default = super(HandleInvoiceException, self).default_ask(fields)
        default['recreate_invoices'] = []
        return default


class HandleShipmentException(metaclass=PoolMeta):
    __name__ = 'purchase.handle.shipment.exception'

    def default_ask(self, fields):
        default = super(HandleShipmentException, self).default_ask(fields)
        default['recreate_moves'] = []
        return default
