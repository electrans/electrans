# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import Workflow, ModelView, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.config import config as config_

DIGITS = config_.getint('product', 'price_decimal', default=4)
DISCOUNT_DIGITS = config_.getint('product', 'discount_decimal', default=4)


class Amendment(metaclass=PoolMeta):
    __name__ = 'purchase.amendment'

    @classmethod
    def __setup__(cls):
        super(Amendment, cls).__setup__()

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate_amendment(cls, amendments):
        super(Amendment, cls).validate_amendment(amendments)


class AmendmentLine(metaclass=PoolMeta):
    __name__ = 'purchase.amendment.line'

    gross_unit_price = fields.Numeric('Gross Price', digits=(16, DIGITS),
                                      required=True)
    discount = fields.Numeric('Discount', digits=(16, DISCOUNT_DIGITS),
                              required=True)
    delivery_date = fields.Date('Delivery Date')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.unit_price.states.update({
            'readonly': True
        })

    @fields.depends('line')
    def on_change_line(self):
        super().on_change_line()
        if self.line:
            self.gross_unit_price = self.line.gross_unit_price
            self.discount = self.line.discount
            if self.line.delivery_date:
                self.delivery_date = self.line.delivery_date

    def _apply_line(self, purchase, purchase_line):
        super(AmendmentLine, self)._apply_line(purchase, purchase_line)
        purchase_line.gross_unit_price = self.gross_unit_price
        purchase_line.discount = self.discount
        if self.delivery_date:
            purchase_line.delivery_date_store = self.delivery_date

    @fields.depends('gross_unit_price', 'discount')
    def update_unit_price(self):
        if self.discount and self.discount != 0.0:
            self.unit_price = self.gross_unit_price - (self.gross_unit_price *
                                                       self.discount)
        else:
            self.unit_price = self.gross_unit_price

    @fields.depends(methods=['update_unit_price'])
    def on_change_gross_unit_price(self):
        return self.update_unit_price()

    @fields.depends(methods=['update_unit_price'])
    def on_change_discount(self):
        return self.update_unit_price()


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @classmethod
    def __setup__(cls):
        super(PurchaseLine, cls).__setup__()
        states_ = ['processing', 'done', 'cancelled']
        cls.gross_unit_price.states.update({
            'readonly': Eval('purchase_state').in_(states_)
        })
        cls.discount.states.update({
            'readonly': Eval('purchase_state').in_(states_)
        })
