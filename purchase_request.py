# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields, ModelView, sequence_ordered
from trytond.pool import Pool, PoolMeta
from itertools import groupby
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateAction
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
from trytond.ir.attachment import AttachmentCopyMixin
from trytond.modules.electrans_line_management.line_management import (
    LineManagement)
from trytond.modules.product import round_price


__all__ = ['PurchaseRequest', 'CreatePurchase', 'AddToPurchaseAskPurchase',
           'AddToPurchase', 'AddToPurchaseQuotationAskQuotation',
           'AddToPurchaseQuotation']


def compute_purchase_line_from_quotation(requests, line):
    """
    Used in Create Purchase and Add to Purchase actions from Requests
    to compute the values of a line from a valid quotation of the request
    """
    best_quotation_line = None
    min_total_price = None
    total_price = None
    for req in requests:
        request_best_quotation_line = req.best_quotation_line
        # Pass best_quotation_line with the cheapest
        # (unit_price * discount) of the request to the purchase line
        if request_best_quotation_line:
            # If It's called from add_to_purchase action compute best_quotation_line with party condition
            if Transaction().context.get('from_add_to_purchase'):
                for quotation_line in req.quotation_lines_active:
                    if quotation_line.quotation_state == 'received' and quotation_line.supplier == req.party:
                        request_best_quotation_line = quotation_line
                        break
            # Compute total (unit_price * discount)
            if request_best_quotation_line.unit_price:
                total_price = request_best_quotation_line.total_unit_price
            # Check if current total is cheaper than the current cheapest
            if ((total_price and not min_total_price) or
                    (total_price and total_price < min_total_price)):
                best_quotation_line = request_best_quotation_line
                min_total_price = total_price
            if best_quotation_line:
                if best_quotation_line.unit_price and min_total_price:
                    line.unit_price = min_total_price
                    line.gross_unit_price = best_quotation_line.unit_price
                line.discount = best_quotation_line.discount
                if best_quotation_line.product_to_request:
                    line.product = best_quotation_line.product_to_request
                else:
                    line.product = best_quotation_line.product
                # Recompute product_supplier of the purchase line
                product_suppliers = list(line.product.product_suppliers_used(
                    **line._get_product_supplier_pattern()))
                if len(product_suppliers) == 1:
                    line.product_supplier, = product_suppliers
                elif (line.product_supplier
                      and line.product_supplier not in product_suppliers):
                    line.product_supplier = None
    return line

class CreatePurchase(metaclass=PoolMeta):
    __name__ = 'purchase.request.create_purchase'

    create_ = StateAction('purchase.act_purchase_form')

    def transition_start(self):
        res = super(CreatePurchase, self).transition_start()
        if res == 'ask_party':
            return res
        pool = Pool()
        Date = pool.get('ir.date')
        Request = pool.get('purchase.request')
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        QuotationLine = pool.get('purchase.request.quotation.line')
        Warning = pool.get('res.user.warning')
        requests = self.records
        unreceived_quotations = QuotationLine.search([
            ('request', 'in', requests),
            ('quotation_state', 'not in', ['received', 'rejected', 'cancelled']
             )])
        if unreceived_quotations:
            warning_name = '%s_quotation_not_received' % (
                unreceived_quotations[0].request.id)
            if Warning.check(warning_name):
                raise UserWarning(warning_name, gettext(
                    'electrans.purchase_request_quotation_not_received',
                    product=', '.join(set(quotation.product.code for quotation
                                          in unreceived_quotations))))
        reqs_to_warn = Request.check_party_not_matching_requisition(requests)
        if reqs_to_warn:
            key = 'request_%s' % reqs_to_warn[0].id
            if Warning.check(key):
                raise UserWarning(key, gettext(
                    'electrans.supplier_without_quotation',
                    request=', '.join(r.rec_name for r in reqs_to_warn)))

        lines_to_save = []
        for request in reqs_to_warn:
            request.purchase_line.update_price_to_last_purchase()
            lines_to_save.append(request.purchase_line)

        to_save = []
        purchases = {request.purchase for request in requests}
        for request in requests:
            if request.purchase in purchases:
                purchases.remove(request.purchase)
                request.purchase.purchase_date = Date.today()
                request.purchase.delivery_date = request.manual_delivery_date
                to_save.append(request.purchase)
            request.copy_resources_to(request.purchase)
        PurchaseLine.save(lines_to_save)
        Purchase.save(to_save)

        return 'create_'

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        line = super(CreatePurchase, cls).compute_purchase_line(
            key, requests, purchase)
        request = requests[0]
        line = compute_purchase_line_from_quotation(requests, line)
        PurchaseLine.update_purchase_line(request, line)
        return line

    def do_create_(self, action):
        data = {'res_id': [request.purchase.id for request in self.records]}
        action['views'].reverse()
        return action, data


class PurchaseRequest(AttachmentCopyMixin, metaclass=PoolMeta):
    __name__ = 'purchase.request'

    requester = fields.Many2One(
        'company.employee', "Requester",
        domain=[('company', '=', Eval('company', -1))],
        states={'readonly': True},
        depends=['company'])
    purpose = fields.Char('Purpose',
                          help='Purpose of the purchase_request. If there are more than 1, you should use this way: 5@VAR-1132,7@VAR-234')
    manual_delivery_date = fields.Date('Delivery Date')
    product_category = fields.Function(fields.Many2One(
        'product.category', 'Product Category'),
        'get_product_category',
        searcher='search_product_category')
    purchase_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('quotation', 'Quotation'),
            ('confirmed', 'Confirmed'),
            ('processing', 'Processing'),
            ('done', 'Done'),
            ('cancelled', 'Cancelled'),
            ], "Purchase State"),
        'get_purchase_state',
        searcher='search_purchase_state')
    shipment_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('none', 'None'),
            ('waiting', 'Waiting'),
            ('received', 'Received'),
            ('exception', 'Exception'),
            ], "Shipment State"),
        'get_shipment_state',
        searcher='search_shipment_state')
    invoice_state = fields.Function(
        fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('validated', 'Validated'),
            ('posted', 'Posted'),
            ('paid', 'Paid'),
            ('cancelled', 'Cancelled'),
            ], "Invoice State"),
        'get_invoice_state')
    comment = fields.Text('Comment', states={
        'readonly': Eval('state') != 'draft'})
    estimated_date = fields.Function(
        fields.Date('Estimated Delivery Date'),
        'get_date_purchase_line')
    production_origin = fields.Function(
        fields.Boolean('Production Origin'),
        'get_production_origin')
    unit_price = fields.Numeric('Unit Price', digits=(16, 4),
        states={'invisible':
                ~Eval('production_origin')},
        depends=['production_origin'])
    
    @classmethod
    def __setup__(cls):
        super(PurchaseRequest, cls).__setup__()
        cls.warehouse.readonly = False
        cls.purchase_date.readonly = False
        cls.warehouse.readonly = False
        cls.quotation_lines_active.order = [
            ('quotation_state', 'ASC'), ('total_unit_price', 'ASC'),
            ('supply_date', 'ASC')]
        cls.party.states['readonly'] = (
            cls.party.states['readonly'] & Bool(Eval('state') != 'quotation'))

    @classmethod
    def create(cls, vlist):
        # TODO: The next 3 lines have to be checked in 6.2.
        # this is for duplicate with patch APP-3208
        for vals in vlist:
            if not vals.get('company'):
                vals['company'] = Transaction().context['company']
        purchases = super(PurchaseRequest, cls).create(vlist)
        for purchase in purchases:
            if not purchase.party:
                Request = Pool().get('purchase.line')
                last_purchase = Request.search([
                    ('product', '=', purchase.product),
                    ('purchase.party', '!=', None)
                ], order=[('create_date', 'DESC')]
                    , limit=1)
                if last_purchase:
                    purchase.party = last_purchase[0].purchase.party
                    purchase.save()
        return purchases

    @classmethod
    def copy(cls, requests, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('quotation_lines', None)
        return super(PurchaseRequest, cls).copy(requests, default=default)

    @classmethod
    def find_best_supplier(cls, product, date, **pattern):
        supplier, super_date = super(PurchaseRequest, cls).find_best_supplier(product, date)
        if supplier is None:
            PurchaseLine = Pool().get('purchase.line')
            purchase_lines = PurchaseLine.search([
                ('product', '=', product),
                ('purchase.state', 'not in', ['cancelled', 'draft', 'quotation'])],
                order=[('create_date', 'DESC')],
                limit=1)
            if purchase_lines:
                last_line, = purchase_lines
                supplier = last_line.purchase.party
        return supplier, super_date

    @classmethod
    def search_rec_name(cls, name, clause):
        result = super(PurchaseRequest, cls).search_rec_name(name, clause)
        return [('OR', result, [('origin.requisition.number',) +
                tuple(clause[1:3]) + ('purchase.requisition.line',) +
                tuple(clause[3:])])]

    def get_product_category(self, name):
        if self.product and self.product.template and self.product.template.account_category:
            return self.product.template.account_category.id

    @classmethod
    def search_product_category(cls, name, clause):
        return [('product.template.account_category',) + tuple(clause[1:])]

    def get_product_supplier(self):
        for ps in self.product.template.product_suppliers:
            if ps.product == self.product.template \
                    and ps.party == self.party:
                return True
        return False

    def get_purchase_state(self, name):
        return self.purchase.state if self.purchase else ""

    @classmethod
    def search_purchase_state(cls, name, clause):
        return [('purchase.state',) + tuple(clause[1:])]

    def get_shipment_state(self, name):
        return self.purchase_line.shipment_state \
            if self.purchase_line and self.purchase_line.shipment_state != "none" else ""

    @classmethod
    def search_shipment_state(cls, name, clause):
        return [('purchase.shipment_state',) + tuple(clause[1:])]

    def get_invoice_state(self, name):
        return self.purchase_line.invoice_state \
            if self.purchase_line and self.purchase_line.invoice_state != "none" else ""

    def get_date_purchase_line(self, name=None):
        if self.purchase_line:
            return self.purchase_line.delivery_date
        else:
            return ''

    def get_production_origin(self, name=None):
        pool = Pool()
        Production = pool.get('production')
        return isinstance(self.origin, Production)

    @classmethod
    def update_state(cls, requests):
        # Pass requests list cleaned of None elements
        super(PurchaseRequest, cls).update_state(list(filter(None, requests)))

    @fields.depends('preferred_quotation_line')
    def on_change_preferred_quotation_line(self, name=None):
        # When there's a preferred quotation line selected, the program use its
        # supplier instead of self.party, this is only to avoid creating
        # confusions to users about creating a purchase with a different party
        if self.preferred_quotation_line:
            self.party = self.preferred_quotation_line.supplier
        else:
            self.party = None

    @classmethod
    def check_party_not_matching_requisition(cls, requests):
        """
        Returns requests with a different supplier than their purchase
        requisition, and no preferred quotation line defined
        """
        pool = Pool()
        RequisitionLine = pool.get('purchase.requisition.line')

        # Filter requests coming from purchase requisitions
        requisition_requests = filter(
            lambda x: hasattr(x, 'origin')
                      and isinstance(x.origin, RequisitionLine), requests)

        not_matching_requests = []
        for r in requisition_requests:
            if (r.origin.supplier and r.origin.supplier != r.party
                    and not r.preferred_quotation_line):
                not_matching_requests.append(r)
        return not_matching_requests

    @classmethod
    def get_resources_to_copy(cls, name):
        return {
            'purchase.purchase'
            }
    
    def copy_resources_to(self, target):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Attachment = pool.get('ir.attachment')
        
        # Drag purchase request attachment to purchase automatically
        to_save = []
        if isinstance(target, Purchase):
            # file_id contains an unique id based on the file, different 
            # records with the same file attached, will share the same file_id
            file_ids = set(x.file_id for x in getattr(target, 'attachments'))
            for record in getattr(self, 'attachments'):
                if record.file_id not in file_ids:
                    file_ids.add(record.file_id)
                    record.copy_to_resources = [target.__name__]
                    to_save.append(record)
        Attachment.save(to_save)
        super().copy_resources_to(target)


class AddToPurchaseAskPurchase(ModelView):
    'Add to Purchase Ask Purchase'
    __name__ = 'purchase.request.add_to_purchase.ask_purchase'
    requests = fields.One2Many('purchase.request', None, 'Purchase Requests')
    party = fields.Many2One('party.party', 'Supplier', required=True)
    purchase = fields.Many2One('purchase.purchase', 'Purchase', required=True,
        domain=[('state', '=', 'draft'), ('party', '=', Eval('party'))],
        depends=['party'])


# TODO: Check company, party, payment_term, warehouse, currency and invoice_address
# are the same in all requests (see stock_supply.purchase_request._group_purchase_key),
# and put this in the domain of the purchases to choose.
# And check the date.
class AddToPurchase(Wizard):
    'Add to Purchase'
    __name__ = 'purchase.request.add_to_purchase'
    start = StateTransition()
    ask_purchase = StateView('purchase.request.add_to_purchase.ask_purchase',
        'electrans.purchase_request_add_to_purchase_ask_purchase', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'create_', 'tryton-forward', default=True),
            ])
    create_ = StateAction('purchase.act_purchase_form')

    def default_ask_purchase(self, fields):
        requests = self.records
        requests_id = [r.id for r in requests]

        # The party if there is one and only one; otherwise None
        parties = []
        for k, g in groupby(requests, lambda x: x.party):
            parties.append(k)
        if len(parties) == 1 and parties[0] is not None:
            party = parties[0]
            return {
                'requests': requests_id,
                'party': party.id,
                }
        else:
            return {
                'requests': requests_id,
                }

    def _group_purchase_line_key(self, request):
        '''
        The key to group requests by lines
        A list of key-value as tuples of the purchase line
        '''
        return (
                ('product', request.product),
                ('unit', request.uom),
            )

    def transition_start(self):
        for record in self.records:
            if not record.warehouse:
                raise UserError(gettext('electrans.no_request_warehouse'))
        return 'ask_purchase'

    # TODO: Function copied from purchase.request.create_request,
    # delete (see TODO in compute_purchase_line function)
    @staticmethod
    def _get_tax_rule_pattern(line, purchase):
        '''
        Get tax rule pattern
        '''
        return {}

    # TODO: Function copied from purchase.request.create_request,
    # change by a call
    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        request = requests[0]
        line = PurchaseLine()
        line.unit_price = Decimal(0).quantize(
            Decimal(1) / 10 ** PurchaseLine.unit_price.digits[1])
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.quantity for r in requests)
        line.purchase = purchase
        line.on_change_product()
        with Transaction().set_context(from_add_to_purchase=True):
            line = compute_purchase_line_from_quotation(requests, line)
        PurchaseLine.update_purchase_line(request, line)
        return line

    def do_create_(self, action):
        pool = Pool()
        Request = pool.get('purchase.request')
        Warning = pool.get('res.user.warning')

        if not (getattr(self.ask_purchase, 'party', None)
                and getattr(self.ask_purchase, 'purchase', None)):
            return 'ask_purchase'
        
        requests = [r for r in self.records if not r.purchase_line]

        purchase = self.ask_purchase.purchase
        for line_key, line_requests in groupby(
                requests, key=self._group_purchase_line_key):
            line_requests = list(line_requests)
            line = self.compute_purchase_line(line_key, line_requests, purchase)
            line.purchase = purchase
            line.save()
            Request.write(line_requests, {
                    'party': self.ask_purchase.party.id,
                    'purchase_line': line.id,
                    })
            Request.update_state(line_requests)

        reqs_to_warn = Request.check_party_not_matching_requisition(requests)
        if reqs_to_warn:
            key = 'request_%s' % reqs_to_warn[0].id
            if Warning.check(key):
                raise UserWarning(key, gettext(
                    'electrans.supplier_without_quotation',
                    request=', '.join(r.rec_name for r in reqs_to_warn)))

        lines_to_save = []
        for request in requests:
            if request in reqs_to_warn:
                request.purchase_line.update_price_to_last_purchase()
                lines_to_save.append(request.purchase_line)
            request.copy_resources_to(purchase)
        
        data = {'res_id': [purchase.id]}
        action['views'].reverse()
        return action, data


class SplitPurchaseRequest(Wizard):
    'Split the purchase request'
    __name__ = 'purchase.request.split'

    start_state = 'ask_quantity'
    ask_quantity = StateView('purchase.request.split.start',
        'electrans.purchase_request_split_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Split', 'split', 'tryton-forward', default=True),
        ])
    split = StateAction('purchase_request.act_purchase_request_form')

    def do_split(self, action):
        pool = Pool()
        Request = pool.get('purchase.request')
        original_request = self.record
        # Allow to split if state not in purchased or done
        if original_request.state in ['purchased', 'done']:
            raise UserError(gettext('electrans.invalid_request_state'))
        if original_request.quantity <= self.ask_quantity.quantity or self.ask_quantity.quantity <= 0:
            raise UserError(gettext('electrans.invalid_quantity'))
        original_request.quantity -= self.ask_quantity.quantity
        original_request.save()
        new_request, = Request.copy([original_request],{'quantity': self.ask_quantity.quantity})
        data = {'res_id': [new_request.id]}
        action['views'].reverse()
        return action, data


class SplitPurchaseRequestStart(ModelView):
    'Split the purchase request start'
    __name__ = 'purchase.request.split.start'

    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('uom_digits', 2)), depends=['uom_digits'])
    uom = fields.Many2One('product.uom', 'UOM', readonly=True)
    uom_digits = fields.Function(fields.Integer('UoM Digits'),
        'on_change_with_uom_digits')

    @fields.depends('uom')
    def on_change_with_uom_digits(self, name=None):
        if self.uom:
            return self.uom.digits
        return 2


class Quotation(LineManagement, metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation'

    @classmethod
    def __setup__(cls):
        super(Quotation, cls).__setup__()
        cls.lines.readonly = False
        cls.lines.states['readonly'] = Eval('state') == 'rejected'


class QuotationLine(sequence_ordered(), metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation.line'

    request_purpose = fields.Function(
        fields.Char('Purpose'), 'get_request_purpose')
    discount = fields.Numeric('Discount', digits=(16, 4), required=True)
    total_unit_price = fields.Function(
        fields.Numeric('Total Unit Price', digits=(16, 4)), 'get_total_unit_price')
    product_to_request = fields.Many2One(
        'product.product', 'Product To Request',
        help="If it is filled, the purchase will be created with that "
             "product instead of the purchase request product.",
        domain=[('purchasable', '=', True)],
        context={
            'company': Eval('company', -1),
            },
        depends=['company'])
    company = fields.Function(
        fields.Many2One('company.company', "Company"),
        'on_change_with_company')

    @classmethod
    def __setup__(cls):
        super(QuotationLine, cls).__setup__()
        cls.request.required = False

    @fields.depends('quotation', '_parent_quotation.company')
    def on_change_with_company(self, name=None):
        if self.quotation and self.quotation.company:
            return self.quotation.company.id

    @staticmethod
    def default_discount():
        return Decimal(0)

    @fields.depends('request', '_parent_request.manual_delivery_date',
                    '_parent_request.supply_date')
    def on_change_request(self):
        super(QuotationLine, self).on_change_request()
        if self.request:
            if self.request.supply_date:
                self.supply_date = self.request.supply_date
            else:
                self.supply_date = (
                        self.request.manual_delivery_date or None)

    def get_request_purpose(self, name=None):
        return (self.request.purpose if self.request and
                self.request.purpose else None)

    def get_total_unit_price(self, name=None):
        total_unit_price = None
        if self.unit_price != None:
            total_unit_price = round_price(self.unit_price * (1 - self.discount))
        return total_unit_price

    @classmethod
    def order_total_unit_price(cls, tables):
        pool = Pool()
        QuotationLine = pool.get('purchase.request.quotation.line')
        quotation_line, _ = tables[None]
        _cast = QuotationLine.unit_price.sql_cast
        return [quotation_line.unit_price == None,
                _cast(quotation_line.unit_price) * (_cast(1) - _cast(quotation_line.discount))]


class CreatePurchaseRequestQuotation(metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation.create'

    def transition_create_quotations(self):
        pool = Pool()
        Quotation = pool.get('purchase.request.quotation')
        QuotationLine = pool.get('purchase.request.quotation.line')
        res = super(CreatePurchaseRequestQuotation, self).transition_create_quotations()
        for request in self.records:
            quotations = set(l.quotation for l in request.quotation_lines)
            for quotation in quotations:
                modified_lines = Quotation.generate_line_positions(
                    quotation.lines)
                QuotationLine.save(modified_lines)
        return res

    def get_quotation_line(self, request, quotation):
        quotation_line = super().get_quotation_line(request, quotation)
        if request.supply_date:
            quotation_line.supply_date = request.supply_date
        else:
            quotation_line.supply_date = (
                    request.manual_delivery_date or None)
        return quotation_line

    def get_quotation(self, supplier, key):
        quotation = super().get_quotation(supplier, key)
        if quotation and self.record and self.record.warehouse:
            quotation.warehouse = self.record.warehouse
        return quotation


class AddToPurchaseQuotationAskQuotation(ModelView):
    'Add to Purchase Quotation Ask Quotation'
    __name__ = 'purchase.request.add_to_purchase_quotation.ask_quotation'
    requests = fields.One2Many('purchase.request', None, 'Purchase Requests')
    quotation = fields.Many2One(
        'purchase.request.quotation', 'Quotation', required=True,
        domain=[('state', '=', 'draft')])


class AddToPurchaseQuotation(Wizard):
    'Add to Purchase Quotation'
    __name__ = 'purchase.request.add_to_purchase_quotation'
    start = StateTransition()
    ask_quotation = StateView(
        'purchase.request.add_to_purchase_quotation.ask_quotation',
        'electrans.purchase_request_add_to_purchase_quotation_ask_quotation', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'create_', 'tryton-forward', default=True),
            ])
    create_ = StateAction(
        'purchase_request_quotation.act_purchase_request_quotation_form')

    def _group_purchase_line_key(self, request):
        '''
        The key to group requests by lines
        A list of key-value as tuples of the purchase line
        '''
        return (
                ('product', request.product),
                ('unit', request.uom),
            )

    def transition_start(self):
        for record in self.records:
            if record.state != 'draft':
                raise UserError(gettext('electrans.request_not_in_draft'))
        return 'ask_quotation'

    def default_ask_quotation(self, fields):
        return {
            'requests': [r.id for r in self.records],
        }

    @classmethod
    def get_sequence_value(cls, quotation):
        pool = Pool()
        QuotationLine = pool.get('purchase.request.quotation.line')
        max_sequence = QuotationLine.search(
            [('quotation', '=', quotation)],
            order=[('sequence', 'DESC')], limit=1)

        return max_sequence[0].sequence + 1 if max_sequence else 1

    @classmethod
    def compute_quotation_line(cls, key, requests, quotation):
        pool = Pool()
        PurchaseQuotationLine = pool.get('purchase.request.quotation.line')
        request = requests[0]
        line = PurchaseQuotationLine()
        line.unit_price = Decimal(0).quantize(
            Decimal(1) / 10 ** PurchaseQuotationLine.unit_price.digits[1])
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.quantity for r in requests)
        line.quotation = quotation
        line.request = request
        line.unit = request.uom
        line.currency = request.currency
        line.product = request.product
        line.description = request.description
        line.sequence = cls.get_sequence_value(quotation)

        return line

    def do_create_(self, action):
        pool = Pool()
        Request = pool.get('purchase.request')
        Quotation = pool.get('purchase.request.quotation')
        QuotationLine = pool.get('purchase.request.quotation.line')

        if not getattr(self.ask_quotation, 'quotation', None):
            return 'ask_quotation'

        requests = self.records
        requests = [r for r in requests if not r.purchase_line]

        quotation = self.ask_quotation.quotation

        if quotation.lines and not quotation.lines[0].sequence:
            modified_lines = Quotation.generate_line_positions(
                quotation.lines)
            QuotationLine.save(modified_lines)

        for line_key, line_requests in groupby(
                requests, key=self._group_purchase_line_key):
            line_requests = list(line_requests)
            line = self.compute_quotation_line(
                line_key, line_requests, quotation)
            line.save()

        Request.write(requests, {'state': 'quotation'})

        data = {'res_id': [quotation.id]}
        action['views'].reverse()
        return action, data


class PurchaseRequestQuotationGenerateLinePositions(Wizard):
    'Sale Generate Positions'
    __name__ = 'purchase.request.quotation.generate_line_positions'

    start_state = 'generate_line_positions_start'
    generate_line_positions_start = StateTransition()

    @staticmethod
    def transition_generate_line_positions_start():
        pool = Pool()
        Quotation = pool.get('purchase.request.quotation')
        Line = pool.get('purchase.request.quotation.line')
        for quotation in Quotation.browse(Transaction().context['active_ids']):
            # only returns de lines which position has been modified
            modified_lines = Quotation.generate_line_positions(quotation.lines)
            Line.save(modified_lines)
        return 'end'
