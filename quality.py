# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.jasper_reports.jasper import JasperReport

__all__ = ['QualityTemplateReport', 'QualityTestReport',
    'QualityTemplateInternalReport', 'QualityTestInternalReport',
    'QualitativeTemplateLine', 'QuantitativeTemplateLine', 'TemplateLine', 'TestLine']


class QualityTemplateReport(JasperReport):
    __name__ = 'quality.template.report'


class QualityTestReport(JasperReport):
    __name__ = 'quality.test.report'


class QualityTemplateInternalReport(JasperReport):
    __name__ = 'quality.template.internal.report'

    @classmethod
    def execute(cls, ids, data):
        new_data = data.copy()
        parameters = data.get('parameters', {})
        parameters.update({'internal_description': True})
        new_data['parameters'] = parameters
        return super(QualityTemplateInternalReport, cls).execute(
            ids, new_data)


class QualityTestInternalReport(QualityTemplateInternalReport):
    __name__ = 'quality.test.internal.report'


class QualitativeTemplateLine(metaclass=PoolMeta):
    __name__ = 'quality.qualitative.template.line'

    print_on_reports = fields.Boolean('Print on reports')

    @staticmethod
    def default_print_on_reports():
        return True


class QuantitativeTemplateLine(QualitativeTemplateLine):
    __name__ = 'quality.quantitative.template.line'


class TemplateLine(QualitativeTemplateLine):
    __name__ = 'quality.template.line'


class TestLine(metaclass=PoolMeta):
    __name__ = 'quality.test.line'

    print_on_reports = fields.Function(fields.Boolean('Print on reports'),
        'get_print_on_reports')

    @classmethod
    def get_print_on_reports(cls, models, name):
        result = {}
        for model in models:
            record = cls.union_unshard(model.id)
            value = False
            if record.template_line:
                value = record.template_line.print_on_reports
            result[model.id] = value
        return result


class Configuration(metaclass=PoolMeta):
    __name__ = 'quality.configuration'

    quality_department_email = fields.Many2One(
        'party.contact_mechanism', 'Email from the Quality Department')
