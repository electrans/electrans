import datetime as dt
from trytond.model import fields
from trytond.pool import PoolMeta, Pool

__all__ = ['User']


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    signature_image = fields.Binary('Signature Image')


class Role(metaclass=PoolMeta):
    __name__ = 'res.role'

    users = fields.Function(
        fields.One2Many('res.user', None, "Users"),
        'get_users')

    def get_users(self, name):
        UserRole = Pool().get('res.user.role')
        return [rel.user.id for rel in UserRole.search([('role', '=', self.id)])
                if rel.valid(dt.datetime.today()) and rel.user]
