# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from sql.conditionals import Coalesce
from trytond.model import Workflow, ModelView, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.pyson import Eval, Bool, Not, And, If
from decimal import Decimal
import datetime
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import AccessError
from trytond.modules.stock_serial_number.stock import NUMBER_REGEXP
from trytond.modules.company.model import employee_field

__all__ = ['Location', 'Lot', 'ShipmentOut', 'ShipmentIn', 'ShipmentInReturn',
    'ShipmentInternal', 'StockMove', 'ShipmentInternalReport',
    'LocationLeadTime', 'ShipmentInternalReportOrderByLocation',
    'ShipmentInternalReportOrderByProduct',
    'ShipmentInternalReportOutgoingMovesOrderByProduct', 'SplitMoveStart']

invisible_fields_by_internal_shipment_subtype = {
    'reference': ['intrawarehouse', 'out'],
    'origin': ['intrawarehouse', 'out'],
    'effective_start_date': ['intrawarehouse', 'in'],
    'planned_start_date': ['intrawarehouse', 'in'],
    'company': ['intrawarehouse', 'in', 'out'],
    'comment': [],
    'carrier': ['intrawarehouse', 'in'],
    'delivery_address': ['intrawarehouse', 'in'],
    'number_packages': ['intrawarehouse', 'in'],
    'weight': ['intrawarehouse', 'in'],
    }

_INTERNAL_SHIPMENT_STATES = {
    'readonly': ~Eval('state').in_(['draft', 'waiting', 'assigned', 'packed']),
}


class LocationLeadTime(metaclass=PoolMeta):
    __name__ = 'stock.location.lead_time'

    @classmethod
    def get_max_lead_time(cls):
        res = super(LocationLeadTime, cls).get_max_lead_time()
        return min(res, datetime.timedelta(days=3))


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    different_warehouse_location = fields.Function(
        fields.Boolean(
            "Use different warehouse location",
            help="If this address is not indicated, the one defined in the warehouse will be used.",
            states={'invisible': ~Eval('type').in_(['storage', 'view'])}),
        'get_different_warehouse_location',
        setter='set_different_warehouse_location')
    route = fields.Function(fields.Char('Route'), 'get_route')
    movable = fields.Boolean('Movable',
                             states={
                                 'readonly': ~Eval('active'),
                                 'invisible': Eval('type') != 'storage'
                             })
    pallet_zone = fields.Many2One('stock.location', 'Pallet Zone',
                                  states={
                                      'invisible': Eval('type') != 'warehouse',
                                      'readonly': ~Eval('active'),
                                  },
                                  domain=[
                                      ('type', '=', 'view'),
                                      ('flat_childs', '=', True)
                                  ])
    pallet_sequence = fields.Many2One('ir.sequence', 'Pallet Sequence',
                                      states={
                                          'invisible': Eval('type') != 'warehouse',
                                          'readonly': ~Eval('active')
                                      },
                                      domain=[
                                          ('company', 'in', [Eval('context', {}).get('company', -1), None])
                                      ])
    exclude_consumables_picking = fields.Boolean('Exclude consumables in picking',
                                                 states={
                                                     'invisible': Eval('type') != 'warehouse',
                                                 })
    entry_location = fields.Many2One('stock.location', 'Entries Location',
                                     states={
                                         'invisible': Eval('type') != 'warehouse',
                                     })
    leftovers_location = fields.Many2One(
        'stock.location', "Leftovers Location",
        states={
            'readonly': ~Eval('active'),
            'invisible': Eval('type') != 'warehouse'},
        domain=[
            ('type', '=', 'storage')],
        help="If this location is not indicated, the one defined in the stock configuration will be used.")
    provisioning_location_used = fields.Function(
        fields.Many2One(
            'stock.location', "Provisioning Location",
            states={
                'invisible': Eval('type') != 'view',
                'readonly': Not(Eval('provisioning_location_edit', False))}),
        'get_provisioning_location_used',
        'set_provisioning_location_used')
    provisioning_location_edit = fields.Boolean(
        "Provisioning location edit",
        states={
            'invisible': Eval('type') != 'view'})

    @classmethod
    def __setup__(cls):
        super(Location, cls).__setup__()
        t = cls.__table__()
        cls.name.translate = False
        cls.code.required = True
        cls.code.size = 20
        cls._sql_constraints = [
            ('unique_code', Unique(t, t.code),
            'electrans.msg_location_code_unique')
            ]
        cls.address.states['invisible'] = If(
            ~Eval('type').in_(['warehouse', 'lost_found']),
            If(And(Eval('type').in_(['storage', 'view']),Bool(Eval('different_warehouse_location'))),
                False,
                True),
            False)

    @classmethod
    def get_route(cls, products, name):
        def get_parent_name(product):
            return get_parent_name(product.parent) + " / " + product.name if product.parent else product.name

        routes = {}
        for product in products:
            routes[product.id] = get_parent_name(product)
        return routes

    def get_different_warehouse_location(self, name):
        return bool(self.address)

    def set_different_warehouse_location(self, location, name):
        'Dummy function to enable the check'
        pass

    @fields.depends('different_warehouse_location')
    def on_change_with_address(self, name=None):
        return

    def get_provisioning_location_used(self, name):
        Location = Pool().get('stock.configuration.location')
        # If boolean is checked and fill with provisioning_location if provisioning_location exists
        if self.provisioning_location_edit and self.provisioning_location:
            return self.provisioning_location.id
        # Get provisioning_location if exists in his storage_location (means that current location is warehouse)
        if self.storage_location and self.storage_location.provisioning_location:
            return self.storage_location.provisioning_location.id
        # Search in the parent of the current location the provisioning_location_used that is recursive
        # cause if the previous conditions are not true it will search in the parent another time
        elif self.parent:
            return self.parent.provisioning_location_used.id
        # Get provisioning_location from the configuration
        else:
            return Location(1).provisioning_movable_location.id

    @classmethod
    def set_provisioning_location_used(cls, locations, name, value):
        if not value:
            return
        # When modifying provisioning_location_used also modify provisioning_location
        cls.write(locations, {'provisioning_location': value})


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    drawing_lines = fields.Function(fields.One2Many('production.drawing.line',
            None, 'Drawing Lines'), 'get_drawing_lines')
    tests = fields.Function(fields.One2Many('quality.test', None,
            'Quality Tests'), 'get_quality_tests')
    drawing_image = fields.Function(fields.Binary('Drawing Image'),
        'get_drawing_image')
    lines = fields.One2Many('stock.move', 'lot', 'Moves')
    note = fields.Char('Note')
    output_production = fields.Function(
        fields.Many2One(
            'production', "Output Production",
            states={'invisible': Not(Bool(Eval('output_production')))}),
        'get_output_production')
    production_date = fields.Function(
        fields.Date(
            "Production date",
            states={'invisible': Not(Bool(Eval('output_production')))}),
        'get_production_date')
    manual_production_date = fields.Date(
        "Manual production date",
        states={
            'invisible': Bool(Eval('output_production')),
            'readonly': Bool(Eval('unknown_manufacturing_date'))},
        depends=['output_production', 'unknown_manufacturing_date'])
    unknown_manufacturing_date = fields.Boolean(
        "Unknown manufacturing date",
        states={
            'invisible': Bool(Eval('output_production')),
            'readonly': Bool(Eval('manual_production_date'))},
        depends=['output_production', 'unknown_manufacturing_date'])
    manufacturer = fields.Function(
        fields.Many2One(
            "party.party", "Manufacturer",
            states={'invisible': Not(Bool(Eval('output_production')))}),
        'get_manufacturer')
    manual_manufacturer = fields.Many2One(
        "party.party", "Manufacturer",
        states={'invisible': Bool(Eval('output_production'))})
    supplier_ref = fields.Char('Supplier Reference')

    @classmethod
    def __setup__(cls):
        super(Lot, cls).__setup__()
        t = cls.__table__()
        cls.product.states['readonly'] = Bool(Eval('lines', False))
        cls._sql_constraints += [
            ('product_number_uniq', Unique(t, t.product, t.number),
                'electrans.msg_product_code_unique'),
            ]

    def get_rec_name(self, name):
        res = super(Lot, self).get_rec_name(name)
        if (self.product and self.product.template and
                self.product.template.serial_number):
            seq = self.product.template.lot_sequence
            if seq and seq.prefix == self.number[:len(seq.prefix)]:
                res = res[len(seq.prefix):]
        return res

    def get_drawing_image(self, name):
        Production = Pool().get('production')
        productions = Production.search([
                ('outputs.lot', '=', self.id),
                ], order=[('number', 'DESC')], limit=1)
        if not productions:
            return None
        production, = productions
        return production.drawing.image if production.drawing else None

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Lot, cls).search_rec_name(name, clause)
        return ['OR',
            domain,
            ('product.name',) + tuple(clause[1:]),
            ]

    def get_drawing_lines(self, name):
        Production = Pool().get('production')
        productions = Production.search([
                ('outputs.lot', '=', self.id),
                ], order=[('number', 'DESC')], limit=1)
        if not productions:
            return []
        production, = productions
        return [x.id for x in production.drawing_lines]

    def get_quality_tests(self, name):
        Test = Pool().get('quality.test')
        tests = Test.search([
                ('document', '=', str(self)),
                ])
        return [x.id for x in tests]

    def get_move(self):
        pool = Pool()
        Move = pool.get('stock.move')
        move = Move.search([('lot', '=', self.id), ('production_output', '!=', None), ('state', '=', 'done')],
                           order=[('effective_date', 'ASC')], limit=1)
        return move

    def get_output_production(self, name):
        move = self.get_move()
        return move[0].production_output.id if move else None

    def get_production_date(self, name):
        move = self.get_move()
        return move[0].effective_date if move else self.manual_production_date

    def get_manufacturer(self, name):
        move = self.get_move()
        if move and move[0].production_output and move[0].production_output.supplier:
            return move[0].production_output.supplier.id
        if move and move[0].production_output and move[0].production_output.company \
                and move[0].production_output.company.party:
            return move[0].production_output.company.party.id
        return self.manual_manufacturer.id if self.manual_manufacturer else None
    
    @classmethod
    def get_quantity(cls, lots, name):
        "Return null instead of 0.0 if no locations in context"
        pool = Pool()
        User = pool.get('res.user')
        Location = pool.get('stock.location')
        if not Transaction().context.get('locations'):
            user = User(Transaction().user)
            warehouses = None
            if user.warehouse:
                warehouses = [user.warehouse]
            else:
                warehouses = Location.search(['type', '=', 'warehouse'])
            if not warehouses:
                return {}.fromkeys([l.id for l in lots], None)

            locations = [w.storage_location.id for w in warehouses]
            Transaction().set_context(locations=locations)
        return super(Lot, cls).get_quantity(lots, name)


class LotTrace(metaclass=PoolMeta):
    __name__ = 'stock.lot.trace'

    move_origin = fields.Reference("Origin", 'get_origins')
    origin = fields.Function(
        fields.Reference("Origin", 'get_origins'), 'get_origin')
    from_location = fields.Many2One(
        'stock.location', "From Location")
    to_location = fields.Many2One(
        'stock.location', "To Location")
    description = fields.Text('Description')
    planned_date = fields.Date("Planned Date")
    manufacturer = fields.Function(
        fields.Many2One('party.party', 'Manufacturer'), 'get_manufacturer')
    last_modification = fields.Function(fields.Char('Last Modification'),
        'get_last_modification')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ], "State")

    # Override table_query to show moves in any state
    @classmethod
    def table_query(cls):
        pool = Pool()
        Move = pool.get('stock.move')
        move = Move.__table__()
        return (
            move.select(
                *cls._columns(move)))

    @classmethod
    def _columns(cls, move):
        return super()._columns(move) + [
            move.origin.as_('move_origin'),
            move.planned_date.as_('planned_date'),
            move.description.as_('description'),
            move.from_location.as_('from_location'),
            move.to_location.as_('to_location'),
            move.state.as_('state'),
            ]

    @classmethod
    def _is_trace_move(cls, move):
        # Show all moves in any state
        return True

    @classmethod
    def get_origins(cls):
        pool = Pool()
        Move = pool.get('stock.move')
        return Move.get_origin()

    def get_origin(self, name):
        if self.move_origin:
            return self.move_origin

    @classmethod
    def get_manufacturer(cls, records, name):
        result = {}
        for line in records:
            result[line.id] = (line.product.manufacturer.id
                if line.product and line.product.manufacturer
                else None)
        return result

    def get_last_modification(self, name):
        def format_date(date):
            date_format = '%Y-%m-%d %H:%M:%S'
            return str(date.strftime(date_format))
        return format_date(self.write_date) if self.write_date else format_date(self.create_date)


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    origin_number = fields.Function(fields.Char('Origin'), 'get_origin_number')
    invoice_numbers = fields.Function(fields.Char('Invoice Numbers'),
        'get_invoice_numbers')
    purchase_request_users_email = fields.Function(fields.Char('Users email'),
        'get_purchase_request_users_email')

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        if not cls.reference.required:
            cls.reference.required = True

        cls.effective_date.states['readonly'] = Not(Eval('state').in_(
            ['draft']))

        cls._buttons.update({
            'generate_move_lot': {'invisible': Eval('state') != 'draft'},
        })

    @classmethod
    def receive(cls, shipments):
        pool = Pool()
        Move = pool.get('stock.move')
        Warning = pool.get('res.user.warning')
        for shipment in shipments:
            key = 'shipment_%s' % shipment.id
            if Warning.check(key):
                raise UserWarning(key, gettext('electrans.info_generate_lot'))
            Move.generate_lot(shipment.incoming_moves)
        super(ShipmentIn, cls).receive(shipments)

    @classmethod
    def done(cls, shipments):
        lots_name = []
        for shipment in shipments:
            for move in shipment.incoming_moves:
                if move.lot and move.product and not move.lot.supplier_ref and move.product.serial_number:
                    lots_name.append(move.lot.rec_name)
            if lots_name:
                raise UserError(gettext('electrans.lot_supplier_ref_required',
                                        lots=', '.join(name for name in lots_name)))
        super(ShipmentIn, cls).done(shipments)

    @classmethod
    @ModelView.button
    def generate_move_lot(cls, shipments):
        Move = Pool().get('stock.move')
        for shipment in shipments:
            Move.generate_lot(shipment.incoming_moves)

    @fields.depends('warehouse')
    def on_change_with_warehouse_storage(self, name=None):
        wh = self.warehouse
        if wh:
            return wh.entry_location and wh.entry_location.id or wh.storage_location.id

    def get_origin_number(self, name=None):
        PurchaseLine = Pool().get('purchase.line')
        for move in self.moves:
            if move.origin and isinstance(move.origin, PurchaseLine) and move.origin.purchase:
                return move.origin.purchase.number

    def get_invoice_numbers(self, name=None):
        invoice_numbers = set()
        for move in self.incoming_moves:
            numbers = [line.invoice.number for line in move.invoice_lines if
                       line.invoice and line.invoice.number]
            if numbers:
                invoice_numbers.add(", ".join(numbers))
        return ", ".join(invoice_numbers)

    def get_purchase_request_users_email(self, name):
        pool = Pool()
        User = pool.get('res.user')
        emails = []
        if self.moves:
            for move in self.incoming_moves:
                for line in move.purchase.lines:
                    if line.requests:
                        for request in line.requests:
                            if request.requester:
                                user, = User.search([
                                    ('employee.id', '=', request.requester.id)
                                ])
                                if user.email not in emails:
                                    emails.append(user.email)
        return ', '.join(email for email in emails)


class ShipmentInReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    customer = fields.Function(fields.Many2One('party.party', 'Customer'),
        'get_customer')
    #origin = fields.Function(fields.Char('Origin'),
    #    'get_origin_purchase')
    carrier = fields.Many2One('carrier', 'Carrier', states={
            'readonly': ~Eval('state').in_(['draft', 'waiting', 'assigned',
                    'packed']),
            },
        depends=['state'])
    weight = fields.Float('Weight',
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting', 'assigned',
                    'packed']),
            },
        depends=['state'])
    number_packages = fields.Integer('Number of Packages', states={
            'readonly': ~Eval('state').in_(['draft', 'waiting', 'assigned',
                    'packed']),
            }, depends=['state'])

    def get_customer(self, name):
        return self.supplier.id if self.supplier else None

    @classmethod
    def get_supplier(cls, shipments, name):
        res = {}
        PurchaseLine = Pool().get('purchase.line')
        for shipment in shipments:
            party = None
            if shipment.moves:
                origin = None
                for move in shipment.moves:
                    if move.origin:
                        origin = move.origin
                if origin:
                    if isinstance(origin, PurchaseLine):
                        party = origin.purchase.party.id
            res[shipment.id] = party
        return res


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    weight = fields.Float('Weight',
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting', 'assigned',
                    'packed', 'picked']),
            },
        depends=['state'])
    contact = fields.Char('Contact', states={
            'readonly': Eval('state').in_(['cancelled', 'done']),
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        # delivery address readonly in cancelled and done states
        cls.delivery_address.states['readonly'] = Eval('state').in_(
            ['cancelled', 'done'])
        cls.reference.help = "In the shipment report, this field is shown next to the 'Your order' label"
        cls.reference.states['invisible'] = True

    def _sync_outgoing_move(self, template=None):
        move = super(ShipmentOut, self)._sync_outgoing_move(template)
        if template:
            move.description = template.description
        return move


class ShipmentOutReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @fields.depends('warehouse')
    def on_change_with_warehouse_storage(self, name=None):
        wh = self.warehouse
        if wh:
            return wh.entry_location and wh.entry_location.id or wh.storage_location.id


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    origin = fields.Function(
        fields.Many2One('purchase.purchase','Origin'),
        'get_origin_purchase')
    origin_number = fields.Function(
        fields.Char('Origin'),
        'get_origin_number')
    customer = fields.Function(
        fields.Many2One('party.party', 'Customer', readonly=True),
        'get_delivery_address_customer')
    delivery_address = fields.Function(
        fields.Many2One('party.address', 'Delivery Address', readonly=True),
        'get_delivery_address')
    carrier = fields.Many2One('carrier', 'Carrier',
        states=_INTERNAL_SHIPMENT_STATES,
        depends=['state'])
    weight = fields.Float('Weight',
        states=_INTERNAL_SHIPMENT_STATES,
        depends=['state'])
    number_packages = fields.Integer('Number of Packages', 
        states=_INTERNAL_SHIPMENT_STATES, 
        depends=['state'])
    ordered_moves = fields.Function(
        fields.One2Many('stock.move', 'shipment', 'Order Moves'),
        'get_order_moves')
    related_to = fields.Char('Related to', 
        states=_INTERNAL_SHIPMENT_STATES,
        depends=['state'])
    subtype = fields.Function(
        fields.Char('Subtype'),
        'on_change_with_subtype')
    productions = fields.Many2Many('production-stock.shipment.internal',
        'shipment', 'production', 'Productions',
        states={'invisible': ~Eval('productions'),},
        readonly=True)
    delivered_to = employee_field("Delivered To")

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        # Make fields invisible depending on shipment subtype
        for field_name, subtypes_to_hide in invisible_fields_by_internal_shipment_subtype.items():
            f = getattr(cls, field_name)
            f.depends.update({'subtype'})
            f.states.update({
                'invisible': f.states.get('invisible') | Bool(Eval('subtype').in_(subtypes_to_hide))
                })
        # Make reference field required and readonly when...
        cls.reference.states['required'] = (Not(Eval('state').in_(['draft', 'waiting']))) & (Eval('subtype').in_(['in']))
        cls.reference.states['readonly'] = Not(Eval('state').in_(['draft', 'waiting']))
        cls._buttons['split_wizard']['invisible'] = ~Eval('state').in_(['draft', 'waiting'])
        cls._buttons['split_wizard']['readonly'] = ~Eval('state').in_(['draft', 'waiting'])

    @classmethod
    def copy(cls, shipments, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['productions'] = None
        return super(ShipmentInternal, cls).copy(shipments, default=default)

    # Used with act_window context
    @staticmethod
    def default_from_location():
        return Transaction().context.get('from_location', None)

    # Used with act_window context
    @staticmethod
    def default_to_location():
        return Transaction().context.get('to_location', None)

    @fields.depends('from_location', 'to_location')
    def on_change_with_subtype(self, name=None):
        from_warehouse = None
        from_type = None
        to_warehouse = None
        to_type = None
        company = None
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
        company_warehouses = company.warehouses if company else ()
        if self.from_location:
            from_type = self.from_location.type
            if self.from_location.warehouse:
                from_warehouse = self.from_location.warehouse
        if self.to_location:
            to_type = self.to_location.type
            if self.to_location.warehouse:
                to_warehouse = self.to_location.warehouse
        # Intrawarehouse
        if to_warehouse in company_warehouses and from_warehouse in company_warehouses:
            return 'intrawarehouse'
        # Service Supplier -> Almacén Electrans
        if from_warehouse and to_warehouse in company_warehouses:
            return 'in'
        # Almacén Electrans -> Service Supplier
        if from_warehouse in company_warehouses and to_warehouse:
            return 'out'
        # From/To Lost/Found
        if from_type == 'lost_found' or to_type == 'lost_found':
            return 'intrawarehouse'
        # Special case: From/To Zona Recuperaciones Almacén
        if (from_warehouse == None and from_type == 'view') or \
            (to_warehouse == None and to_type == 'view'):
            return 'intrawarehouse'
        # From/To Production
        if from_type == 'production' or to_type == 'production':
            return 'intrawarehouse'
        # From/To not set, or other cases (not expected)
        return 'intrawarehouse'

    @classmethod
    def get_delivery_address(cls, shipments, name):
        res = {}
        for shipment in shipments:
            to_location = shipment.to_location
            address = to_location.address or \
                (to_location.parent.address if to_location.parent else None) or \
                (to_location and to_location.warehouse and to_location.warehouse.address or None)
            res[shipment.id] = address and address.id
        return res

    @classmethod
    def get_delivery_address_customer(cls, shipments, name):
        res = {}
        for shipment in shipments:
            res[shipment.id] = shipment.delivery_address.party.id if shipment.delivery_address\
                and shipment.delivery_address.party else None
        return res

    @classmethod
    def get_origin_purchase(cls, shipments, name):
        pool = Pool()
        Production = pool.get('production')

        shipment_ids = [x.id for x in shipments]
        productions = Production.search([('incoming_shipment', 'in',
            shipment_ids)])
        res = {}.fromkeys(shipment_ids, None)
        for production in productions:
            res[production.incoming_shipment.id] = (production.purchase_request
                and production.purchase_request.purchase
                and production.purchase_request.purchase.id)
        return res

    def get_origin_number(self, name):
        return self.origin.number if self.origin else None

    @classmethod
    def generate_internal_shipment(cls, clean=True):
        internal_shipments = super(
            ShipmentInternal, cls).generate_internal_shipment(clean=clean)
        # change wait to draft state
        cls.draft(internal_shipments)

    def get_order_moves(self, name):
        # Return inputs according the context (order by product, location...)

        Move = Pool().get('stock.move')
        context = Transaction().context
        if context.get('ordered_incoming_moves'):
            if not self.transit_location:
                return []
            incoming_moves = [i.id for i in self.incoming_moves]
            moves = Move.search([('id', 'in', incoming_moves)],
                order=[('product', 'ASC')])
            return [i.id for i in moves]
        else:
            outgoing_moves = [i.id for i in self.outgoing_moves]
            if context.get('ordered_location_moves'):
                moves = Move.search([('id', 'in', outgoing_moves)],
                    order=[('from_location', 'ASC')])
                return [i.id for i in moves]
            if context.get('ordered_product_moves'):
                moves = Move.search([('id', 'in', outgoing_moves)],
                    order=[('product', 'ASC')])
                return [i.id for i in moves]
            else:
                return [i.id for i in self.outgoing_moves]

    @fields.depends('from_location', 'to_location', 'effective_date')
    def on_change_with_transit_location(self, name=None):
        location = super(ShipmentInternal, self).on_change_with_transit_location()
        if location:
            # [APP-5853]: Since trytond 6.2 the on_change_with_transit_location
            # is modified in core stock module, and as it defines a transit
            # location in records where it was None in 6.0, some internal
            # shipments don't show its moves, so, we still assign None to
            # transit_location in old records in order to keep important data
            # available for users, but work as tryton core do in new records
            migration_date = datetime.date(2024, 9, 21)
            if (self.effective_date
                    and self.effective_date <= migration_date):
                return None
            return location

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, shipments):
        Product = Pool().get('product.product')
        Move = Pool().get('stock.move')
        Production = Pool().get('production')
        super(ShipmentInternal, cls).done(shipments)
        for shipment in shipments:
            # Deactivate movable locations after finishing the shipment
            location = shipment.from_location
            if location.movable:
                pending_moves = Move.search(
                    [['OR',
                        ('from_location', '=', location),
                        ('to_location', '=', location)],
                     ('state', 'not in', ['done', 'cancelled'])], limit=1)
                if not pending_moves:
                    stock = False
                    pbl = Product.products_by_location([location.id]).items()
                    for key, value in list(pbl):
                        if bool(value):
                            stock = True
                            break
                    if not stock:
                        location.active = False
                        location.save()
            # Assign related productions after finishing the shipment
            to_assign = [prod for prod in shipment.productions
                         if prod.state in ['draft', 'waiting']]
            if to_assign:
                Production.wait(to_assign)
                Production.assign_try(to_assign)


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'
    customer_code = fields.Function(fields.Char('Customer Code'),
        'get_customer_fields')
    customer_name = fields.Function(fields.Char('Customer Name'),
        'get_customer_fields')
    # map distribution field mapping by Industry
    map_distribution = fields.Function(fields.Char('Map Distribution'),
        'get_map_distribution')
    last_modification = fields.Function(fields.Char('Last Modification'),
        'get_last_modification')
    production = fields.Function(fields.Many2One('production', 'Production'),
        'get_production', searcher='search_production')
    customer = fields.Function(fields.Many2One('party.party', 'Customer'),
        'on_change_with_customer')
    purchasable = fields.Function(fields.Boolean('Purchasable'),
        'get_purchasable')
    producible = fields.Function(fields.Boolean('Producible'),
        'get_producible')
    serial_number_split = fields.Many2One('production.serial_number_split_start', 'Split Move')
    purchase_date = fields.Function(fields.Date('Purchase Date'),
                                    'get_purchase_date',
                                    searcher='search_purchase_date')
    component_breakdown = fields.Text('Component Breakdown',
                                      states={'invisible': ~Eval('component_breakdown')},
                                      depends=['component_breakdown'])

    @classmethod
    def __setup__(cls):
        super(StockMove, cls).__setup__()
        cls._order.insert(0, ('last_modification', 'DESC'))
        cls.lot.states['readonly'] = Eval('state').in_(['cancelled', 'assigned', 'done'])
        # overwrite readonly state because we don't want planned_date to be readonly if the move have a shipment
        cls.planned_date.states['readonly'] = Eval('state').in_(['cancelled', 'assigned', 'done'])
        cls.lot.context['locations'] = If(Eval('from_location'),
            [Eval('from_location')], [])
        if 'from_location' not in cls.lot.depends:
            cls.lot.depends.add('from_location')
        cls.lot.loading = 'lazy'

        if 'product' not in cls.lot.depends:
            cls.lot.depends.add('product')
        cls.lot.states['readonly'] |= ~Eval('product') | ~Eval('from_location')

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(StockMove, cls).search_rec_name(name, clause)
        return ['OR',
            domain,
            ('purchase',) + tuple(clause[1:]),
            ]

    def get_purchase_date(self, name):
        return self.purchase.purchase_date if self.purchase else None

    @classmethod
    def search_purchase_date(cls, name, clause):
        return [('origin.purchase.' + clause[0],) + tuple(clause[1:3])
                + ('purchase.line',) + tuple(clause[3:])]

    @fields.depends('from_location', 'product', 'unit_price')
    def on_change_with_unit_price(self):
        # Set stock move unit price to 0 if it belongs to an output production
        if self.product:
            from_type = self.from_location.type if self.from_location else None
            if self.product and from_type == 'production' and not self.unit_price:
                return Decimal(0)
        return self.unit_price

    @fields.depends('shipment')
    def on_change_with_customer(self, name=None):
        if self.shipment and isinstance(self.shipment, ShipmentOut):
            return self.shipment.customer.id

    @classmethod
    def generate_lot(cls, moves):
        Lot = Pool().get('stock.lot')
        Date = Pool().get('ir.date')
        for move in moves:
            if not move.lot and move.product.lot_is_required(move.from_location, move.to_location):
                seq = move.product.lot_sequence
                if seq:
                    lot_number = move.product.lot_sequence.get()
                elif move.product.account_category\
                        and move.product.account_category.lot_sequence:
                    seq = move.product.account_category.lot_sequence.id
                    lot_number = (move.product.account_category.lot_sequence
                        and move.product.account_category.lot_sequence.get())
                elif move.purchase and move.purchase.number:
                    p_number = move.purchase.parent.number[3:] if getattr(
                        move.purchase, 'parent', None) and\
                        move.purchase.parent else move.purchase.number[3:]
                    date = move.effective_date if move.effective_date else\
                        Date.today()
                    lot_number = p_number+"/"+date.strftime("%Y%m%d")[2:]
                    lot_search = Lot.search([
                        ('number', '=', lot_number),
                        ('product', '=', move.product)])
                    if lot_search:
                        new_lot, = lot_search
                        lot_number = False
                else:
                    # Cannot generate automatically lot number - no move origin and no product/category lot sequence
                    raise UserError(gettext('electrans.lot_number_not_generated',
                        product=move.product.rec_name))
                if lot_number:
                    new_lot, = Lot.create([{'number': lot_number,
                                            'product': move.product.id}])

                move.lot = new_lot.id
                move.save()

    # Method moved from nonconformity.py to avoid 2 definitions of the Move class
    @classmethod
    def _get_origin(cls):
        models = super(StockMove, cls)._get_origin()
        models.append('nonconformity.line')
        models.append('stock.move')
        return models

    def get_last_modification(self, name):
        def format_date(date):
            date_format = '%Y-%m-%d %H:%M:%S'
            return str(date.strftime(date_format))
        return format_date(self.write_date) if self.write_date else format_date(self.create_date)

    @staticmethod
    def order_last_modification(tables):
        table, _ = tables[None]
        return [Coalesce(table.write_date, table.create_date)]

    @classmethod
    def check_location_move(cls, moves):
        for move in moves:
            if move.from_location.type == 'view' or move.to_location.type == 'view':
                raise UserError(gettext('electrans.view_location',
                                        move=move.rec_name,
                                        from_location=move.from_location.rec_name,
                                        to_location=move.to_location.rec_name,
                                        from_location_type=move.from_location.type,
                                        to_location_type=move.to_location.type,
                                        product_type=move.product.type,
                                        consumable=move.product.consumable))

            if move.from_location == move.to_location:
                raise UserError(gettext('electrans.same_location',
                                move=move.rec_name,
                                from_location=move.from_location.rec_name,
                                to_location=move.to_location.rec_name,
                                from_location_type=move.from_location.type,
                                to_location_type=move.to_location.type,
                                product_type=move.product.type,
                                consumable=move.product.consumable))

    @classmethod
    def assign(cls, moves):
        cls.check_location_move(moves)
        super(StockMove, cls).assign(moves)

    def get_customer_fields(self, name):
        pool = Pool()
        Product = pool.get('product.product')
        customer = None
        for parent_field in ['shipment', 'origin']:
            if customer:
                break
            parent = getattr(self, parent_field, None)
            if parent:
                for attr_name in ['customer', 'party']:
                    if hasattr(parent, attr_name):
                        customer = getattr(parent, attr_name)
                        if customer:
                            break
        if not customer:
            return None
        with Transaction().set_context(sale_customer=customer.id):
            product, = Product.browse([self.product])
            return getattr(product, name)

    @classmethod
    def get_map_distribution(cls, records, name):
        res = {}
        for record in records:
            map_distribution = None
            if record.production_input and record.production_input.bom:
                for i in record.production_input.bom.inputs:
                    if i.product == record.product:
                        map_distribution = i.map_distribution
                        break
            res[record.id] = map_distribution
        return res

    def sort_quantities(self, quantities, locations, grouping):
        """
        Override to sort quantities using FIFO.
        'quantities' contains the result of Product.products_by_location()
        from Move.assign_try()
        e.g: quantities = [((location.id, product.id, lot.id), quantity)]
        quantities[0] = ((location.id, product.id, lot.id), quantity)
        quantities[0][0] = (location.id, product.id, lot.id)
        quantities[0][0][0-2] = location.id/product.id/lot.id
        """
        pool = Pool()
        Lot = pool.get('stock.lot')

        quantities = super().sort_quantities(quantities, locations, grouping)

        if 'lot' not in grouping:
            return quantities

        # 'grouping' is a tuple of keys for sorting 'quantities'.
        # location is the default key, and the others are added in 'grouping';
        # ('product', 'lot').
        # Assuming 'lot' key position: (quantities[0][0][x]) would crash if
        # another module adds an extra key, so we get the index + 1
        lot_idx = grouping.index('lot') + 1

        lots = Lot.search([('id', 'in', {q[0][lot_idx] for q in quantities})])
        lot2date = {lot.id: lot.create_date for lot in lots}

        # Sort by create date, and for id when dates are equal to each other
        return sorted(
            quantities,
            key=lambda x: (lot2date.get(x[0][lot_idx], datetime.datetime.max),
                           x[0][lot_idx]))

    @classmethod
    def assign_try(cls, moves, with_childs=True, grouping=('product', 'lot')):
        for move in moves:
            if (not move.unit_price and move.shipment
                    and move.shipment.__name__ == 'stock.shipment.in.return'):
                move.unit_price = 0
        return super().assign_try(moves,
                with_childs=with_childs, grouping=grouping)

    def get_lot_range_by_count(self, start_lot, count):
        " Return a lot range from start_lot to start_lot+count"
        r = [x for x in NUMBER_REGEXP.split(start_lot) if x != '']
        if not r:
            raise UserError(gettext('electrans.no_numbers'))
        start = int(r[-1])
        end = start + count - 1
        prefix = ''.join(r[0:len(r)-1])
        suffix_len = max(len(r[-1]), len(str(end)))
        return [prefix + str(n).zfill(suffix_len) for n in range(start, start + count)]

    def split_by_lot(self, quantity, uom, count=None, lots=None,
            start_lot=None, end_lot=None):
        """ Split moves by lots:
            * If lots is specified creates a move of quantity foreach lot
            * If start_lot and end_lot are specified create all the lots
              in range between start_lot and end_lot and uses the resulting
              lots as lots parameter
            * JAAC: If end_lot is None use a new way of doing lots
        """
        #if end_lot is not None:
        #    return super(self, quantity, uom, count, lots, start_lot, end_lot)
        #else:
        pool = Pool()
        Lot = pool.get('stock.lot')
        to_create = []
        lots = []
        transaction = Transaction()
        context = transaction.context
        if not count or quantity * count > self.quantity:
            raise UserError(gettext('electrans.impossible_split'))
        if (self.product and self.product.lot_sequence and
                context.get('generate_lots', False)):
            for move in range(0, count):
                new_lot, = Lot.create([{
                    'number': self.product.lot_sequence.get(),
                    'product': self.product.id,
                    }])
                lots.append(new_lot)
        else:
            if (not lots and start_lot and count and
                    context.get('generate_lots', False)):
                for number in self.get_lot_range_by_count(start_lot, count):
                    current_lots = Lot.search([
                        ('product', '=', self.product),
                        ('number', '=', number),
                    ], limit=1)
                    if current_lots:
                        lots.append(current_lots[0])
                        continue
                    to_create.append({
                        'product': self.product,
                        'number': number,
                    })
                if to_create:
                    lots += Lot.create(to_create)
        count = count or len(lots)
        moves = self.split(quantity, uom, count)
        # Last move must be without lot
        if count < self.quantity / quantity:
            lots.append(None)
        for lot, move in zip(lots, moves):
            move.lot = lot
            move.save()
        return moves

    def get_production(self, name):
        return self.production_input and self.production_input.id or self.production_output and  self.production_output.id

    @classmethod
    def search_production(cls, name, clause):
        return ['OR', ('production_input',) + tuple(clause[1:]), ('production_output',) + tuple(clause[1:])]

    def get_purchasable(self, name):
        return self.product.purchasable

    def get_producible(self, name):
        return self.product.producible

    def split(self, quantity, uom, count=None):
        transaction = Transaction()
        context = transaction.context
        if not context.get('generate_lots', False):
            pool = Pool()
            Move = pool.get('stock.move')
            original_lot = self.lot
            Move.write([self], {'lot': None})
            moves = super(StockMove, self).split(quantity, uom, count)
            Move.write([moves[-1] if moves else None], {'lot': original_lot})
            return moves
        return super(StockMove, self).split(quantity, uom, count)

    @fields.depends(
        'production_input', '_parent_production_input.update_outputs_cost_price', 'production_output',
        '_parent_production_output.update_outputs_cost_price')
    def on_change_with_unit_price_required(self, name=None):
        unit_price_required = super(StockMove, self).on_change_with_unit_price_required(name)
        if (self.production_input and not self.production_input.update_outputs_cost_price) or (
                self.production_output and not self.production_output.update_outputs_cost_price):
            unit_price_required = False
        return unit_price_required

    @fields.depends(
        'production_input', '_parent_production_input.update_outputs_cost_price', 'production_output',
        '_parent_production_output.update_outputs_cost_price')
    def on_change_with_cost_price_required(self, name=None):
        cost_price_required = super(StockMove, self).on_change_with_cost_price_required(name)
        if (self.production_input and not self.production_input.update_outputs_cost_price) or (
                self.production_output and not self.production_output.update_outputs_cost_price):
            cost_price_required = False
        return cost_price_required

    def _do(self):
        # Return None cost_price, cause we make unrequired cost_price, so the cost of the product doesn't modify
        if (self.production_input and not self.production_input.update_outputs_cost_price) or (
                self.production_output and not self.production_output.update_outputs_cost_price):
            return None, []
        return super(StockMove, self)._do()

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            company = Company(company)
            return company.currency.id


class SplitMoveStart(metaclass=PoolMeta):
    __name__ = 'stock.move.split.start'

    lot_sequence = fields.Boolean('Have the product a lot sequence?',
        states = {
                     'invisible': True,
                 },
        )
    generate_lots = fields.Boolean("Create/Keep Lots",
        help="If marked, when dividing moves lots will be kept or created.")

    @classmethod
    def __setup__(cls):
        super(SplitMoveStart, cls).__setup__()
        cls.start_lot.states['readonly'] = Bool(Eval('lot_sequence', False))


class SplitMove(metaclass=PoolMeta):
    __name__ = 'stock.move.split'

    def default_start(self, fields):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        default = super(SplitMove, self).default_start(fields)
        default['generate_lots'] = True
        product = self.record.product
        if product and product.lot_sequence:
            # get the next sequence number using the get_id function, and after
            # that update again the number_next field with the original value
            with Transaction().set_context(user=False, _check_access=False):
                with Transaction().set_user(0):
                    number_next = product.lot_sequence.number_next
                    next_seq = product.lot_sequence.get()
                    Sequence.write([product.lot_sequence], {
                        'number_next': number_next})
                    default['lot_sequence'] = True
                    default['start_lot'] = next_seq
        return default

    def transition_split(self):
        with Transaction().set_context(generate_lots=self.start.generate_lots):
            super(SplitMove, self).transition_split()
        return 'end'


class ShipmentInternalReport(JasperReport):
    __name__ = 'stock.shipment.internal.electrans'

    @classmethod
    def render(cls, report, data, model, ids):
        with Transaction().set_user(0):
            res = super(ShipmentInternalReport, cls).render(report, data,
                model, ids)
        return res


class ShipmentInternalReportOrderByLocation(JasperReport):
    __name__ = 'stock.shipment.internal.order.location.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(ordered_location_moves=True):
            return super(ShipmentInternalReportOrderByLocation, cls).execute(ids, data)


class ShipmentInternalReportOrderByProduct(JasperReport):
    __name__ = 'stock.shipment.internal.order.product.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(ordered_product_moves=True):
            return super(ShipmentInternalReportOrderByProduct, cls).execute(ids, data)


class ShipmentInternalReportOutgoingMovesOrderByProduct(JasperReport):
    __name__ = 'stock.shipment.internal.outgoing_moves.order.location.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(ordered_incoming_moves=True):
            return super(ShipmentInternalReportOutgoingMovesOrderByProduct, cls).execute(ids, data)


class FillLocationProducts(Wizard):
    "Conform Invoices"
    __name__ = 'fill.location.products'
    start = StateTransition()

    def transition_start(self):
        pool = Pool()
        Date = pool.get('ir.date')

        shipment = self.record
        from_location = shipment.from_location.id
        to_location = shipment.to_location.id
        with Transaction().set_context(stock_date_end=Date.today()):
            to_create = []
            Product = pool.get('product.product')
            Move = pool.get('stock.move')
            pbl = Product.products_by_location([from_location], True)
            for key, quantity in list(pbl.items()):
                if quantity > 0:
                    to_create.append({
                        'from_location': from_location,
                        'to_location': to_location,
                        'product': key[1],
                        'internal_quantity': quantity,
                        'quantity': quantity,
                        'uom': Product(key[1]).template.default_uom,
                        'description': Product(key[1]).rec_name,
                    })
            shipment.moves = Move.create(to_create)
            shipment.save()
        return 'end'


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    leftovers_location = fields.MultiValue(
        fields.Many2One(
            'stock.location', "Leftovers Location",
            domain=[
                ('type', '=', 'storage'),
            ], required=True))
    provisioning_movable_location = fields.MultiValue(
        fields.Many2One('stock.location', 'Provisioning Movable Location',
        domain=[
            ('type', '=', 'view'),
        ], required=True))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'leftovers_location' or field == 'provisioning_movable_location':
            return pool.get('stock.configuration.location')
        return super(Configuration, cls).multivalue_model(field)


class ConfigurationLocation(metaclass=PoolMeta):
    __name__ = 'stock.configuration.location'

    leftovers_location = fields.Many2One(
        'stock.location', "Leftovers Location",
        required=True,
        domain=[
            ('type', '=', 'storage'),
        ])
    provisioning_movable_location = fields.Many2One(
        'stock.location', "Provisioning Movable Location",
        required=True,
        domain=[
            ('type', '=', 'view'),
        ])


class FillToLocation(Wizard):
    'Fill to location'
    __name__ = 'stock.shipment.in.fill_to_location'

    start = StateView('stock.shipment.in.fill_to_location.start',
                      'electrans.shipment_in_fill_to_location_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Do', 'modify', 'tryton-ok')
                      ])
    modify = StateTransition()

    def default_start(self, fields):
        return {
            'warehouse': self.record.warehouse.id if self.record\
                and self.record.warehouse else None,
            }

    def transition_modify(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Location = pool.get('stock.location')

        moves = []
        parent_location = Location.search([
            ('parent', 'parent_of', [self.start.to_location]), 
            ('type', '=', 'warehouse')
            ], order=[('left', 'DESC')])
        parent_location = parent_location[0] if parent_location else None
        for shipment in self.records:
            if parent_location == shipment.warehouse:
                moves.extend(shipment.inventory_moves)
        Move.write(moves, {'to_location': self.start.to_location})
        return 'end'


class FillToLocationStart(ModelView):
    'Fill to location Start'
    __name__ = 'stock.shipment.in.fill_to_location.start'

    to_location = fields.Many2One('stock.location', 'To location', domain=[
        ('parent', 'child_of', Eval('warehouse')),
        ], required=True, depends=['warehouse'])
    warehouse = fields.Many2One('stock.location', 'Warehouse')


class SplitShipment(metaclass=PoolMeta):
    __name__ = 'stock.shipment.split'

    def transition_split(self):
        """
        Override to allow to split shipments in 'waiting' state
        """
        pool = Pool()
        Move = pool.get('stock.move')
        ShipmentInternal = pool.get('stock.shipment.internal')

        if (isinstance(self.record, ShipmentInternal) and
                self.record.state == 'waiting'):
            shipment, = ShipmentInternal.copy([self.record],
                default={'moves': None})
            if self.start.moves:
                Move.write(list(self.start.moves),
                    {'shipment': str(shipment)})
                shipment.productions = self.record.productions
                shipment.save()
        else:
            super().transition_split()
            # Keep link of the original shipment with the productions in the
            # internal shipment's copy
            if self.start.moves and self.start.moves[0].shipment:
                shipment = self.start.moves[0].shipment
                if isinstance(shipment, ShipmentInternal):
                    shipment.productions = self.record.productions
                    shipment.save()

        return 'end'


class ShipmentOutMerge(metaclass=PoolMeta):
    __name__ = 'shipment.out.merge'

    def do_merge(self, action):
        # merge comments of customer shipments and
        # leave blank weight and number_packages
        action, data = super(ShipmentOutMerge, self).do_merge(action)
        comments = []
        shipments = self.records
        for shipment in shipments:
            if shipment.comment:
                comments.append(shipment.comment)
        main_shipment, = shipments[:1]
        if comments:
            main_shipment.comment = ', '.join(comments)
        # Let user filling these fields with the value they want
        main_shipment.weight = None
        main_shipment.number_packages = None
        main_shipment.save()
        return action, data


class ModifyToLocationStart(ModelView):
    'Modify to location Start'
    __name__ = 'stock.shipment.internal.modify_to_location.start'

    to_location = fields.Many2One(
        'stock.location', 'To location',
        domain=[('type', '=', 'storage')],
        required=True)


class ModifyToLocation(Wizard):
    'Modify to location'
    __name__ = 'stock.shipment.internal.modify_to_location'

    start = StateView('stock.shipment.internal.modify_to_location.start',
                      'electrans.shipment_internal_modify_to_location_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Modify', 'modify', 'tryton-ok')
                      ])
    modify = StateTransition()

    def transition_modify(self):
        pool = Pool()
        Move = pool.get('stock.move')
        ShipmentInternal = pool.get('stock.shipment.internal')

        to_write, to_save = [], []
        for shipment in self.records:
            if shipment.state not in ['request', 'draft']:
                raise AccessError(gettext(
                    'electrans.msg_to_location_modify_shipment_not_draft'))
            shipment.to_location = self.start.to_location
            to_save.append(shipment)
            to_write.extend((list(shipment.moves), 
                             {'to_location': self.start.to_location}))
        if to_write:
            Move.write(*to_write)
        if to_save:
            ShipmentInternal.save(to_save)
        return 'end'

class LocateProductsStart(ModelView):
    'Locate Products Start'
    __name__ = 'stock.shipment.internal.locate_products.start'

    to_location = fields.Many2One(
        'stock.location', 'To location',
        domain=[
            ('type', '=', 'storage'),
            ['OR', 
                ('parent', '=', Eval('shipment_to_location', -1)), 
                ('parent', 'child_of', Eval('shipment_to_location', -1)),
            ],   
        ],
        required=True)
    moves = fields.One2Many(
        'stock.move', None, 'Moves',
        domain=[
            ('shipment.id', '=', Eval('context', {}).get('active_id', 0),
             'stock.shipment.internal'),
        ],
        filter=[('state', '=', 'draft')],
        required=True)
    shipment_to_location = fields.Many2One(
        'stock.location', 'Warehouse',
        readonly=True)


class LocateProducts(Wizard):
    'Locate Products'
    __name__ = 'stock.shipment.internal.locate_products'

    start = StateView(
        'stock.shipment.internal.locate_products.start',
        'electrans.shipment_internal_locate_products_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Locate', 'locate', 'tryton-ok'),
        ])
    
    locate = StateTransition()
    
    def default_start(self, fields):
        moves = [m.id for s in self.records for m in s.incoming_moves 
                 if m.state == 'draft']
         
        return {
            'moves': moves,
            'shipment_to_location': self.record.to_location.id,
        }
    
    def transition_locate(self):
        pool = Pool()
        Move = pool.get('stock.move')
        
        Move.write(list(self.start.moves),
                   {'to_location': self.start.to_location})
        return 'end'


class AddComponentBreakdownStart(ModelView):
    'Add Component Breakdown Text To Moves Start'
    __name__ = 'stock.shipment.out.add_component_breakdown.start'

    moves_to_update = fields.One2Many(
        'stock.move', None, "Moves To Update",
        required=True,
        domain=[('id', 'in', Eval('domain_moves'))],
        depends=['domain_moves'])
    domain_moves = fields.Many2Many(
        'stock.move', None, None, 'Domain Moves')


class AddComponentBreakdown(Wizard):
    'Add Component Breakdown Text To Moves'
    __name__ = 'stock.shipment.out.add_component_breakdown'

    start = StateView('stock.shipment.out.add_component_breakdown.start',
                      'electrans.shipment_out_add_component_breakdown_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Update', 'update_component_breakdown', 'tryton-ok')
                      ])
    update_component_breakdown = StateTransition()

    def default_start(self, fields):
        for shipment in self.records:
            if shipment.state != 'packed':
                raise UserError(gettext('electrans.shipment_out_not_packed', shipment=shipment.number))
        # In case that more than 1 shipment has been selected, there will appear all the allowed moves of each shipment
        # Only allow to select moves that his quantity is 1 and the product is producible
        moves = [m.id for shipment in self.records for m in shipment.outgoing_moves
                 if m.product and m.product.producible and m.quantity == 1]
        return {
            'moves_to_update': moves,
            'domain_moves': moves,
            }

    def transition_update_component_breakdown(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Warning = pool.get('res.user.warning')
        moves_to_save = []
        warnings_moves = []
        for m in self.start.moves_to_update:
            # Add the string in the same language of the customer
            customer_lang = m.shipment.customer.lang
            with Transaction().set_context(language=customer_lang.code if customer_lang else 'es'):
                component_breakdown = [gettext('electrans.each_unit_composed_of')]
            # Only update Component breakdown if the lot's move has an output production and the output is quantity 1
            if (m.lot and m.lot.output_production and m.lot.output_production.quantity == 1 and
                    len(m.lot.output_production.outputs) == 1):
                # Order inputs by product code and quantity, so the delivery note will group the moves correctly
                sorted_inputs = sorted(m.lot.output_production.inputs, key=lambda x: (x.product.code, x.quantity))
                for _input in sorted_inputs:
                    if _input.product and _input.uom:
                        # If the input's product has serial_number checked, then add the serial number too
                        if _input.product.serial_number and _input.lot:
                            component_breakdown.append(
                                str(_input.quantity) + ' ' + _input.uom.symbol + '  ' +
                                _input.product.rec_name + ' S/N: ' + _input.lot.rec_name)
                        else:
                            component_breakdown.append(
                                str(_input.quantity) + ' ' + _input.uom.symbol + '  ' + _input.product.rec_name)
                # Update move component_breakdown
                m.component_breakdown = '\n'.join(component_breakdown)
                moves_to_save.append(m)
            else:
                warnings_moves.append(m)
        if warnings_moves:
            lots = ', '.join(m.lot.rec_name for m in warnings_moves if m.lot)
            # Show warning if the quantity of the move's lot output production is different from 1
            # If "Ignore always the warning" was checked and some lot's move is changed it will be shown again
            key = 'add_component_breakdown_shipment_lots%s' % lots
            if Warning.check(key):
                raise UserWarning(key, gettext(
                    'electrans.lot_output_production_not_quantity_one', lots=lots))
        if moves_to_save:
            Move.save(moves_to_save)
        return 'end'
