===============
SCENARIO INVENTORY
=================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install purchase::

    >>> config = activate_modules(['electrans','purchase'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']
    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Bienes")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.salable = True
    >>> template.list_price = Decimal('40')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.manufacturer_name = 'NameManuf'
    >>> product, = template.products
    >>> product.cost_price = Decimal('25')
    >>> template.save()
    >>> product, = template.products
    >>> product.manufacturer_code = "34122"

Create product 2::

    >>> template2 = ProductTemplate()
    >>> template2.name = 'product2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.purchasable = True
    >>> template2.salable = True
    >>> template2.list_price = Decimal('40')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.account_category = account_category
    >>> template2.manufacturer_name = 'NameManuf'
    >>> product2, = template.products
    >>> product2.cost_price = Decimal('25')
    >>> template2.save()
    >>> product2, = template.products
    >>> product2.manufacturer_code = "34122"


Create Lot::

    >>> Lot = Model.get('stock.lot')
    >>> lot = Lot(number='LOT001', product=product)
    >>> lot2 = Lot(number='LOT002', product=product2)
    >>> Lot.save([lot, lot2])

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.lot = lot
    >>> inventory_line.quantity = 200.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory_line2 = inventory.lines.new(product=product2)
    >>> inventory_line2.lot = lot2
    >>> inventory_line2.quantity = 200.0
    >>> inventory_line2.expected_quantity = 0.0

    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Create Shipment Internal::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> shipment_internal = ShipmentInternal()
    >>> shipment_internal.from_location = storage
    >>> warehouse, = Location.find([('code', '=', 'WH')])
    >>> storage1 = Location(name="Storage 1", parent=warehouse)
    >>> storage1.code = 'ST11'
    >>> storage1.save()
    >>> shipment_internal.to_location = storage1
    >>> move = shipment_internal.moves.new()
    >>> move.product = product
    >>> move.lot = lot
    >>> move.uom = unit
    >>> move.quantity = 50
    >>> move.from_location = storage
    >>> move.to_location = storage1
    >>> move2 = shipment_internal.moves.new()
    >>> move2.product = product2
    >>> move2.uom = unit
    >>> move2.quantity = 50
    >>> move2.from_location = storage
    >>> move2.to_location = storage1
    >>> shipment_internal.save()
    >>> shipment_internal.click('wait')
    >>> shipment_internal.click('assign_try')
    True
    >>> shipment_internal.state
    'assigned'


Create new Inventory to check raise (Product with lot number)::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.lot = lot
    >>> inventory_line.quantity = 0.0
    >>> inventory_line.expected_quantity = 200.0
    >>> inventory.click('confirm') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.ValidationError: Cannot complete the inventory because the new quantity is less than the reserved quantity (Lot n./Product code):
    LOT001 - None -

Create new Inventory to check raise (Product without lot number)::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product2)
    >>> inventory_line.quantity = 200.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: To move product "product" you must provide a stock lot. -

Create new Inventory  (Product with lot number)::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.lot = lot
    >>> inventory_line.quantity = 150.0
    >>> inventory_line.expected_quantity = 200.0
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Create new inventory using "complete_lines" Button without update any quantity::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.click('complete_lines')
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'


