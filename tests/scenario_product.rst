===================
Production Scenario
===================

""""
- Clean input lots
- Create internal shipment
""""

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import config, Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> today = datetime.date.today()
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> yesterday = today - relativedelta(days=1)
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, create_chart, get_accounts, create_tax

Create database::

    >>> config = config.set_trytond()
    >>> config.pool.test = True

Install production Module::

    >>> config = activate_modules(['electrans'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create taxes::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()
    >>> tax2 = create_tax(Decimal('.9'))
    >>> tax2.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Bienes")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.supplier_taxes.append(tax2)
    >>> account_category.save()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.deprecated = True
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal(30)
    >>> template.cost_price = Decimal(20)
    >>> template.account_category = account_category
    >>> template.save()
    >>> product.template = template
    >>> product.producible = True
    >>> product.save()
    >>> product2_ = Product()
    >>> template2_ = ProductTemplate()
    >>> template2_.name = 'product'
    >>> template2_.deprecated = False
    >>> template2_.default_uom = unit
    >>> template2_.type = 'goods'
    >>> template2_.list_price = Decimal(30)
    >>> template2_.cost_price = Decimal(20)
    >>> template2_.account_category = account_category
    >>> template2_.save()
    >>> product2_.template = template2_
    >>> product2_.save()

Test deprecated product rec_name::

    >>> "!" in template.rec_name
    True
    >>> "!" in product.rec_name
    True
