===================
Production Scenario
===================

""""
- Clean input lots
- Create internal shipment
""""

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import config, Model, Wizard
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Create database::

    >>> config = config.set_trytond()
    >>> config.pool.test = True

Install production Module::

    >>> Module = Model.get('ir.module.module')
    >>> modules = Module.find([('name', '=', 'electrans')])
    >>> Module.install([x.id for x in modules], config.context)
    >>> Wizard('ir.module.module.install_upgrade').execute('upgrade')

Create company::

    >>> Sequence = Model.get('ir.sequence')
    >>> Currency = Model.get('currency.currency')
    >>> CurrencyRate = Model.get('currency.currency.rate')
    >>> Company = Model.get('company.company')
    >>> Party = Model.get('party.party')
    >>> company_config = Wizard('company.company.config')
    >>> company_config.execute('company')
    >>> company = company_config.form
    >>> party = Party(name='Dunder Mifflin')
    >>> party.save()
    >>> company.party = party
    >>> currencies = Currency.find([('code', '=', 'USD')])
    >>> if not currencies:
    ...     currency = Currency(name='Euro', symbol=u'$', code='USD',
    ...         rounding=Decimal('0.01'), mon_grouping='[3, 3, 0]',
    ...         mon_decimal_point=',')
    ...     currency.save()
    ...     CurrencyRate(date=today + relativedelta(month=1, day=1),
    ...         rate=Decimal('1.0'), currency=currency).save()
    ... else:
    ...     currency, = currencies
    >>> company.currency = currency
    >>> company_config.execute('add')
    >>> company, = Company.find()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal(30)
    >>> template.cost_price = Decimal(20)
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Create Components::

    >>> component1 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'component 1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.list_price = Decimal(5)
    >>> template1.cost_price = Decimal(1)
    >>> template1.save()
    >>> component1.template = template1
    >>> component1.save()

    >>> meter, = ProductUom.find([('name', '=', 'Meter')])
    >>> centimeter, = ProductUom.find([('name', '=', 'centimeter')])
    >>> component2 = Product()
    >>> template2 = ProductTemplate()
    >>> template2.name = 'component 2'
    >>> template2.default_uom = meter
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal(7)
    >>> template2.cost_price = Decimal(5)
    >>> template2.save()
    >>> component2.template = template2
    >>> component2.save()

Create Bill of Material::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> bom.inputs.append(input1)
    >>> input1.product = component1
    >>> input1.quantity = 5
    >>> input2 = BOMInput()
    >>> bom.inputs.append(input2)
    >>> input2.product = component2
    >>> input2.quantity = 150
    >>> input2.uom = centimeter
    >>> output = BOMOutput()
    >>> bom.outputs.append(output)
    >>> output.product = product
    >>> output.quantity = 1
    >>> bom.save()

    >>> ProductBom = Model.get('product.product-production.bom')
    >>> product.boms.append(ProductBom(bom=bom))
    >>> product.save()

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> InventoryLine = Model.get('stock.inventory.line')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> warehouse = storage.parent
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line1 = InventoryLine()
    >>> inventory.lines.append(inventory_line1)
    >>> inventory_line1.product = component1
    >>> inventory_line1.quantity = 20
    >>> inventory_line2 = InventoryLine()
    >>> inventory.lines.append(inventory_line2)
    >>> inventory_line2.product = component2
    >>> inventory_line2.quantity = 6
    >>> inventory.save()
    >>> Inventory.confirm([inventory.id], config.context)
    >>> inventory.state
    u'done'

Production Configuration::

    >>> ProductionConfiguration = Model.get('production.configuration')
    >>> sequence_lot, = Sequence.find([('code', '=', 'stock.lot')], limit=1)
    >>> production_conf, = ProductionConfiguration.find([], limit=1)
    >>> production_conf.output_lot_sequence = sequence_lot
    >>> production_conf.supply_period = Decimal('40')
    >>> production_conf.save()

Create Product Lot::

    >>> Lot = Model.get('stock.lot')
    >>> lot1 = Lot()
    >>> lot1.product = component1
    >>> lot1.number = 'LC0001'
    >>> lot1.save()
    >>> lot2 = Lot()
    >>> lot2.product = component2
    >>> lot2.number = 'LC0002'
    >>> lot2.save()

Set lot required in production::

    >>> LotType = Model.get('stock.lot.type')
    >>> production_lot_type, = LotType.find([('name', '=', 'Production')], limit=1)
    >>> template.lot_required.append(production_lot_type)
    >>> template.save()

Make a production::

    >>> Production = Model.get('production')
    >>> production = Production()
    >>> production.product = product
    >>> production.bom = bom
    >>> production.quantity = 2
    >>> sorted([i.quantity for i in production.inputs]) == [10, 300]
    True
    >>> output, = production.outputs
    >>> output.quantity == 2
    True
    >>> production.cost
    Decimal('25.0')
    >>> production.save()
    >>> Production.wait([production.id], config.context)
    >>> production.state
    u'waiting'
    >>> Production.assign_try([production.id], config.context)
    True
    >>> production.reload()
    >>> production.rec_name.startswith('*')
    True
    >>> all(i.state == 'assigned' for i in production.inputs)
    True
    >>> Production.run([production.id], config.context)
    >>> production.reload()
    >>> all(i.state == 'done' for i in production.inputs)
    True
    >>> len(set(i.effective_date == today for i in production.inputs))
    1
    >>> output, = production.outputs
    >>> output.lot.number
    u'1'
    >>> Production.done([production.id], config.context)
    >>> production.reload()
    >>> output, = production.outputs
    >>> output.state
    u'done'
    >>> output.effective_date == production.effective_date
    True
    >>> config._context['locations'] = [storage.id]
    >>> product.reload()
    >>> product.quantity == 2
    True

Clean lots in a production::

    >>> production = Production()
    >>> production.product = product
    >>> production.bom = bom
    >>> production.quantity = 2
    >>> production.save()
    >>> input1, input2 = production.inputs
    >>> input1.lot = lot1
    >>> input1.save()
    >>> input1.reload()
    >>> input1.lot.number
    u'LC0001'
    >>> input2.lot = lot2
    >>> input2.save()
    >>> input2.reload()
    >>> input2.lot.number
    u'LC0002'
    >>> Production.clean_lots([production.id], config.context)
    >>> production.reload()
    >>> input1, input2 = production.inputs
    >>> input1.lot
    >>> input2.lot

Create new warehouse::

    >>> provisioning_location = Location()
    >>> provisioning_location.name = 'Provisioning'
    >>> provisioning_location.type = 'storage'
    >>> provisioning_location.save()
    >>> storage.provisioning_location = provisioning_location
    >>> storage.save()

    >>> input_location2 = Location()
    >>> input_location2.name = 'Input 2'
    >>> input_location2.type = 'storage'
    >>> input_location2.save()
    >>> output_location2 = Location()
    >>> output_location2.name = 'Output 2'
    >>> output_location2.type = 'storage'
    >>> output_location2.save()
    >>> storage_location2 = Location()
    >>> storage_location2.name = 'Storage 2'
    >>> storage_location2.type = 'storage'
    >>> storage_location2.save()

    >>> production2 = Location()
    >>> production2.name = 'Production'
    >>> production2.type = 'production'
    >>> production2.save()

    >>> warehouse2 = Location()
    >>> warehouse2.name = "Warehouse 2"
    >>> warehouse2.type = 'warehouse'
    >>> warehouse2.input_location = input_location2
    >>> warehouse2.output_location = output_location2
    >>> warehouse2.storage_location = storage_location2
    >>> warehouse2.production_location = production2
    >>> warehouse2.save()

Create an Inventory::

    >>> inventory = Inventory()
    >>> inventory.location = storage_location2
    >>> inventory_line1 = InventoryLine()
    >>> inventory.lines.append(inventory_line1)
    >>> inventory_line1.product = component1
    >>> inventory_line1.quantity = 20
    >>> inventory_line2 = InventoryLine()
    >>> inventory.lines.append(inventory_line2)
    >>> inventory_line2.product = component2
    >>> inventory_line2.quantity = 400
    >>> inventory.save()
    >>> Inventory.confirm([inventory.id], config.context)
    >>> inventory.state
    u'done'

Make two production and internal shipments::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> production2 = Production()
    >>> production2.product = product
    >>> production2.warehouse = warehouse
    >>> production2.bom = bom
    >>> production2.quantity = 2
    >>> production2.destination_warehouse = warehouse2
    >>> production2.save()
    >>> production3 = Production()
    >>> production3.product = product
    >>> production3.warehouse = warehouse
    >>> production3.bom = bom
    >>> production3.quantity = 2
    >>> production3.destination_warehouse = warehouse2
    >>> production3.save()
    >>> Production.wait([production2.id, production3.id], config.context)
    >>> production_internal = Wizard('production.electrans.internal', [production2, production3])
    >>> production_internal.execute('create_internal')
    >>> internal, = ShipmentInternal.find([], limit=1)
    >>> internal.reference
    u'3, 4'
    >>> move1, move2 = internal.moves
    >>> move1.quantity
    10.0
    >>> move2.quantity
    597.0
