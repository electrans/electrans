===============================
Work Project Structure Scenario
===============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import config, Model, Wizard
    >>> today = datetime.date.today()

Create database::

    >>> config = config.set_trytond()
    >>> config.pool.test = True

Install sale::

    >>> Module = Model.get('ir.module.module')
    >>> module, = Module.find([('name', '=', 'electrans')])
    >>> Module.install([module.id], config.context)
    >>> Wizard('ir.module.module.install_upgrade').execute('upgrade')

Create company::

    >>> Currency = Model.get('currency.currency')
    >>> CurrencyRate = Model.get('currency.currency.rate')
    >>> currencies = Currency.find([('code', '=', 'USD')])
    >>> if not currencies:
    ...     currency = Currency(name='U.S. Dollar', symbol='$', code='USD',
    ...         rounding=Decimal('0.01'), mon_grouping='[3, 3, 0]',
    ...         mon_decimal_point='.', mon_thousands_sep=',')
    ...     currency.save()
    ...     CurrencyRate(date=today + relativedelta(month=1, day=1),
    ...         rate=Decimal('1.0'), currency=currency).save()
    ... else:
    ...     currency, = currencies
    >>> Company = Model.get('company.company')
    >>> Party = Model.get('party.party')
    >>> company_config = Wizard('company.company.config')
    >>> company_config.execute('company')
    >>> company = company_config.form
    >>> party = Party(name='Dunder Mifflin')
    >>> party.save()
    >>> company.party = party
    >>> company.currency = currency
    >>> company_config.execute('add')
    >>> company, = Company.find([])

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create fiscal year::

    >>> FiscalYear = Model.get('account.fiscalyear')
    >>> Sequence = Model.get('ir.sequence')
    >>> SequenceStrict = Model.get('ir.sequence.strict')
    >>> fiscalyear = FiscalYear(name=str(today.year))
    >>> fiscalyear.start_date = today + relativedelta(month=1, day=1)
    >>> fiscalyear.end_date = today + relativedelta(month=12, day=31)
    >>> fiscalyear.company = company
    >>> post_move_seq = Sequence(name=str(today.year), code='account.move',
    ...     company=company)
    >>> post_move_seq.save()
    >>> fiscalyear.post_move_sequence = post_move_seq
    >>> invoice_seq = SequenceStrict(name=str(today.year),
    ...     code='account.invoice', company=company)
    >>> invoice_seq.save()
    >>> fiscalyear.out_invoice_sequence = invoice_seq
    >>> fiscalyear.in_invoice_sequence = invoice_seq
    >>> fiscalyear.out_credit_note_sequence = invoice_seq
    >>> fiscalyear.in_credit_note_sequence = invoice_seq
    >>> fiscalyear.save()
    >>> FiscalYear.create_period([fiscalyear.id], config.context)


Create chart of accounts::

    >>> AccountTemplate = Model.get('account.account.template')
    >>> Account = Model.get('account.account')
    >>> account_template = AccountTemplate.find([
    ...     ('parent', '=', None),
    ...     ('name', '=', 'Plan General Contable 2008'),
    ...     ])[0]
    >>> create_chart = Wizard('account.create_chart')
    >>> create_chart.execute('account')
    >>> create_chart.form.account_template = account_template
    >>> create_chart.form.company = company
    >>> create_chart.form.account_code_digits = 8
    >>> create_chart.execute('create_account')
    >>> receivable, = Account.find([
    ...         ('kind', '=', 'receivable'),
    ...         ('company', '=', company.id),
    ...         ], limit=1)
    >>> payable, = Account.find([
    ...         ('kind', '=', 'payable'),
    ...         ('company', '=', company.id),
    ...         ], limit=1)
    >>> revenue, = Account.find([
    ...         ('kind', '=', 'revenue'),
    ...         ('company', '=', company.id),
    ...         ], limit=1)
    >>> expense, = Account.find([
    ...         ('kind', '=', 'expense'),
    ...         ('company', '=', company.id),
    ...         ], limit=1)
    >>> create_chart.form.account_receivable = receivable
    >>> create_chart.form.account_payable = payable
    >>> create_chart.execute('create_properties')

Create Employee::

    >>> Employee = Model.get('company.employee')
    >>> party = Party(name='Employee')
    >>> party.save()
    >>> employee = Employee()
    >>> employee.party = party
    >>> employee.company = company
    >>> cost = employee.cost_prices.new()
    >>> cost.date = today
    >>> cost.cost_price = Decimal('10.0')
    >>> employee.save()
    >>> user, = User.find([])
    >>> user.employees.append(employee)
    >>> user.employee = employee
    >>> user.save()
    >>> config._context = User.get_preferences(True, config.context)

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.category = category
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('8')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_expense = expense
    >>> template.account_revenue = revenue
    >>> template.save()
    >>> product.template = template
    >>> product.save()

    >>> service = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_expense = expense
    >>> template.account_revenue = revenue
    >>> template.save()
    >>> service.template = template
    >>> service.save()


Create payment term::

    >>> PaymentTerm = Model.get('account.invoice.payment_term')
    >>> PaymentTermLine = Model.get('account.invoice.payment_term.line')
    >>> payment_term = PaymentTerm(name='Direct')
    >>> payment_term_line = PaymentTermLine(type='remainder', days=0)
    >>> payment_term.lines.append(payment_term_line)
    >>> payment_term.save()

Create a Project::

    >>> Project = Model.get('work.project')
    >>> project = Project()
    >>> project.code = 'MAIN'
    >>> project.party = customer
    >>> project.save()

Create a Sale::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.project = project
    >>> sale.description = 'Work Project Structure'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.description = 'Product Line'
    >>> sale_line.quantity = 10
    >>> sale_line = sale.lines.new()
    >>> sale_line.type = 'title'
    >>> sale_line.description = 'Title'
    >>> sale.save()
    >>> _, sale_line = sale.lines
    >>> child_sale_line = sale.lines.new()
    >>> child_sale_line.parent = SaleLine(sale_line.id)
    >>> child_sale_line.product = service
    >>> child_sale_line.description = 'Service Line'
    >>> child_sale_line.quantity = 5
    >>> sale.save()

Chapter Number must be computed correctly::

    >>> project_line, title_line, service_line = sale.lines
    >>> project_line.chapter_number
    '1'
    >>> len(project_line.childs)
    0
    >>> title_line.chapter_number
    '2'
    >>> child_line, = title_line.childs
    >>> child_line.id == service_line.id
    True
    >>> service_line.chapter_number
    '2.1'

When the sale is quoted a structure is created::

    >>> WBS = Model.get('work.breakdown.structure')
    >>> sale.click('quote')
    >>> project.reload()
    >>> non_child_line, child_line = project.breakdown_structure_tree
    >>> non_child_line.name
    u'Product Line'
    >>> child_line.name
    u'Title'
    >>> child_child_line, = child_line.childs
    >>> child_child_line.name
    u'Service Line'

Going back to draft deletes the structure::

    >>> sale.click('draft')
    >>> project.reload()
    >>> len(project.breakdown_structure)
    0
    >>> sale.click('quote')

If we relate another sale to the same project the structure is copied::

    >>> new_sale = Sale()
    >>> new_sale.party = customer
    >>> new_sale.payment_term = payment_term
    >>> new_sale.project = project
    >>> new_sale.click('quote')
    >>> non_child_line, child_line = new_sale.lines_tree
    >>> non_child_line.description
    u'Product Line'
    >>> len(non_child_line.childs)
    0
    >>> child_line.description
    u'Title'
    >>> child_child_line, = child_line.childs
    >>> child_child_line.description
    u'Service Line'

Invoice sales and check that the full strutucutre is copied on invoice::

    >>> sale.click('confirm')
    >>> process_lines = Wizard('sale.process.lines', [sale])
    >>> process_lines.form.lines.append(SaleLine(sale.lines[0].id))
    >>> process_lines.execute('process_lines')
    >>> sale.reload()
    >>> sale.state
    u'processing'
    >>> invoice, = sale.invoices
    >>> product_line, title_line, service_line = invoice.lines
    >>> product_line.description
    u'Product Line'
    >>> product_line.type
    u'line'
    >>> title_line.description
    u'Title'
    >>> service_line.description
    u'Service Line'

Reprocess Sale and check the structure is fully copied::

    >>> process_lines = Wizard('sale.process.lines', [sale])
    >>> process_lines.execute('process_order')
    >>> sale.reload()
    >>> _, invoice, = sale.invoices
    >>> product_line, title_line, service_line = invoice.lines
    >>> product_line.description
    u'Product Line'
    >>> product_line.type
    u'comment'
    >>> title_line.description
    u'Title'
    >>> service_line.description
    u'Service Line'
