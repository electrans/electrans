# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.modules.company.tests.test_module import (
    create_company, set_company, create_employee)
from trytond.modules.account.tests import get_fiscalyear
from trytond.modules.account_invoice.tests import set_invoice_sequences
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.model.exceptions import AccessError, RequiredValidationError
from trytond.exceptions import UserWarning, UserError
from trytond.modules.stock.exceptions import MoveFutureWarning
from trytond.modules.electrans.tests import (
    create_fiscalyear_and_chart, get_accounts, create_production,
    create_account_category, create_product, _create_bom)
from trytond.i18n import gettext
import datetime

today = datetime.date.today()


class TestElectransCase(ModuleTestCase):
    'Test Electrans module'
    module = 'electrans'

    def create_internal_order_point(self, product, warehouse_loc):
        pool = Pool()
        OrderPoint = pool.get('stock.order_point')
        order_point = OrderPoint()
        order_point.product = product
        order_point.warehouse_location = warehouse_loc
        order_point.type = 'purchase'
        order_point.min_quantity = 150
        order_point.target_quantity = 200
        order_point.max_quantity = 500
        order_point.save()

    @with_transaction()
    def test_add_unit_price_in_requests(self):
        "Test Unit Price Requests"
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        PurchaseRequisition = pool.get('purchase.requisition')
        # PurchaseLine = pool.get('purchase.line')
        # Purchase = pool.get('purchase.purchase')
        Tax = pool.get('account.tax')
        OrderPoint = pool.get('stock.order_point')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Get warehouses and production location
            production_loc, = Location.search([('code', '=', 'PROD')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.save()
            # Create employee (this function creates a party internally)
            employee1 = create_employee(company, 'supplier1')
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue, "Servicios")
            account_category_goods = create_account_category(
                tax, expense, revenue)
            template, product = create_product('Product', account_category)
            template.producible = True
            template.purchasable = True
            template.save()
            template2, component1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template2.purchasable = True
            template2.producible = True
            template2.save()
            # if request comes from a production (SdC)
            production1 = create_production(1, product, unit,
                                                 warehouse, production_loc,
                                                 company)
            Production.create_purchase_request([production1])
            self.assertTrue(bool(production1.purchase_request))
            request1, = PurchaseRequest.search([
                ('origin', '=', 'production,' + str(production1.id))
            ])
            # check that request comes from a production
            self.assertTrue(request1.production_origin)
            request1.party = employee1.party
            request1.unit_price = Decimal('2.56')
            request1.save()
            WizardCreatePurchase = pool.get(
                'purchase.request.create_purchase',
                type='wizard')
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request1]
            wizard_create_purchase.transition_start()
            purchase1 = request1.purchase
            purchase_line1, = purchase1.lines
            self.assertEqual(request1.state, 'purchased')
            self.assertEqual(purchase_line1.gross_unit_price,
                             request1.unit_price)

            # If request comes from a requisition line
            requisition1, = PurchaseRequisition.create([{
                'number': 'PET00001',
                'supply_date': datetime.date(2022, 10, 30),
                'employee': employee1,
                'state': 'draft',
            }])
            requisition_line1, = PurchaseRequisitionLine.create([{
                'product': component1.id,
                'quantity': 1.00,
                'unit_price': Decimal('7.77'),
                'requisition': requisition1.id,
                'supplier': employee1.party,
                'unit': unit.id,
            }])
            PurchaseRequisition.wait([requisition1])
            PurchaseRequisition.approve([requisition1])
            self.assertEqual(requisition1.state, 'processing')
            """req_line1 = str(requisition_line1.id)
            request2, = PurchaseRequest.search([
                ('origin', '=', 'purchase.requisition.line,' + req_line1)
            ])
            # check that request NOT comes from a production
            self.assertFalse(request2.production_origin)
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request2]
            with Transaction().set_context(active_ids=[request2]):
                wizard_create_purchase.transition_start()
                wizard_create_purchase._execute('create_')
            purchase2 = request2.purchase
            purchase_line2, = purchase2.lines
            self.assertEqual(purchase_line2.gross_unit_price,
                             requisition_line1.unit_price)
            product_purchase_lines = PurchaseLine.search([])
            self.assertEqual(len(product_purchase_lines), 2)
            """
            # If request comes from stock order point
            self.create_internal_order_point(component1, warehouse)
            order_points = OrderPoint.search([])
            self.assertEqual(len(order_points), 1)
            """Create Purchase Requests from order points
            purchase3, = Purchase.create([{
                'party': employee1.party,faccount_category
                'warehouse': warehouse.id,
            }])
            purchase_line3, = PurchaseLine.create([{
                'product': component1.id,
                'quantity': 1.00,
                'unit_price': Decimal('1.77'),
                'purchase': purchase3.id,
                'unit': unit.id,
            }])"""
            PurchaseRequest.generate_requests()
            # Convert Purchase Request to Purchase
            purchase_request, = PurchaseRequest.search([
                ('origin', 'ilike', 'stock.order_point%')],
                limit=1)
            purchase_request.party = employee1.party
            purchase_request.save()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [purchase_request]
            wizard_create_purchase.transition_start()
            # purchase4 = purchase_request.purchase
            # purchase_line4, = purchase4.lines
            # TODO: check if the purchase line has been filled with the
            #  gross_unit_price of last_purchase_line with same product
            # and supplier
            # NOTE: We couldn't check that cause the purchase lines are created
            # with same create_date and write_date is None
            """self.assertEqual(purchase_line4.gross_unit_price,
                             purchase_line3.gross_unit_price)"""

    @with_transaction()
    def test_unit_price_add_to_create_purchase(self):
        "Test Unit Price Add to Create Purchase"
        pool = Pool()
        Location = pool.get('stock.location')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequestQuotation = pool.get('purchase.request.quotation')
        PurchaseRequestQuotationLine = pool.get(
            'purchase.request.quotation.line')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardAddToPurchase = pool.get(
            'purchase.request.add_to_purchase', type='wizard')
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        WizardCreateQuotation = pool.get(
            'purchase.request.quotation.create', type='wizard')
        company = create_company()
        with set_company(company):
            warehouse, = Location.search([('code', '=', 'WH')])
            # Create some parties
            supplier1 = create_employee(company, 'supplier1')
            supplier2 = create_employee(company, 'supplier2')
            supplier2.party.active = False
            supplier2.party.addresses[0].active = False
            supplier2.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue, "Servicios")
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products
            template, product = create_product('Product', account_category)
            template.producible = True
            template.purchasable = True
            template.save()
            template2, component1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template2.purchasable = True
            template2.producible = True
            template2.save()
            request, = PurchaseRequest.create([{
                'product': product.id,
                'quantity': 1,
                'uom': unit,
                'warehouse': warehouse.id,
                'party': supplier1.party.id,
                'company': company.id,
            }])
            # Test create quotation and create purchase, check that line with
            # the cheapest total_unit_price (unit_price * discount) of the
            # quotation is dragged to purchase
            session_id, _, _ = WizardCreateQuotation.create()
            wizard_create_quotation = WizardCreateQuotation(session_id)
            wizard_create_quotation.records = [request]
            wizard_create_quotation.record = request
            wizard_create_quotation.model = PurchaseRequest
            wizard_create_quotation.transition_start()
            wizard_create_quotation.ask_suppliers.suppliers = [
                request.party, supplier2.party]
            wizard_create_quotation.transition_create_quotations()
            self.assertEqual(len(request.quotation_lines), 2)
            quotation = request.quotation_lines[0].quotation
            quotation2 = request.quotation_lines[1].quotation
            self.assertTrue(quotation)
            self.assertTrue(quotation2)
            quotation.lines[0].unit_price = Decimal('1.77')
            quotation2.lines[0].unit_price = Decimal('10.00')
            quotation2.lines[0].discount = Decimal('0.9')
            lines_to_save = quotation.lines + quotation2.lines
            PurchaseRequestQuotationLine.save(lines_to_save)
            self.assertEqual(quotation2.lines[0].total_unit_price,
                             Decimal('1.00'))
            PurchaseRequestQuotation.send([quotation, quotation2])
            PurchaseRequestQuotation.receive([quotation, quotation2])

            # Execute create purchase wizard
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request]
            wizard_create_purchase.model = PurchaseRequest
            wizard_create_purchase.transition_start()
            purchase1 = request.purchase
            purchase_line1, = purchase1.lines
            self.assertEqual(request.state, 'purchased')
            # check that the purchase line is set with the price of the
            # second quotation cause with discount is cheaper
            self.assertEqual(
                purchase_line1.gross_unit_price,
                quotation2.lines[0].unit_price)
            # Test create new request with another product, create quotation
            # and add to existing purchase check that the line with the
            # unit_price of the quotation is dragged to purchase
            request2, = PurchaseRequest.create([{
                'product': component1.id,
                'quantity': 5,
                'uom': unit,
                'warehouse': warehouse.id,
                'party': supplier1.party.id,
                'company': company.id,
            }])
            session_id, _, _ = WizardCreateQuotation.create()
            wizard_create_quotation = WizardCreateQuotation(session_id)
            wizard_create_quotation.records = [request2]
            wizard_create_quotation.record = request2
            wizard_create_quotation.model = PurchaseRequest
            wizard_create_quotation.transition_start()
            wizard_create_quotation.ask_suppliers.suppliers = [
                request2.party]
            wizard_create_quotation.transition_create_quotations()
            self.assertEqual(len(request2.quotation_lines), 1)
            quotation = request2.quotation_lines[0].quotation
            self.assertTrue(quotation)
            quotation.lines[0].unit_price = Decimal('5')
            PurchaseRequestQuotationLine.save(quotation.lines)
            PurchaseRequestQuotation.send([quotation])
            PurchaseRequestQuotation.receive([quotation])
            
            session_id, _, _ = WizardAddToPurchase.create()
            wizard_add_to_purchase = WizardAddToPurchase(session_id)
            wizard_add_to_purchase.records = [request2]
            wizard_add_to_purchase.transition_start()
            wizard_add_to_purchase.ask_purchase.party = request2.party
            wizard_add_to_purchase.ask_purchase.purchase = purchase1
            wizard_add_to_purchase._execute('create_')
            purchase_lines = purchase1.lines
            self.assertEqual(len(purchase_lines), 2)
            self.assertEqual(purchase_lines[1].gross_unit_price, Decimal('5'))

    @with_transaction()
    def test_one_step_do_production(self):
        "Test One Step Do Production wizard"
        pool = Pool()
        Production = pool.get('production')
        Move = pool.get('stock.move')
        Location = pool.get('stock.location')
        ProductUom = pool.get('product.uom')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        Warning = pool.get('res.user.warning')
        WizardProductionOneStepDo = pool.get(
            'production.one_step_do', type='wizard')
        company = create_company()
        with set_company(company):
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create product
            template, product = create_product(
                'Product', account_category_goods)
            template.producible = True
            template.save()
            production1, = Production.create([{
                'company': company,
                'number': 'PROD-001',
                'planned_date': datetime.date(2023, 4, 30),
                'planned_start_date': datetime.date(2023, 4, 30),
                'state': 'waiting',
            }])
            production2, = Production.create([{
                'company': company,
                'number': 'PROD-002',
                'planned_date': datetime.date(2023, 4, 30),
                'planned_start_date': datetime.date(2023, 4, 30),
                'state': 'assigned',
            }])
            production3, = Production.create([{
                'company': company,
                'number': 'PROD-003',
                'planned_date': datetime.date(2023, 4, 30),
                'planned_start_date': datetime.date(2023, 4, 30),
                'state': 'running',
            }])
            session_id, _, _ = WizardProductionOneStepDo.create()
            wizard_production_one_step_do = WizardProductionOneStepDo(session_id)
            records = Production.search(
                [('state', 'in', ['waiting', 'running', 'assigned'])])
            wizard_production_one_step_do.records = records
            wizard_production_one_step_do.start.productions_number = (
                    production1.number + ', ' + production2.number
                    + ', ' + production3.number)
            input = Move()
            input.quantity = 0
            input.from_location = storage_location
            input.to_location = production_loc
            input.product = product
            input.uom = unit
            input.effective_date = (datetime.date.today() +
                                    datetime.timedelta(days=1))
            input.save()
            production1.inputs = [input]
            production1.save()
            # Assert a MoveFutureWarning is raised if there are moves with 
            # future effective_date
            try:
                wizard_production_one_step_do.transition_one_step_do()
                self.fail("MoveFutureWarning Not Raised")
            except MoveFutureWarning as warning:
                _, (key, *_) = warning.args
            
            # Save the warning to prevent it from being raised again
            Warning(user=Transaction().user,name=key, always=True).save()
            wizard_production_one_step_do.transition_one_step_do()

            # test that productions have been assigned, run and done
            production1 = Production(production1.id)
            production2 = Production(production2.id)
            production3 = Production(production3.id)
            self.assertEqual(production1.state, 'done')
            self.assertEqual(production2.state, 'done')
            self.assertEqual(production3.state, 'done')

    @with_transaction()
    def test_create_internal_shipments_with_null_values(self):
        """Test Production.create_internal_shipments with null values"""
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        company = create_company()
        with set_company(company):
            to_location, = Location.search([('code', '=', 'STO')])
            # Call function without productions and location
            result = Production.create_internal_shipments([], None, True)
            # Assert the result is an empty list and not NoneType
            self.assertIsNotNone(result)
            self.assertEqual(result, [])

            # Call function without productions but location
            result = Production.create_internal_shipments([], to_location, True)
             # Assert the result is an empty list and not NoneType
            self.assertIsNotNone(result)
            self.assertEqual(result, [])

    @with_transaction()
    def test_link_production_supply_shipments(self):
        "Test Production Supply Shipments"
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        ShipmentInternal = pool.get('stock.shipment.internal')
        ProductionInShipment = pool.get('production-stock.shipment.internal')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardProductionSplit = pool.get('production.split', type='wizard')
        WizardShipmentSplit = pool.get('stock.shipment.split', type='wizard')
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_view = Location(name='View 2', type='view', code='VIW')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_view
            storage_location.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(
                tax, expense, revenue)
            template_1, product_1 = create_product(
                'Product 1', account_category_goods, consumable=False)
            template_1.purchasable = True
            template_1.producible = True
            template_1.save()
            template_2, product_2 = create_product(
                'Product 2', account_category_goods, consumable=True)
            template_2.purchasable = True
            template_2.producible = True
            template_2.save()
            bom = _create_bom(product_1, product_2, unit)
            bom.save()
            
            # Create a production
            production1 = create_production(
                3, product_1, unit, warehouse, production_loc, company, bom)
            # Create internal shipments from the productions
            shipments = Production.create_internal_shipments(
                [production1], storage_location, True)
            # Assert shipment values are correct
            self.assertEqual(len(shipments), 1)
            self.assertEqual(len(shipments[0].moves), 1)
            
            # Execute production split wizard
            session_id, _, _ = WizardProductionSplit.create()
            wizard_production_split = WizardProductionSplit(session_id)
            wizard_production_split.records = [production1]
            wizard_production_split.record = production1
            wizard_production_split.start.quantity = 1
            wizard_production_split.start.create_serial_numbers = True
            wizard_production_split.start.count = 1
            wizard_production_split.start.uom = production1.uom
            with Transaction().set_context(active_id=production1.id):
                wizard_production_split.transition_split()
            productions = Production.search([])
            self.assertEqual(len(productions), 2)
            self.assertEqual(productions[0].quantity, 2)
            self.assertEqual(productions[1].quantity, 1)
            # test if when the production is split we keep
            # the link between shipments and productions
            self.assertTrue(productions[0].supply_shipments)
            self.assertTrue(productions[1].supply_shipments)
            productions_supply_shipments = ProductionInShipment.search([])
            self.assertEqual(len(productions_supply_shipments), 2)
            internal_shipments = ShipmentInternal.search([])
            self.assertTrue(internal_shipments)
            self.assertEqual(len(internal_shipments[0].productions), 2)
            # Test the shipment move is linked only with the first
            # production, so we can use it in the picking report
            origin_productions1 = set(move.origin.production_input for move in
                                      internal_shipments[0].moves
                                      if move.origin)
            self.assertEqual(len(origin_productions1), 1)
            # Test that if we create a shipment from 2 productions with the
            # same products as inputs, does not group them in the shipment
            # 2 productions 1 input each same products will result in:
            # 2 moves in shipment with origin to different productions
            production3 = create_production(
                1, product_1, unit, warehouse, production_loc, company, bom)
            production4 = create_production(
                1, product_1, unit, warehouse, production_loc, company, bom)
            productions = Production.search([])
            self.assertEqual(len(productions), 4)
            shipments = Production.create_internal_shipments(
                [production3, production4], storage_location, True)
            self.assertEqual(len(shipments), 1)
            self.assertTrue(production3.supply_shipments)
            self.assertTrue(production4.supply_shipments)
            self.assertEqual(len(shipments[0].moves), 2)
            self.assertEqual(len(shipments[0].productions), 2)
            # Test each move is linked with his corresponding production
            origin_productions2 = set(move.origin.production_input for move in
                                      shipments[0].moves if move.origin)
            self.assertEqual(len(origin_productions2), 2)
            # If a production with supply_shipments is duplicated,
            # test that don't link the supply_shipments of the original one
            production5, = Production.copy([production4])
            self.assertFalse(production5.supply_shipments)
            # Test when splitting shipment in draft state the link with the
            # copy keep
            self.assertEqual(shipments[0].state, 'draft')
            
            # Execute shipment split wizard
            session_id, _, _ = WizardShipmentSplit.create()
            wizard_shipment_split = WizardShipmentSplit(session_id)
            wizard_shipment_split.records = [shipments[0]]
            wizard_shipment_split.record = shipments[0]
            wizard_shipment_split.model = ShipmentInternal
            wizard_shipment_split.start.moves = [shipments[0].moves[0].id]
            wizard_shipment_split.start.domain_moves = [
                shipments[0].moves[0].id]
            original_move_to_split = shipments[0].moves[0]
            wizard_shipment_split.transition_split()
            self.assertEqual(len(original_move_to_split.shipment.moves), 1)
            self.assertEqual(len(
                original_move_to_split.shipment.productions), 2)
            # Test when splitting shipment in waiting the link with the
            # copy keep
            ShipmentInternal.wait([shipments[0]])
            self.assertEqual(shipments[0].state, 'waiting')
            session_id, _, _ = WizardShipmentSplit.create()
            wizard_shipment_split = WizardShipmentSplit(session_id)
            wizard_shipment_split.records = [shipments[0]]
            wizard_shipment_split.record = shipments[0]
            wizard_shipment_split.model = ShipmentInternal
            wizard_shipment_split.start.moves = [shipments[0].moves[0].id]
            wizard_shipment_split.start.domain_moves = [
                shipments[0].moves[0].id]
            original_move_to_split = shipments[0].moves[0]
            wizard_shipment_split.transition_split()
            self.assertEqual(len(original_move_to_split.shipment.moves), 1)
            self.assertEqual(len(shipments[0].moves), 0)
            self.assertEqual(len(
                original_move_to_split.shipment.productions), 2)

    @with_transaction()
    def test_modify_production_planned_start_date(self):
        "Test Modify Production Planned Date"
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        ShipmentInternal = pool.get('stock.shipment.internal')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_sto2 = Location(name='STO 2', type='storage', code='STO2')
            # It modifies at the same time the provisioning_location
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_sto2
            storage_location.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(
                tax, expense, revenue)
            template_1, product_1 = create_product(
                'Product 1', account_category_goods, consumable=False)
            template_1.purchasable = True
            template_1.producible = True
            template_1.save()
            template_2, product_2 = create_product(
                'Product 2', account_category_goods, consumable=True)
            template_2.purchasable = True
            template_2.producible = True
            template_2.save()
            bom = _create_bom(product_1, product_2, unit)
            bom.save()
            production1 = create_production(
                3, product_1, unit, warehouse, production_loc, company, bom)
            shipments_draft = Production.create_internal_shipments(
                [production1], storage_location, True)
            shipments_assigned = Production.create_internal_shipments(
                [production1], storage_location, True)
            shipments_cancelled = Production.create_internal_shipments(
                [production1], storage_location, True)
            shipments = ShipmentInternal.search([])
            self.assertEqual(len(shipments), 3)
            self.assertEqual(len([m for m in shipments]), 3)
            ShipmentInternal.wait([shipments_assigned[0]])
            ShipmentInternal.assign([shipments_assigned[0]])
            ShipmentInternal.cancel([shipments_cancelled[0]])
            # Before the change
            self.assertEqual(production1.planned_start_date, today)
            self.assertEqual(shipments_draft[0].planned_date, today)
            self.assertEqual(shipments_assigned[0].planned_date, today)
            self.assertEqual(shipments_cancelled[0].planned_date, today)
            production1.planned_start_date = datetime.date(2023, 1, 1)
            production1.save()
            # AINs after the change
            self.assertEqual(production1.planned_start_date, 
                             datetime.date(2023, 1, 1))
            self.assertEqual(shipments_draft[0].planned_date, 
                             datetime.date(2023, 1, 1))
            self.assertEqual(shipments_assigned[0].planned_date, 
                             datetime.date(2023, 1, 1))
            self.assertEqual(shipments_cancelled[0].planned_date, today)
            # Moves of AINs after the change
            self.assertEqual(shipments_draft[0].moves[0].planned_date, 
                             datetime.date(2023, 1, 1))
            self.assertEqual(shipments_assigned[0].moves[0].planned_date, 
                             datetime.date(2023, 1, 1))
            self.assertEqual(shipments_cancelled[0].moves[0].planned_date, 
                             today)

    @with_transaction()
    def test_modify_to_location_internal_shipment_and_locate_products(self):
        "Test Modify To Location Internal Shipment And Locate Products"
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        ShipmentInternal = pool.get('stock.shipment.internal')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardShipmentModifyToLocation = pool.get(
            'stock.shipment.internal.modify_to_location', type='wizard')
        WizardLocateProducts = pool.get(
            'stock.shipment.internal.locate_products', type='wizard')
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_view = Location(name='View 2', type='view', code='VIW')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_view
            storage_location.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(tax, expense, revenue)
            template_1, product_1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template_1.purchasable = True
            template_1.producible = True
            template_1.save()
            template_2, product_2 = create_product(
                'Component 2', account_category_goods, consumable=True)
            template_2.purchasable = True
            template_2.producible = True
            template_2.save()
            bom = _create_bom(product_1, product_2, unit)
            bom.save()
            production1 = create_production(
                3, product_1, unit, warehouse, production_loc, company, bom)
            shipment, = Production.create_internal_shipments(
                [production1], storage_location, True)
            self.assertEqual(len(shipment.moves), 1)
            move = shipment.moves[0]
            move.unit_price = Decimal('1.00')
            move.save()
            # Define a new location to modify the shipment
            new_storage_location, = Location.copy(
                [storage_location], default={'code': 'STO2'})
            
            # Execute Shipment Modify to Location Wizard
            session_id, _, _ = WizardShipmentModifyToLocation.create()
            wizard_modify_to_location = WizardShipmentModifyToLocation(session_id)
            wizard_modify_to_location.records = [shipment]
            wizard_modify_to_location.start.to_location = new_storage_location

            # Assert locations are correctly updated
            self.assertEqual(shipment.to_location, storage_location)
            self.assertEqual(move.to_location, storage_location)
            wizard_modify_to_location.transition_modify()
            self.assertEqual(shipment.to_location, new_storage_location)
            self.assertEqual(move.to_location, new_storage_location)
            ShipmentInternal.wait([shipment])

            # Assert an error is raised if the shipment is not in draft or 
            # request state
            session_id, _, _ = WizardShipmentModifyToLocation.create()
            wizard_modify_to_location = WizardShipmentModifyToLocation(session_id)
            wizard_modify_to_location.records = [shipment]
            wizard_modify_to_location.start.to_location = storage_location
            self.assertEqual(shipment.to_location, new_storage_location)
            self.assertRaises(AccessError, 
                              wizard_modify_to_location.transition_modify)
            
            # Continue shipment's workflow 
            ShipmentInternal.assign_try([shipment])
            ShipmentInternal.ship([shipment])

            # Execute Locate Products Wizard
            session_id, _, _ = WizardLocateProducts.create()
            wizard_locate_products = WizardLocateProducts(session_id)
            wizard_locate_products.record = shipment
            wizard_locate_products.start.to_location = storage_location
            wizard_locate_products.start.moves = [move]
            wizard_locate_products.start.shipment_to_location = shipment.to_location
            wizard_locate_products._execute('locate')

            # Assert only the move location has been modified
            self.assertEqual(move.to_location, storage_location)
            self.assertNotEqual(shipment.to_location, storage_location)

    @with_transaction()
    def test_fields_dragging_to_purchase_request(self):
        "Test Fields dragging to Purchase Request"
        pool = Pool()
        ApprovalGroup = pool.get('approval.group')
        ApprovalRequest = pool.get('approval.request')
        Production = pool.get('production')
        Location = pool.get('stock.location')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        Tax = pool.get('account.tax')
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        Product = pool.get('product.product')
        Purchase = pool.get('purchase.purchase')
        Employee = pool.get('company.employee')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        company = create_company()
        with set_company(company):
            # Create party
            party = Party(name='Party')
            # Create employee
            employee = Employee(party=party)
            employee.save()
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_sto2 = Location(name='STO 2', type='storage', code='STO2')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_sto2
            storage_location.save()
            # Supplier locations
            sup_storage = Location(
                name='Sup Storage', type='storage', code='STO3')
            sup_storage.save()
            sup_input = Location(
                name='Sup Input', type='storage', code='IN2')
            sup_input.save()
            sup_output = Location(
                name='Sup Output', type='storage', code='OUT2')
            sup_output.save()
            sup_lost_found = Location(
                name='Sup Lost Found', type='lost_found', code='LF2')
            sup_lost_found.save()
            sup_production = Location(
                name='Sup Production', type='production', code='PROD2')
            sup_production.save()
            sup_warehouse = Location()
            sup_warehouse.type = 'warehouse'
            sup_warehouse.name = 'Supplier Warehouse'
            sup_warehouse.code = 'SUPWH'
            sup_warehouse.storage_location = sup_storage
            sup_warehouse.input_location = sup_input
            sup_warehouse.output_location = sup_output
            sup_warehouse.lost_found_location = sup_lost_found
            sup_warehouse.production_location = sup_production
            sup_warehouse.save()
            # Create supplier
            supplier, = Party.create([{
                'name': 'Supplier',
                'addresses': [('create', [{}])],
                'production_warehouse': sup_warehouse,
            }])
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue, "Servicios")
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products and bom
            template, product = create_product('Product', account_category)
            template.purchasable = True
            template.producible = True
            template.save()
            template2, component1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template2.purchasable = True
            template2.producible = True
            template2.save()
            template3, component2 = create_product(
                'Component 2', account_category_goods, consumable=True)
            template3.purchasable = True
            template3.producible = True
            template3.save()
            bom = _create_bom(component1, component2, unit)
            # Check dragging fields from production to request
            production1 = create_production(
                3, component1, unit, warehouse, production_loc, company, bom)
            production1.subcontract_product = product
            production1.save()

            # The requester from the purchase request is taken from the context
            with Transaction().set_context(employee=employee):
                Production.create_purchase_request([production1])
            self.assertIsNotNone(production1.purchase_request)
            self.assertEqual(production1.planned_start_date, 
                             production1.purchase_request.manual_delivery_date)
            self.assertEqual(production1.purchase_request.requester, 
                             employee)
            self.assertEqual(Product.get_purchase_product_unit_price(production1.purchase_request),
                             production1.purchase_request.unit_price)

            request = production1.purchase_request
            request.party = supplier
            request.save()
            # Execute create purchase wizard from production purchase request
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request]
            wizard_create_purchase.model = PurchaseRequest
            wizard_create_purchase.transition_start()

            # Assert planned dates are correctly dragged to purchase line
            self.assertIsNotNone(request.purchase_line.delivery_date)
            self.assertIsNotNone(request.purchase_line.delivery_date_store)

            # Save purchase
            purchase = request.purchase

            # Modify purchase line delivery date
            purchase.lines[0].delivery_date += datetime.timedelta(days=1)
            purchase.lines[0].save()

            # Create approval group
            approval_group, = ApprovalGroup.create([{
                'name': 'Approve Request Group'
            }])
            approval_group.users = tuple([Transaction().user])
            approval_group.save()

            # Purchase workflow
            purchase.approval_group = approval_group.id
            purchase.invoice_address = supplier.addresses[0]
            purchase.save()
            Purchase.quote([purchase])
            self.assertEqual(purchase.state, 'quotation')
            ApprovalRequest.approve(purchase.approval_requests)
            Purchase.confirm([purchase])
            self.assertEqual(purchase.state, 'confirmed')
            Purchase.process([purchase])
            self.assertEqual(purchase.state, 'processing')

            # Assert a move related to purchase line is generated, and
            # move/line planned dates are saved & dragged
            line = purchase.lines[0]
            move = purchase.lines[0].moves[0]
            self.assertIsNotNone(move)
            self.assertIsNotNone(move.planned_date)
            self.assertIsNotNone(line.delivery_date)
            self.assertEqual(line.delivery_date, move.planned_date)
            self.assertIsNotNone(line.delivery_date_store)
            self.assertEqual(line.delivery_date_store, move.planned_date)

            # Check dragging fields from requisition to request
            requisition = PurchaseRequisition(
                supply_date=today + datetime.timedelta(days=30),
                warehouse=warehouse, employee=employee)
            requisition.save()
            requisition_line = PurchaseRequisitionLine(
                product=component1, quantity=Decimal('1.00'),
                requisition=requisition, supplier=party, unit=unit)
            requisition_line.save()

            # It passes through all the states until processing one
            PurchaseRequisition.wait([requisition])
            self.assertEqual(requisition.state, 'processing')
            purchase_request, = PurchaseRequest.search(
                ['origin', '=', 'purchase.requisition.line,' + str(requisition_line.id)])
            self.assertEqual(today + datetime.timedelta(days=30), 
                             purchase_request.manual_delivery_date)
            self.assertEqual(requisition.employee, purchase_request.requester)
            self.assertEqual((requisition.comment + "\n" + requisition_line.comment
                              if requisition.comment else requisition_line.comment), purchase_request.comment)

    @with_transaction()
    def test_purchase_amendment(self):
        "Test purchase amendment"
        pool = Pool()
        ApprovalGroup = pool.get('approval.group')
        ApprovalRequest = pool.get('approval.request')
        Party = pool.get('party.party')
        Purchase = pool.get('purchase.purchase')
        PurchaseAmendment = pool.get('purchase.amendment')
        PurchaseAmendmentLine = pool.get('purchase.amendment.line')
        PurchaseLine = pool.get('purchase.line')
        ProductUom = pool.get('product.uom')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        Warehouse = pool.get('stock.location')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue)
            template, product = create_product('Product', account_category)
            template.purchasable = True
            template.save()
            # Create supplier
            supplier_1, = Party.create([{
                'name': 'Supplier 1',
                'addresses': [('create', [{}])],
            }])
            # Create approval group
            approval_group, = ApprovalGroup.create([{
                'name': 'Approve Request Group',
            }])
            approval_group.users = tuple([Transaction().user])
            approval_group.save()
            # Create purchase
            warehouse, = Warehouse.search([('code', '=', 'WH')])
            purchase_1, = Purchase.create([{
                'company': company.id,
                'party': supplier_1.id,
                'invoice_address': supplier_1.addresses[0],
                'currency': company.currency.id,
                'warehouse': warehouse.id,
                'approval_group': approval_group.id,
                'invoice_method': 'manual',
                'invoice_state': 'none',
                'shipment_state': 'none',
                'state': 'draft',
            }])
            purchase_line = PurchaseLine()
            purchase_line.purchase = purchase_1.id
            purchase_line.product = product.id
            purchase_line.quantity = Decimal('1')
            purchase_line.gross_unit_price = Decimal('10')
            purchase_line.discount = Decimal('50')
            purchase_line.unit = unit.id
            purchase_line.update_prices()
            purchase_line.save()
            # Checks if the unit price is correctly calculated
            self.assertTrue(purchase_line.unit_price ==
                            purchase_line.gross_unit_price -
                            (purchase_line.gross_unit_price *
                            purchase_line.discount))
            Purchase.quote([purchase_1])
            self.assertEqual(purchase_1.state, 'quotation')
            ApprovalRequest.approve(purchase_1.approval_requests)
            Purchase.confirm([purchase_1])
            self.assertEqual(purchase_1.state, 'confirmed')
            Purchase.process([purchase_1])
            self.assertEqual(purchase_1.state, 'processing')
            # Create purchase amendment
            amendment_1, = PurchaseAmendment.create([{
                'purchase': purchase_1.id,
                'date': datetime.date(2024, 1, 10),
            }])
            amendment_line = PurchaseAmendmentLine()
            amendment_line.amendment = amendment_1.id
            amendment_line.action = 'line'
            amendment_line.line = purchase_line.id
            amendment_line.on_change_line()
            amendment_line.quantity = Decimal('2')
            amendment_line.gross_unit_price = Decimal('50')
            amendment_line.discount = Decimal('10')
            amendment_line.update_unit_price()
            # Checks if the unit price is correctly calculated
            self.assertTrue(amendment_line.unit_price ==
                            amendment_line.gross_unit_price -
                            (amendment_line.gross_unit_price *
                             amendment_line.discount))
            amendment_line.delivery_date = datetime.date(2024, 1, 25)
            amendment_line.save()
            PurchaseAmendment.validate_amendment([amendment_1])
            # Checks if the amendment has been validated
            self.assertEqual(amendment_1.state, 'validated')
            # Checks if purchase has been correctly modified
            self.assertTrue(purchase_line.gross_unit_price ==
                            amendment_line. gross_unit_price)
            self.assertTrue(purchase_line.discount == amendment_line.discount)
            self.assertTrue(purchase_line.unit_price ==
                            amendment_line.unit_price)
            self.assertEqual(purchase_line.delivery_date_store,
                             amendment_line.delivery_date)

    @with_transaction()
    def test_reference_internal_shipments(self):
        "Test Reference Internal Shipment"
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        Location = pool.get('stock.location')
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            lost_found, = Location.search([('type', '=', 'lost_found')])
            # warehouse of the company
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            # warehouse 2 of the company
            storage_location2 = Location(
                name='STO 2', code='STO2', type='storage')
            storage_location2.save()
            production_loc2 = Location(
                name='PROD 2', code='PROD2', type='production')
            warehouse2 = Location(
                name='WAREHOUSE 2', code='WH2', type='warehouse',
                storage_location=storage_location2,
                production_location=production_loc2,
                input_location=storage_location2,
                output_location=storage_location2)
            warehouse2.save()
            # warehouse 3 NOT of the company
            storage_location3 = Location(
                name='STO 3', code='STO3', type='storage')
            storage_location3.save()
            production_loc3 = Location(
                name='PROD 3', code='PROD3', type='production')
            warehouse3 = Location(
                name='WAREHOUSE 3', code='WH3', type='warehouse',
                storage_location=storage_location3,
                production_location=production_loc3,
                input_location=storage_location3,
                output_location=storage_location3)
            warehouse3.save()
            storage_location3.parent = warehouse3.id
            storage_location3.save()
            # Define warehouses of the company
            company.warehouses = tuple([warehouse, warehouse2])
            company.save()

            # 1- If to and from warehouses are of the same company
            internal_shipment = ShipmentInternal()
            internal_shipment.from_location = warehouse.storage_location
            internal_shipment.to_location = warehouse2.storage_location
            internal_shipment.save()
            self.assertEqual(internal_shipment.reference, None)
            self.assertEqual(internal_shipment.subtype, 'intrawarehouse')
            self.assertEqual(internal_shipment.state, 'draft')
            ShipmentInternal.wait([internal_shipment])
            self.assertEqual(internal_shipment.state, 'waiting')
            ShipmentInternal.assign_try([internal_shipment])
            self.assertEqual(internal_shipment.state, 'assigned')
            ShipmentInternal.done([internal_shipment])
            self.assertEqual(internal_shipment.state, 'done')

            # 2- If from_location is not from the company but to_location is
            # reference field will be required=True
            internal_shipment = ShipmentInternal()
            internal_shipment.from_location = warehouse3.storage_location
            internal_shipment.to_location = warehouse.storage_location
            internal_shipment.save()
            self.assertEqual(internal_shipment.reference, None)
            self.assertEqual(internal_shipment.subtype, 'in')
            self.assertEqual(internal_shipment.state, 'draft')
            ShipmentInternal.wait([internal_shipment])
            self.assertEqual(internal_shipment.state, 'waiting')
            with self.assertRaises(RequiredValidationError):
                ShipmentInternal.assign_try([internal_shipment])
            internal_shipment.reference = 'REF0001'
            internal_shipment.save()
            ShipmentInternal.assign_try([internal_shipment])
            self.assertEqual(internal_shipment.state, 'assigned')
            ShipmentInternal.done([internal_shipment])
            self.assertEqual(internal_shipment.state, 'done')

            # 3- If to_location is not of the company but from_location is
            internal_shipment = ShipmentInternal()
            internal_shipment.from_location = warehouse.storage_location
            internal_shipment.to_location = warehouse3.storage_location
            internal_shipment.save()
            self.assertEqual(internal_shipment.reference, None)
            self.assertEqual(internal_shipment.subtype, 'out')
            self.assertEqual(internal_shipment.state, 'draft')
            ShipmentInternal.wait([internal_shipment])
            self.assertEqual(internal_shipment.state, 'waiting')
            ShipmentInternal.assign_try([internal_shipment])
            self.assertEqual(internal_shipment.state, 'assigned')
            ShipmentInternal.done([internal_shipment])
            self.assertEqual(internal_shipment.state, 'done')

            # 4- If to_location or from_location are of type lost_found
            internal_shipment = ShipmentInternal()
            internal_shipment.from_location = warehouse3.storage_location
            internal_shipment.to_location = lost_found
            internal_shipment.save()
            self.assertEqual(internal_shipment.reference, None)
            self.assertEqual(internal_shipment.subtype, 'intrawarehouse')
            self.assertEqual(internal_shipment.state, 'draft')
            ShipmentInternal.wait([internal_shipment])
            self.assertEqual(internal_shipment.state, 'waiting')
            ShipmentInternal.assign_try([internal_shipment])
            self.assertEqual(internal_shipment.state, 'assigned')
            ShipmentInternal.done([internal_shipment])
            self.assertEqual(internal_shipment.state, 'done')

    @with_transaction()
    def test_product_price_list_amount_wizard(self):
        "Test Product Price List Amount Wizard"
        pool = Pool()
        ProductPriceList = pool.get('product.price_list')
        Tax = pool.get('account.tax')
        WizardPriceListAmount = pool.get(
            'product.price_list_amount', type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create product
            template, product = create_product('Product 1', account_category)
            # Create product price list
            price_list, = ProductPriceList.create([{
                'name': 'TestingPriceList',
                'unit': 'product_default',
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': Decimal('1'),
                    'formula': '100.00',
                }])],
            }])
            # Create product price list
            price_list_2, = ProductPriceList.create([{
                'name': 'TestingPriceList 2',
                'unit': 'product_default',
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': Decimal('1'),
                    'formula': 'unit_price',
                }])],
            }])
            session_id, _, _ = WizardPriceListAmount.create()
            wizard_price_list_amount = WizardPriceListAmount(session_id)
            wizard_price_list_amount.records = [price_list]
            wizard_price_list_amount.transition_start()
            wizard_price_list_amount.ask_price_list.price_lists_lines = [
                line.id for line in price_list.lines]
            wizard_price_list_amount.ask_price_list.percentage = Decimal(
                '0.10')
            wizard_price_list_amount._execute('apply_')
            self.assertEqual(Decimal(price_list.lines[0].formula),
                Decimal('110.00'))
            wizard_price_list_amount.records = [price_list_2]
            wizard_price_list_amount.transition_start()
            wizard_price_list_amount.ask_price_list.price_lists_lines = [
                line.id for line in price_list_2.lines]
            wizard_price_list_amount.ask_price_list.percentage = Decimal(
                '0.10')
            self.assertRaises(UserError, wizard_price_list_amount.do_apply_, 
                              'apply_')

    @with_transaction()
    def test_add_to_purchase_quotation_wizard(self):
        "Test add to purchase quotation wizard"
        pool = Pool()
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        PurchaseQuotation = pool.get('purchase.request.quotation')
        PurchaseRequest = pool.get('purchase.request')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        Location = pool.get('stock.location')
        WizardAddToPurchaseQuotation = pool.get(
            'purchase.request.add_to_purchase_quotation', type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create parties
            party_1, = Party.create([{
                            'name': 'party_1',
                            'addresses': [('create', [{}])],
                }])
            party_2, = Party.create([{
                    'name': 'party_2',
                    'addresses': [('create', [{}])],
                }])
            party_3, = Party.create([{
                    'name': 'supplier_3',
                    'addresses': [('create', [{}])],
                }])
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create products
            template_1, product_1 = create_product('Product_1', account_category)
            template_1.producible = True
            template_1.purchasable = True
            template_1.save()
            template_2, product_2 = create_product('Product_2', account_category)
            template_2.producible = True
            template_2.purchasable = True
            template_2.save()
            # Create warehouse
            warehouse, = Location.search([('code', '=', 'WH')])
            # Create purchase requests
            request_1, = PurchaseRequest.create([{
                'product': product_1.id,
                'quantity': Decimal('1'),
                'uom': unit.id,
                'warehouse': warehouse.id,
                'party': party_1.id,
                'company': company.id,
            }])
            request_2, = PurchaseRequest.create([{
                'product': product_2.id,
                'quantity': Decimal('1'),
                'uom': unit.id,
                'warehouse': warehouse.id,
                'party': party_2.id,
                'company': company.id,
            }])
            request_3, = PurchaseRequest.create([{
                'product': product_2.id,
                'quantity': Decimal('1'),
                'uom': unit.id,
                'warehouse': warehouse.id,
                'party': party_2.id,
                'company': company.id,
                'state': 'purchased'
            }])
            # Create quotation
            quotation, = PurchaseQuotation.create([{
                'supplier': party_3.id,
                'company': company.id,
                'state': 'draft',
            }])
            # Execute Create quotation wizard
            session_id, _, _ = WizardAddToPurchaseQuotation.create()
            wizard_add_to_quotation = WizardAddToPurchaseQuotation(session_id)
            wizard_add_to_quotation.records = ([request_1, request_2])
            wizard_add_to_quotation.transition_start()
            wizard_add_to_quotation.ask_quotation.quotation = quotation.id
            wizard_add_to_quotation._execute('create_')
            qline = quotation.lines[0]

            # Assert all values have been set correctly
            self.assertEqual(qline.request, request_1)
            self.assertEqual(quotation.lines[1].request, request_2)
            self.assertEqual(request_1.state, 'quotation')
            self.assertEqual(request_2.state, 'quotation')
            self.assertEqual(qline.product, request_1.product)
            self.assertEqual(qline.quantity, request_1.quantity)
            self.assertEqual(qline.currency, request_1.currency)
            self.assertEqual(qline.unit, request_1.uom)
            self.assertEqual(qline.description, request_1.description)

            # Assert executting the wizard on a request with a state != 'draft'
            # raise an error
            wizard_add_to_quotation.records = ([request_3])
            self.assertRaises(UserError, 
                              wizard_add_to_quotation.transition_start)

    @with_transaction()
    def test_supplier_ref_required_in_supplier_shipment_to_done(self):
        "Test Supplier Ref required in Supplier Shipment to done"
        pool = Pool()
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        ShipmentIn = pool.get('stock.shipment.in')
        StockLot = pool.get('stock.lot')
        SequenceType = pool.get('ir.sequence.type')
        Sequence = pool.get('ir.sequence')
        Party = pool.get('party.party')
        Move = pool.get('stock.move')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create Supplier
            party = Party(name='Supplier 1')
            party.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            input_location, = Location.search([('code', '=', 'IN')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.input_location = input_location
            warehouse.save()
            location_view = Location(name='View 2', type='view', code='VIW')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_view
            storage_location.save()
            # Create sequence for product
            sequence_type, = SequenceType.search([('name', '=', "Stock Lot"), ], limit=1)
            sequence, = Sequence.create([{
                'name': 'Test Lots',
                'sequence_type': sequence_type.id,
                'prefix': 'EM',
                'suffix': '',
                'type': 'incremental',
            }])
            # Create product
            template, product = create_product('Product', account_category)
            template.purchasable = True
            template.producible = True
            template.serial_number = True
            template.lot_sequence = sequence
            template.save()
            # Create lot
            lot = StockLot()
            lot.number = 'LOT001'
            lot.product = product
            lot.save()
            # Create supplier shipment
            supplier_shipment = ShipmentIn()
            supplier_shipment.reference = 'REF0001'
            supplier_shipment.warehouse = warehouse
            supplier_shipment.warehouse_input = warehouse.input_location
            supplier_shipment.warehouse_storage = warehouse.storage_location
            supplier_shipment.supplier = party
            supplier_shipment.save()
            # Create Move
            move = Move()
            move.product = product
            move.quantity = 1
            move.unit_price = Decimal('1.00')
            move.uom = unit
            move.lot = lot
            move.shipment = supplier_shipment
            move.from_location = warehouse.storage_location
            move.to_location = warehouse.input_location
            move.save()
            self.assertEqual(len(supplier_shipment.moves), 1)
            self.assertEqual(len(supplier_shipment.incoming_moves), 1)
            # Case if product IS serial number, Supplier Ref Required Error cause product is serial number
            self.assertRaises(UserWarning, ShipmentIn.receive, [supplier_shipment])
            supplier_shipment.state = 'received'
            supplier_shipment.save()
            with self.assertRaises(UserError):
                ShipmentIn.done([supplier_shipment])
            supplier_shipment.moves[0].lot.supplier_ref = 'SUPPLIER_REF001'
            supplier_shipment.moves[0].lot.save()
            ShipmentIn.done([supplier_shipment])
            self.assertEqual(supplier_shipment.state, 'done')
            # Case if product IS NOT serial number, allow to finalize shipment with no supplier_ref
            template.serial_number = False
            template.save()
            supplier_shipment2, = ShipmentIn.copy([supplier_shipment])
            self.assertRaises(UserWarning, ShipmentIn.receive, [supplier_shipment2])
            supplier_shipment2.state = 'received'
            supplier_shipment2.save()
            ShipmentIn.done([supplier_shipment2])
            self.assertEqual(supplier_shipment2.state, 'done')

    @with_transaction()
    def test_create_purchase_with_quotation_not_received(self):
        "Test Create Purchase wizard with quotation not received"
        pool = Pool()
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequestQuotation = pool.get('purchase.request.quotation')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        Warehouse = pool.get('stock.location')
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        company = create_company()
        currency = company.currency
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            template, product = create_product('Product', account_category)
            template.purchasable = True
            template.save()
            # Create supplier
            supplier_1, = Party.create([{
                'name': 'Supplier 1',
                'addresses': [('create', [{}])],
            }])
            warehouse, = Warehouse.search([('code', '=', 'WH')])
            # Create Purchase Request
            purchase_request, = PurchaseRequest.create([{
                'product': product.id,
                'quantity': 1,
                'uom': unit,
                'warehouse': warehouse.id,
                'party': supplier_1.id,
                'company': company.id,
            }])
            # Create quotation
            request_quotation, = PurchaseRequestQuotation.create([{
                'company': company.id,
                'supplier': supplier_1.id,
                'state': 'draft',
                'lines': [('create', [{
                    'request': purchase_request.id,
                    'quantity': Decimal('1'),
                    'unit': unit.id,
                    'unit_price': Decimal('1'),
                    'currency': currency.id,
                }])],
            }])
            # Execute create purchase wizard
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [purchase_request]
            # Assert a warning is raised when there is purchase quotations 
            # defined and they are not 'received'
            self.assertRaises(UserWarning,
                              wizard_create_purchase.transition_start)
            # Assert the warning is still raised as the purchase quotation is 
            # still not recieved    
            PurchaseRequestQuotation.send([request_quotation])
            self.assertRaises(UserWarning,
                              wizard_create_purchase.transition_start)
            PurchaseRequestQuotation.receive([request_quotation])
            # Executing wizard again to assert UserWarning is not being
            # raised with 'received' state
            wizard_create_purchase.transition_start()
            # Assert the warning is not raised with 'rejected' state
            purchase_request.state = 'rejected'
            wizard_create_purchase.transition_start()
            # Assert the warning is not raised with 'cancelled' state
            purchase_request.state = 'cancelled'
            wizard_create_purchase.transition_start()

    @with_transaction()
    def test_not_error_deleting_quotation_line_without_request(self):
        "Test Not Error Deleting Quotation Line Without Request"
        pool = Pool()
        PurchaseRequestQuotation = pool.get('purchase.request.quotation')
        PurchaseRequestQuotationLine = pool.get(
            'purchase.request.quotation.line')
        Tax = pool.get('account.tax')
        company = create_company()
        with set_company(company):
            # Create some parties
            supplier1 = create_employee(company, 'supplier1')
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products
            template, product = create_product('Product', account_category_goods)
            template.producible = True
            template.purchasable = True
            template.save()
            quotation = PurchaseRequestQuotation(supplier=supplier1.party)
            quotation.save()
            quotation_line = PurchaseRequestQuotationLine(
                product_to_request=product, quantity=Decimal('1.00'),
                description='Product Descr', quotation=quotation)
            quotation_line.save()
            self.assertEqual(len(quotation.lines), 1)
            PurchaseRequestQuotationLine.delete(quotation.lines)
            self.assertEqual(len(quotation.lines), 0)

    @with_transaction()
    def test_add_component_breakdown_text_to_moves(self):
        "Test Add Component Breakdown Text To Moves"
        pool = Pool()
        Production = pool.get('production')
        Carrier = pool.get('carrier')
        Party = pool.get('party.party')
        Location = pool.get('stock.location')
        ShipmentOut = pool.get('stock.shipment.out')
        SequenceType = pool.get('ir.sequence.type')
        Sequence = pool.get('ir.sequence')
        BOMInput = pool.get('production.bom.input')
        Tax = pool.get('account.tax')
        StockLot = pool.get('stock.lot')
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardAddComponentBreakdown = pool.get(
            'stock.shipment.out.add_component_breakdown', type='wizard')
        company = create_company()
        with set_company(company):
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_view = Location(name='View 2', type='view', code='VIW')
            storage_location.provisioning_location_edit = True
            storage_location.provisioning_location_used = location_view
            storage_location.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue, "Servicios")
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create product input of the product_2's bom
            template_1, product_1 = create_product(
                'Product 1', account_category_goods, consumable=True)
            template_1.purchasable = True
            template_1.producible = True
            template_1.save()
            # Create output product of the production that will be created
            template_2, product_2 = create_product(
                'Product 2', account_category_goods, consumable=True)
            template_2.purchasable = True
            template_2.producible = False
            template_2.save()
            # Create product input of the product_2's bom
            template_3, product_3 = create_product(
                'Product 3', account_category_goods, consumable=True)
            template_3.purchasable = True
            template_3.producible = False
            template_3.save()
            bom = _create_bom(product_1, product_2, unit)
            # Add another input to the product_2 bom
            bom_input = BOMInput()
            bom_input.product = product_3.id
            bom_input.quantity = Decimal('15')
            bom_input.uom = unit.id
            bom_input.bom = bom
            bom_input.save()
            # Create production with quantity different from 1
            production_qty_not_1 = create_production(
                3, product_1, unit, warehouse, production_loc, company, bom)
            # Create production with quantity 1
            production_qty_1 = create_production(
                1, product_1, unit, warehouse, production_loc, company, bom)
            inputs = production_qty_not_1.inputs + production_qty_1.inputs
            self.assertEqual(len(inputs), 4)
            outputs = production_qty_not_1.outputs + production_qty_1.outputs
            self.assertEqual(len(outputs), 2)
            # Create lots for each input and output
            i = 0
            inputs_outputs = inputs + outputs
            for production_move in inputs_outputs:
                lot_product = StockLot()
                lot_product.number = 'LOT000' + str(i)
                lot_product.product = production_move.product
                lot_product.save()
                production_move.lot = lot_product
                production_move.save()
                i += 1
            # Do productions
            production_qty_not_1.update_outputs_cost_price = True
            production_qty_not_1.save()
            production_qty_1.update_outputs_cost_price = True
            production_qty_1.save()
            Production.wait([production_qty_not_1, production_qty_1])
            self.assertEqual(production_qty_not_1.state, 'waiting')
            self.assertEqual(production_qty_1.state, 'waiting')
            Production.assign_try([production_qty_not_1, production_qty_1])
            self.assertEqual(production_qty_not_1.state, 'assigned')
            self.assertEqual(production_qty_1.state, 'assigned')
            Production.run([production_qty_not_1, production_qty_1])
            self.assertEqual(production_qty_not_1.state, 'running')
            self.assertEqual(production_qty_1.state, 'running')
            Production.done([production_qty_not_1, production_qty_1])
            self.assertEqual(production_qty_not_1.state, 'done')
            self.assertEqual(production_qty_1.state, 'done')
            # Create customer and carrier for the shipment
            party = Party(name='Customer')
            party.addresses = [{}]
            party.save()
            service_template, service = create_product('Service', account_category)
            service_template.purchasable = True
            service_template.save()
            carrier, = Carrier.create([{
                'party': party.id,
                'carrier_product': service.id,
                'carrier_cost_method': 'product'
            }])
            # Create customer shipment for production_qty_not_1
            shipment_out_qty_not_1 = ShipmentOut()
            shipment_out_qty_not_1.planned_date = Date.today()
            shipment_out_qty_not_1.customer = party
            shipment_out_qty_not_1.delivery_address, = party.addresses
            shipment_out_qty_not_1.warehouse = warehouse
            shipment_out_qty_not_1.warehouse_output = warehouse.output_location
            shipment_out_qty_not_1.warehouse_storage = warehouse.storage_location
            shipment_out_qty_not_1.carrier = carrier
            shipment_out_qty_not_1.save()
            # Create customer shipment for production_qty_1
            shipment_out_qty_1 = ShipmentOut()
            shipment_out_qty_1.planned_date = Date.today()
            shipment_out_qty_1.delivery_address, = party.addresses
            shipment_out_qty_1.customer = party
            shipment_out_qty_1.carrier = carrier
            shipment_out_qty_1.warehouse = warehouse
            shipment_out_qty_1.warehouse_output = warehouse.output_location
            shipment_out_qty_1.warehouse_storage = warehouse.storage_location
            shipment_out_qty_1.save()
            # Create inventory and outgoing moves from the inputs and outputs of the productions
            for _move in inputs_outputs:
                # Two moves, one for inventory and another for outgoing
                for i in range(0, 2):
                    move = Move()
                    move.product = _move.product
                    move.quantity = 1
                    move.unit_price = Decimal('1.00')
                    move.uom = unit
                    move.lot = _move.lot
                    move.from_location = warehouse.storage_location if i == 0 else warehouse.output_location
                    move.to_location = warehouse.output_location if i == 0 else warehouse.storage_location
                    if _move.production_input == production_qty_not_1 or _move.production_output == production_qty_not_1:
                        move.shipment = shipment_out_qty_not_1
                    else:
                        move.shipment = shipment_out_qty_1
                    move.save()
            self.assertEqual(len(shipment_out_qty_not_1.moves), 6)
            self.assertEqual(len(shipment_out_qty_not_1.inventory_moves), 3)
            self.assertEqual(len(shipment_out_qty_not_1.outgoing_moves), 3)
            self.assertEqual(len(shipment_out_qty_1.moves), 6)
            self.assertEqual(len(shipment_out_qty_1.inventory_moves), 3)
            self.assertEqual(len(shipment_out_qty_1.outgoing_moves), 3)
            # Execute add component breakdown wizard
            session_id, _, _ = WizardAddComponentBreakdown.create()
            wizard_add_component_breakdown = WizardAddComponentBreakdown(session_id)
            wizard_add_component_breakdown.records = [shipment_out_qty_not_1]
            # Assert an UserError is raised when the shipment is not in 
            # 'packed' state
            with self.assertRaises(UserError):
                wizard_add_component_breakdown.default_start(None)
            # Pack both shipments
            for shipment in [shipment_out_qty_not_1, shipment_out_qty_1]:
                ShipmentOut.wait([shipment])
                self.assertEqual(shipment.state, 'waiting')
                ShipmentOut.assign_try([shipment])
                self.assertEqual(shipment.state, 'assigned')
                ShipmentOut.pick([shipment])
                self.assertEqual(shipment.state, 'picked')
                ShipmentOut.pack([shipment])
                self.assertEqual(shipment.state, 'packed')
            # Try now if user error is not thrown
            wizard_add_component_breakdown.default_start(None)
            # Test that a Warning is thrown when a move has quantity 1
            # but his production output quantity is different from 1
            with self.assertRaises(UserWarning):
                # Select the move that has de production output
                moves_to_update = [m for m in shipment_out_qty_not_1.outgoing_moves
                                   if m.lot and m.lot.output_production]
                self.assertEqual(len(moves_to_update), 1)
                self.assertEqual(moves_to_update[0].quantity, 1)
                self.assertNotEqual(moves_to_update[0].lot.output_production.quantity, 1)
                wizard_add_component_breakdown.start.moves_to_update = moves_to_update
                wizard_add_component_breakdown.transition_update_component_breakdown()
            # Test Move Component Breakdown is updated
            session_id, _, _ = WizardAddComponentBreakdown.create()
            wizard_add_component_breakdown = WizardAddComponentBreakdown(session_id)
            wizard_add_component_breakdown.records = [shipment_out_qty_1]
            wizard_add_component_breakdown.default_start(None)
            # Create sequence for product_2
            sequence_type, = SequenceType.search(
                [('name', '=', "Stock Lot"), ], limit=1)
            sequence, = Sequence.create([{
                'name': 'Test Lots',
                'sequence_type': sequence_type.id,
                'prefix': 'EM',
                'suffix': '',
                'type': 'incremental',
            }])
            # Mark product_2 as serial_number so in the Component Breakdown 
            # text we will see the different type of lines that can be added
            product_2.template.serial_number = True
            product_2.template.lot_sequence = sequence
            product_2.template.save()
            # Select the move that has de production output and check that is valid to be updated
            moves_to_update = [m for m in shipment_out_qty_1.outgoing_moves
                               if m.lot and m.lot.output_production]
            self.assertEqual(len(moves_to_update), 1)
            move_to_update = moves_to_update[0]
            self.assertEqual(move_to_update.quantity, 1)
            self.assertEqual(move_to_update.lot.output_production.quantity, 1)
            wizard_add_component_breakdown.start.moves_to_update = moves_to_update
            self.assertEqual(move_to_update.component_breakdown, None)
            wizard_add_component_breakdown.transition_update_component_breakdown()
            # Test that the component_breakdown has been correctly generated
            customer_lang = move_to_update.shipment.customer.lang
            with Transaction().set_context(language=customer_lang.code if customer_lang else 'es'):
                component_breakdown = [gettext('electrans.each_unit_composed_of')]
            sorted_inputs = sorted(
                move_to_update.lot.output_production.inputs, key=lambda x: (x.product.code, x.quantity))
            for _input in sorted_inputs:
                if _input.product and _input.uom:
                    if _input.product.serial_number and _input.lot:
                        component_breakdown.append(str(_input.quantity) + ' ' + _input.uom.symbol + '  ' +
                                                   _input.product.rec_name + ' S/N: ' + _input.lot.rec_name)
                    else:
                        component_breakdown.append(str(_input.quantity) + ' ' + _input.uom.symbol +
                                                   '  ' + _input.product.rec_name)
            component_breakdown = '\n'.join(component_breakdown)
            self.assertEqual(move_to_update.component_breakdown, component_breakdown)

    @with_transaction()
    def test_get_last_line_unit_price(self):
        "Test Get Last Line Unit Price"
        pool = Pool()
        Location = pool.get('stock.location')
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        PurchaseLine = pool.get('purchase.line')
        ApprovalRequest = pool.get('approval.request')
        Purchase = pool.get('purchase.purchase')
        Tax = pool.get('account.tax')
        Journal = Pool().get('account.journal')
        Party = pool.get('party.party')
        ApprovalGroup = pool.get('approval.group')
        ProductUom = pool.get('product.uom')
        FiscalYear = pool.get('account.fiscalyear')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Get warehouses and production location
            production_loc, = Location.search([('code', '=', 'PROD')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.save()
            # Create Party
            party, = Party.create([{
                'name': 'Party',
                'addresses': [('create', [{}])],
            }])
            self.assertEqual(len(party.addresses), 1)
            party.addresses[0].invoice = True
            party.addresses[0].save()
            # Manually create fiscal year; create_fiscalyear_and_chart creates 
            # a fiscal year from the start to the end of the current year, as 
            # this test needs to define dates in the past, they could get 
            # excluded from the account period if "today" var is at the start 
            # of the year, so we define a fiscalyear where "today" is at the 
            # middle of it
            fiscalyear = set_invoice_sequences(get_fiscalyear(company))
            fiscalyear.start_date = today - datetime.timedelta(days=183)
            fiscalyear.end_date = today + datetime.timedelta(days=182)
            fiscalyear.save()
            FiscalYear.create_period([fiscalyear])
            # Create chart of accounts
            # By giving an existing fiscal year to this function, we prevent it
            # to create a new one
            create_fiscalyear_and_chart(company, fiscalyear, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            party_account, = Account.search([
                    ('type.payable', '=', True),
                    ('party_required', '=', True),
                    ])
            party.account_payable = party_account
            party.save()
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products
            template, product = create_product('Product', account_category_goods)
            template.producible = True
            template.purchasable = True
            template.save()
            # Get Journal
            journal, = Journal.search([('type', '=', 'expense')], limit=1)
            # Create invoice
            invoice1 = Invoice()
            invoice1.party = party
            invoice1.type = 'in'
            invoice1.account = party.account_payable
            invoice1.journal = journal
            invoice1.invoice_date = today - datetime.timedelta(days=10)
            invoice1.invoice_address = party.addresses[0]
            invoice1.save()
            # Create account for Invoice Lines
            account_type, = AccountType.search([('expense', '=', True)], limit=1)
            account_line = Account(name='Line Account', type=account_type, closed=False)
            invoice_line1 = InvoiceLine()
            invoice_line1.invoice = invoice1
            invoice_line1.party = party
            invoice_line1.account = account_line
            invoice_line1.product = product
            invoice_line1.quantity = 1
            invoice_line1.unit = unit
            invoice_line1.gross_unit_price = Decimal('10.0')
            invoice_line1.unit_price = Decimal('10.0')
            invoice_line1.taxes = [tax]
            invoice_line1.invoice_type = 'in'
            invoice_line1.save()
            # Create Approval Group
            approval_group = ApprovalGroup()
            approval_group.name = 'Approval Group 1'
            approval_group.users = tuple([Transaction().user])
            approval_group.save()
            # Create Purchase
            purchase1 = Purchase()
            purchase1.party = party.id
            purchase1.warehouse = warehouse
            purchase1.purchase_date = today - datetime.timedelta(days=10)
            purchase1.invoice_address = party.addresses[0]
            purchase1.approval_group = approval_group
            purchase1.save()
            # Create Purchase Line
            purchase_line1 = PurchaseLine()
            purchase_line1.purchase = purchase1
            purchase_line1.product = product
            purchase_line1.quantity = 1
            purchase_line1.unit = unit
            purchase_line1.gross_unit_price = Decimal('5.0')
            purchase_line1.unit_price = Decimal('5.0')
            purchase_line1.save()
            # Approve request and process Purchase
            self.assertEqual(purchase1.state, 'draft')
            Purchase.quote([purchase1])
            self.assertEqual(purchase1.state, 'quotation')
            self.assertEqual(len(purchase1.approval_requests), 1)
            self.assertEqual(purchase1.approval_requests[0].state, 'pending')
            ApprovalRequest.approve(purchase1.approval_requests)
            self.assertEqual(purchase1.approval_requests[0].state, 'approved')
            Purchase.confirm([purchase1])
            self.assertEqual(purchase1.state, 'confirmed')
            Purchase.process([purchase1])
            self.assertEqual(purchase1.state, 'processing')
            # Get last line, invoice and purchase date are the same but invoice
            # is not posted
            last_line = PurchaseLine.get_last_line(product, party)
            self.assertEqual(last_line.unit_price, purchase_line1.unit_price)
            # Invoice and purchase date are the same but invoice now is posted
            self.assertEqual(invoice1.state, 'draft')
            Invoice.post([invoice1])
            self.assertEqual(invoice1.state, 'posted')
            last_line = PurchaseLine.get_last_line(product, party)
            self.assertEqual(last_line.unit_price, invoice_line1.unit_price)
            # Purchase date is more recent than the Invoice Date
            purchase1.purchase_date = today - datetime.timedelta(days=5)
            purchase1.save()
            last_line = PurchaseLine.get_last_line(product, party)
            self.assertEqual(last_line.unit_price, purchase_line1.unit_price)
            # Invoice date (today - 10 days) is more recent than the Purchase 
            # Date
            purchase1.purchase_date = today - datetime.timedelta(days=15)
            purchase1.save()
            last_line = PurchaseLine.get_last_line(product, party)
            self.assertEqual(last_line.unit_price, invoice_line1.unit_price)

    @with_transaction()
    def test_production_split(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Operation = pool.get('production.operation')
        OperationType = pool.get('production.operation.type')
        WorkCenterCategory = pool.get('production.work_center.category')
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        # Create Company
        company = create_company()
        with set_company(company):
            production_loc, = Location.search([('code', '=', 'PROD')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category= create_account_category(tax, expense, revenue)
            # Save UOMs
            unit, = Uom.search([('name', '=', 'Unit')])
            hour, = Uom.search([('name', '=', 'Hour')])
            # Create products
            template, product = create_product('Product', account_category)
            template.producible = True
            template.save()
            template1, product1, = create_product('Product1', account_category)
            template2, product2, = create_product('Product2', account_category)
            # Create bom
            bom = _create_bom(product1, product2, unit)
            # Create production
            production = create_production(
                25, product, unit, warehouse, production_loc, company, bom)
            # Create work center and operations
            work_center_category, = WorkCenterCategory.create([{
                'name': 'test_work_center_category',
                'cost_price': Decimal(30),
                'type': 'employee',
                'active': True,
            }])
            operation_type, = OperationType.create([{
                'name': 'test_operation_type',
            }])
            operation, = Operation.create([{
                'production': production.id,
                'operation_type': operation_type.id,
                'work_center_category': work_center_category.id,
                'lines': [('create', [{
                    'quantity': 25,
                    'uom': hour.id,
                }])],
            }])
            # Split production with 25 units and total_labor_time=25h in
            # productions of 10 units
            productions = production.split(10, unit)

            # Assert there's 3 productions created
            self.assertEqual(len(productions), 3)

            # Assert productions quantity is splitted correctly
            self.assertEqual([p.quantity for p in productions], [10, 10, 5])

            # Assert productions total_labor_time is splitted correctly
            self.assertEqual(productions[0].total_labor_time,
                             datetime.timedelta(hours=10))
            self.assertEqual(productions[1].total_labor_time,
                             datetime.timedelta(hours=10))
            self.assertEqual(productions[2].total_labor_time,
                             datetime.timedelta(hours=5))

    @with_transaction()
    def test_purchase_request_supplier_price(self):
        """
        Test purchase product prices when creating purchases with
        'create purchase' wizard or lines with 'add to purchase' wizard
        """
        pool = Pool()
        Date = pool.get('ir.date')
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        Purchase = pool.get('purchase.purchase')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequestQuotation = pool.get('purchase.request.quotation')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        Warning = pool.get('res.user.warning')
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        WizardHandleException = pool.get(
            'purchase.request.handle.purchase.cancellation', type='wizard')
        WizardAddToPurchase = pool.get(
            'purchase.request.add_to_purchase', type='wizard')
        today = Date.today()
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create products
            template, product = create_product('P001', account_category)
            template.purchasable = True
            template.save()
            template_2, product_2 = create_product('P002', account_category)
            template_2.purchasable = True
            template_2.save()
            # Create employee
            employee1 = create_employee(company, 'employee1')
            # Create suppliers
            supplier_1, = Party.create([{
                'name': 'Supplier 1',
                'addresses': [('create', [{}])],
            }])
            supplier_2, = Party.create([{
                'name': 'Supplier 2',
                'addresses': [('create', [{}])],
            }])
            # Create Purchase Requisition
            requisition1, = PurchaseRequisition.create([{
                'number': 'PET00001',
                'supply_date': today,
                'employee': employee1.id,
                'state': 'draft',
            }])
            # Create Purchase Requisition Lines
            req_line1, = PurchaseRequisitionLine.create([{
                'product': product.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            req_line2, = PurchaseRequisitionLine.create([{
                'product': product_2.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            # PurchaseRequisition.wait() process the requisition
            PurchaseRequisition.wait([requisition1])

            # Save first created request
            request, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line1.id)])

            # Assert the supplier is dragged to the purchase request
            self.assertEqual(request.party, req_line1.supplier)

            # Modify purchase request supplier
            request.party = supplier_2.id
            request.save()

            # Execute create purchase wizard
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request]
            wizard_create_purchase.model = PurchaseRequest
            # Assert a warning is being raised when trying to create purchase
            # because of the party modification
            try:
                wizard_create_purchase.transition_start()
                self.fail("UserWarning not raised")
            except UserWarning as warning:
                _, (key, *_) = warning.args

            # Save the warning key to avoid an infinite warning raise
            Warning(user=Transaction().user, name=key).save()
            wizard_create_purchase.transition_start()

            # Save purchase
            purchase1 = request.purchase
            line1 = request.purchase_line

            # Assert purchase supplier matches request supplier
            self.assertEqual(purchase1.party, request.party)

            # Assert product's price is set to 0 (Because party has been
            # modified and there's no preferred_quotation_line defined)
            self.assertEqual(line1.unit_price, Decimal('0'))

            # Cancel purchase to execute the wizard again
            Purchase.cancel([purchase1])
            self.assertEqual(purchase1.state, 'cancelled')
            self.assertEqual(request.state, 'exception')

            # Handle purchase request exception
            session_id, _, _ = WizardHandleException.create()
            wizard_handle_exception = WizardHandleException(session_id)
            wizard_handle_exception.records = [request]
            wizard_handle_exception.model = PurchaseRequest
            wizard_handle_exception.transition_reset()
            self.assertEqual(request.state, 'draft')
            self.assertIsNone(request.purchase)

            # Create quotation
            request_quotation, = PurchaseRequestQuotation.create([{
                'supplier': supplier_2.id,
                'state': 'draft',
                'lines': [('create', [{
                    'request': request.id,
                    'quantity': Decimal('1'),
                    'unit': unit.id,
                    'unit_price': Decimal('5'),
                    'currency': company.currency.id,
                }])],
            }])

            # Send and receive the previous created quotation
            PurchaseRequestQuotation.send([request_quotation])
            PurchaseRequestQuotation.receive([request_quotation])

            # Define a preferred_quotation_line in the request
            request.preferred_quotation_line = request_quotation.lines[0]
            request.save()

            # Execute 'create purchase' wizard again to assert the warning is
            # not being raised with a preferred quotation line selected
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request]
            wizard_create_purchase.model = PurchaseRequest
            wizard_create_purchase.transition_start()

            # Save purchase
            purchase1 = request.purchase
            line1 = purchase1.lines[0]

            # Assert purchase supplier matches preferred quotation supplier
            self.assertEqual(purchase1.party, request_quotation.supplier)

            # Assert product's price matches preferred quotation price
            self.assertEqual(
                line1.unit_price, request_quotation.lines[0].unit_price)

            # Save second created request by processing the requisition
            request_2, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line2.id)])

            # Modify its supplier
            request_2.party = supplier_2
            request.save()

            # Create quotation
            request_quotation_2, = PurchaseRequestQuotation.create([{
                'supplier': supplier_2.id,
                'state': 'draft',
                'lines': [('create', [{
                    'request': request_2.id,
                    'quantity': Decimal('1'),
                    'unit': unit.id,
                    'unit_price': Decimal('10'),
                    'currency': company.currency.id,
                }])],
            }])
            # Add request to purchase
            session_id, _, _ = WizardAddToPurchase.create()
            wizard_add_to_purchase = WizardAddToPurchase(session_id)
            wizard_add_to_purchase.records = [request_2]
            wizard_add_to_purchase.model = PurchaseRequest
            wizard_add_to_purchase.transition_start()
            wizard_add_to_purchase.ask_purchase.party = request_2.party
            wizard_add_to_purchase.ask_purchase.purchase = purchase1

            # Assert a warning is being raised by the party modification 
            # without defining a preferred quotation line
            self.assertRaises(
                UserWarning, wizard_add_to_purchase._execute, 'create_')

            # Unlinks request to purchase line in order to execute the
            # wizard again
            request_2.purchase_line = None
            request_2.save()

            # Send and receive the previous created quotation
            PurchaseRequestQuotation.send([request_quotation_2])
            PurchaseRequestQuotation.receive([request_quotation_2])

            # Define a preferred_quotation_line in the request
            request_2.preferred_quotation_line = request_quotation_2.lines[0]
            request_2.save()

            # Assert the warning is not being raised after defining a
            # preferred_quotation_line
            wizard_add_to_purchase._execute('create_')

            # Save new generated line
            line_2 = request_2.purchase_line

            # Assert product's price matches preferred quotation price
            self.assertEqual(
                line_2.unit_price, request_quotation_2.lines[0].unit_price)

    @with_transaction()
    def test_production_clean_lots(self):
        """Test clean lots function"""
        pool = Pool()
        Production = pool.get('production')
        ProductUom = pool.get('product.uom')
        Location = pool.get('stock.location')
        Lot = pool.get('stock.lot')
        Inventory = pool.get('stock.inventory')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        # Create Company
        company = create_company()
        with set_company(company):
            production_loc, = Location.search([('code', '=', 'PROD')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            template, product = create_product(
                'Product', account_category, consumable=False)
            template.producible = True
            template.purchasable = True
            template.save()
            template2, product2, = create_product(
                'Product2', account_category, consumable=True)
            template2.producible = True
            template2.save()
            bom = _create_bom(
                out_product=product, in_product=product2, unit=unit)
            bom.save()
            production = create_production(
                quantity=25, product=product, unit=unit, bom=bom,
                warehouse=warehouse, production_loc=production_loc,
                company=company)
            Production.wait([production])
            self.assertEqual(production.state, 'waiting')
            # Create lots
            lot1, lot2 = Lot.create([{
                'number': '01',
                'product': product.id,
            }, {
                'number': '02',
                'product': product2.id,
            }])
            # Create stock to assign productions
            inventory, = Inventory.create([{
                'company': company.id,
                'location': warehouse.storage_location.id,
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': 50,
                    'lot': lot1.id,
                }, {
                    'product': product2.id,
                    'quantity': 50,
                    'lot': lot2.id,
                }])],
            }])
            Inventory.confirm([inventory])
            # Production workflow
            Production.assign_try([production])
            self.assertEqual(production.state, 'assigned')
            # Assert lots are generated
            for production_input in production.inputs:
                self.assertIsNotNone(production_input.lot)
            # Back to draft to clean lots
            Production.wait([production])
            self.assertEqual(production.state, 'waiting')
            Production.draft([production])
            self.assertEqual(production.state, 'draft')
            Production.clean_lots([production])
            # Assert lots are removed
            for production_input in production.inputs:
                self.assertIsNone(production_input.lot)
            # Generate lots again
            Production.wait([production])
            self.assertEqual(production.state, 'waiting')
            Production.assign_try([production])
            self.assertEqual(production.state, 'assigned')
            for production_input in production.inputs:
                self.assertIsNotNone(production_input.lot)
                self.assertEqual(production_input.state, 'assigned')
            # Force 'draft' state to keep the 'assigned' state on inputs, to
            # test if clean_lots() also ignores and warns about assigned moves
            production.state = 'draft'
            # Assert a warning is being raised for assigned inputs
            # (those inputs should keep their lots)
            self.assertRaises(UserWarning, Production.clean_lots, [production])
            # Assert lots are not removed
            for production_input in production.inputs:
                self.assertIsNotNone(production_input.lot)
            # Assign back the production
            production.state = 'assigned'
            production.save()
            # Run production
            Production.run([production])
            self.assertEqual(production.state, 'running')
            # Assert inputs are done
            for production_input in production.inputs:
                self.assertEqual(production_input.state, 'done')
            # Back to draft again to try to clean_lots
            Production.wait([production])
            self.assertEqual(production.state, 'waiting')
            Production.draft([production])
            self.assertEqual(production.state, 'draft')
            # Assert a warning is being raised for finished inputs
            # (those inputs should also keep their lots)
            self.assertRaises(UserWarning, Production.clean_lots, [production])
            # Assert lots are not removed
            for production_input in production.inputs:
                self.assertIsNotNone(production_input.lot)

    @with_transaction()
    def test_request_quotation_product_to_request(self):
        """
        Test purchase line is correctly created when creating/adding to
        purchase a request with 'product to request' defined in quotation
        """
        pool = Pool()
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequestQuotation = pool.get('purchase.request.quotation')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        WizardAddToPurchase = pool.get(
            'purchase.request.add_to_purchase', type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Create products
            template, product = create_product('P001', account_category)
            template.purchasable = True
            template.description = '[P001] Product 1'
            template.save()
            template_2, product_2 = create_product('P002', account_category)
            template_2.purchasable = True
            template_2.description = '[P002] Product 2'
            template_2.save()
            # Create employee
            employee1 = create_employee(company, 'employee1')
            # Create suppliers
            supplier_1, = Party.create([{
                'name': 'Supplier 1',
                'addresses': [('create', [{}])],
            }])
            # Create Purchase Requisition
            requisition1, = PurchaseRequisition.create([{
                'number': 'PET00001',
                'supply_date': today,
                'employee': employee1.id,
                'state': 'draft',
            }])
            # Create Purchase Requisition Lines
            req_line1, = PurchaseRequisitionLine.create([{
                'product': product.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            req_line2, = PurchaseRequisitionLine.create([{
                'product': product_2.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            # PurchaseRequisition.wait() process the requisition
            PurchaseRequisition.wait([requisition1])

            # Save first generated request
            request, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line1.id)])

            # Create quotation for the first request
            request_quotation, = PurchaseRequestQuotation.create([{
                'supplier': supplier_1.id,
                'state': 'draft',
                'lines': [('create', [{
                    'product_to_request': product_2.id,
                    'request': request.id,
                    'quantity': Decimal('1'),
                    'unit': unit.id,
                    'unit_price': Decimal('5'),
                    'currency': company.currency.id,
                }])],
            }])
            # Send and receive request quotation
            PurchaseRequestQuotation.send([request_quotation])
            PurchaseRequestQuotation.receive([request_quotation])

            # Save request quotation line
            qline = request_quotation.lines[0]

            # Select the preferred_quotation_line in the request
            request.preferred_quotation_line = qline
            request.save()

            # Execute create purchase wizard
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request]
            wizard_create_purchase.model = PurchaseRequest
            wizard_create_purchase.transition_start()

            # Save purchase
            purchase1 = request.purchase
            line1 = purchase1.lines[0]

            # Assert the product_to_request from the quotation is the one
            # dragged to the purchase line
            self.assertEqual(line1.product, qline.product_to_request)

            # Assert the rest of the fields matches the selected request
            # quotation line values
            self.assertEqual(line1.description, qline.description)
            self.assertEqual(line1.unit_price, qline.unit_price)
            self.assertEqual(line1.discount, qline.discount)
            self.assertEqual(line1.quantity, qline.quantity)

            # Save second generated request
            request_2, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line2.id)])

            # Create quotation for the second request
            request_quotation_2, = PurchaseRequestQuotation.create([{
                'supplier': supplier_1.id,
                'state': 'draft',
                'lines': [('create', [{
                    'product_to_request': product.id,
                    'request': request_2.id,
                    'quantity': Decimal('1'),
                    'unit': unit.id,
                    'unit_price': Decimal('5'),
                    'currency': company.currency.id,
                }])],
            }])

            # Send and receive request quotation
            PurchaseRequestQuotation.send([request_quotation_2])
            PurchaseRequestQuotation.receive([request_quotation_2])

            # Save request quotation line
            qline_2 = request_quotation_2.lines[0]

            # Select the preferred_quotation_line in the request
            request_2.preferred_quotation_line = qline_2
            request_2.save()

            # Execute add to purchase wizard
            session_id, _, _ = WizardAddToPurchase.create()
            wizard_add_to_purchase = WizardAddToPurchase(session_id)
            wizard_add_to_purchase.records = [request_2]
            wizard_add_to_purchase.model = PurchaseRequest
            wizard_add_to_purchase.transition_start()
            wizard_add_to_purchase.ask_purchase.party = request_2.party
            wizard_add_to_purchase.ask_purchase.purchase = purchase1
            wizard_add_to_purchase._execute('create_')

            # Save new purchase line
            line2 = request_2.purchase_line

            # Assert the product_to_request from the quotation is the one
            # dragged to the purchase line
            self.assertEqual(line2.product, qline_2.product_to_request)

            # Assert the rest of the fields matches the selected request
            # quotation line values
            self.assertEqual(line2.description, qline_2.description)
            self.assertEqual(line2.unit_price, qline_2.unit_price)
            self.assertEqual(line2.discount, qline_2.discount)
            self.assertEqual(line2.quantity, qline_2.quantity)

    @with_transaction()
    def test_purchase_requisition_attachment_drag(self):
        """
        Test attachments from purchase requisition are being dragged to 
        purchase request and from there to the purchase
        """
        pool = Pool()
        Attachment = pool.get('ir.attachment')
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        Purchase = pool.get('purchase.purchase')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        WizardCreatePurchase = pool.get(
            'purchase.request.create_purchase', type='wizard')
        WizardAddToPurchase = pool.get(
            'purchase.request.add_to_purchase', type='wizard')
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Get warehouse
            warehouse, = Location.search([('code', '=', 'WH')])
            # Create products
            template, product = create_product('P001', account_category)
            template.purchasable = True
            template.description = '[P001] Product 1'
            template.save()
            template_2, product_2 = create_product('P002', account_category)
            template_2.purchasable = True
            template_2.description = '[P002] Product 2'
            template_2.save()
            # Create employee
            employee1 = create_employee(company, 'employee1')
            # Create supplier
            supplier_1, = Party.create([{
                'name': 'Supplier 1',
                'addresses': [('create', [{}])],
            }])
            # CASE 1: Create purchase
            # Create Purchase Requisition
            requisition1, = PurchaseRequisition.create([{
                'number': 'PET00001',
                'supply_date': today,
                'employee': employee1.id,
                'state': 'draft',
            }])
            # Create Purchase Requisition Lines
            req_line1, = PurchaseRequisitionLine.create([{
                'product': product.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            req_line2, = PurchaseRequisitionLine.create([{
                'product': product_2.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition1.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            # Create attachment for  purchase requisition
            attachment, = Attachment.create([{
                'name':'attachment1.txt',
                'type': 'data',
                'resource': requisition1,
            }])
            # PurchaseRequisition.wait() process the requisition
            PurchaseRequisition.wait([requisition1])

            # Save first generated request and its attachments
            request_1, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line1.id)])
            attachments_req1 = getattr(request_1, 'attachments')
            
            # Save second generated request and its attachments
            request_2, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line2.id)])
            attachments_req2 = getattr(request_2, 'attachments')
            
            # Assert both requests have the same attachments
            self.assertIsNotNone(attachments_req1)
            self.assertEqual(attachments_req1[0].name, attachment.name)

            self.assertIsNotNone(attachments_req2)
            self.assertEqual(attachments_req2[0].name, attachment.name)
            
            # Execute create purchase wizard
            session_id, _, _ = WizardCreatePurchase.create()
            wizard_create_purchase = WizardCreatePurchase(session_id)
            wizard_create_purchase.records = [request_1, request_2]
            wizard_create_purchase.model = PurchaseRequest
            wizard_create_purchase.transition_start()
            
            # Save purchase and its attachments
            purchase = request_1.purchase
            attachments_purchase = getattr(purchase, 'attachments')

            # Assert the purchase have the same attachment, and its not 
            # duplicated
            self.assertIsNotNone(attachments_purchase)
            self.assertEqual(attachments_purchase[0].name, attachment.name)
            self.assertEqual(len(attachments_purchase), 1)

            # CASE 2: Add to purchase
            # Create Purchase Requisition
            new_purchase, = Purchase.create([{
                'party': supplier_1.id,
                'warehouse': warehouse.id
            }])
            requisition2, = PurchaseRequisition.create([{
                'number': 'PET00002',
                'supply_date': today,
                'employee': employee1.id,
                'state': 'draft',
            }])
            # Create Purchase Requisition Lines
            req_line21, = PurchaseRequisitionLine.create([{
                'product': product.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition2.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            req_line22, = PurchaseRequisitionLine.create([{
                'product': product_2.id,
                'quantity': 1.00,
                'unit_price': Decimal('1'),
                'requisition': requisition2.id,
                'supplier': supplier_1.id,
                'unit': unit.id,
            }])
            # Create attachment for  purchase requisition
            attachment2, = Attachment.create([{
                'name':'attachment2.txt',
                'type': 'data',
                'resource': requisition2,
            }])
            # PurchaseRequisition.wait() process the requisition
            PurchaseRequisition.wait([requisition2])

            # Save first generated request and its attachments
            request_21, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line21.id)])
            attachments_req1 = getattr(request_21, 'attachments')
            
            # Save second generated request and its attachments
            request_22, = PurchaseRequest.search([
                ('origin', 'like', 'purchase.requisition.line,%d'
                 % req_line22.id)])
            attachments_req2 = getattr(request_22, 'attachments')

            # Execute add to purchase wizard
            session_id, _, _ = WizardAddToPurchase.create()
            wizard_add_to_purchase = WizardAddToPurchase(session_id)
            wizard_add_to_purchase.records = [request_21, request_22]
            wizard_add_to_purchase.record = request_21
            wizard_add_to_purchase.model = PurchaseRequest
            wizard_add_to_purchase.transition_start()
            wizard_add_to_purchase.ask_purchase.party = request_21.party
            wizard_add_to_purchase.ask_purchase.purchase = new_purchase
            wizard_add_to_purchase._execute('create_')
            
            # Save new purchase attachments
            attachments_new_purchase = getattr(new_purchase, 'attachments')

            # Assert the purchase have the same attachment, and its not 
            # duplicated
            self.assertIsNotNone(attachments_new_purchase)
            self.assertEqual(attachments_new_purchase[0].name, attachment2.name)
            self.assertEqual(len(attachments_new_purchase), 1)

    @with_transaction()
    def test_move_assign_sort_quantities(self):
        """
        Test Move.assign_try() assign lots in FIFO
        """
        pool = Pool()
        Inventory = pool.get('stock.inventory')
        Location = pool.get('stock.location')
        Lot = pool.get('stock.lot')
        Move = pool.get('stock.move')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])

        # Create Company
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            # Define Locations
            warehouse, = Location.search([('code', '=', 'WH')])
            storage, = Location.search([('code', '=', 'STO')])
            storage_view, = Location.create([{
                    'name': 'Storage View 2',
                    'code': 'STO_2',
                    'type': 'view',
                    'parent': warehouse.id,
                    }])
            storage_internal_1, = Location.create([{
                    'name': 'Storage Internal 2.1',
                    'code': 'STO_2_1',
                    'type': 'storage',
                    'parent': storage_view.id,
                    }])
            storage_internal_2, = Location.create([{
                    'name': 'Storage Internal 2.2',
                    'code': 'STO_2_2',
                    'type': 'storage',
                    'parent': storage_view.id,
                    }])
            # Create product
            _, product = create_product('Product', account_category)
            # Create moves
            moves = Move.create([{
                'product': product.id,
                'uom': unit.id,
                'quantity': 5,
                'from_location': storage_view.id,
                'to_location': storage.id,
                'unit_price': Decimal('1'),
            }, {
                'product': product.id,
                'uom': unit.id,
                'quantity': 5,
                'from_location': storage_view.id,
                'to_location': storage.id,
                'unit_price': Decimal('1'),
            }, {
                'product': product.id,
                'uom': unit.id,
                'quantity': 5,
                'from_location': storage_view.id,
                'to_location': storage.id,
                'unit_price': Decimal('1'),
            }, {
                'product': product.id,
                'uom': unit.id,
                'quantity': 5,
                'from_location': storage_view.id,
                'to_location': storage.id,
                'unit_price': Decimal('1'),
            }])
            # Assert moves can't be assigned before having stock
            self.assertFalse(Move.assign_try(moves))
            # Create lots
            lot_1, lot_2, lot_3 = Lot.create([{
                'number': '01',
                'product': product.id,
            }, {
                'number': '02',
                'product': product.id,
            }, {
                'number': '03',
                'product': product.id,
            }])
            # Create stock in different child locations
            inventories = Inventory.create([{
                'company': company.id,
                'location': storage_internal_1.id,
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': 10,
                    'lot': lot_1.id,
                }, {
                    'product': product.id,
                    'quantity': 5,
                    'lot': lot_2.id,
                }])],
            }, {
                'company': company.id,
                'location': storage_internal_2.id,
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': 5,
                    'lot': lot_3.id,
                }])],
            }])
            # Confirm inventory
            Inventory.confirm(inventories)

            # Assert moves can now be assigned, lots are assigned in the order
            # they have been created, and 'from_location' is correctly updated
            move_1, move_2, move_3, move_4 = moves
            self.assertTrue(Move.assign_try(moves))
            self.assertEqual(move_1.lot, lot_1)
            self.assertEqual(move_1.from_location, storage_internal_1)
            # As all moves have qty=5 and I've created 10 units of stock for
            # 'lot_1', 'move_2' gets the same lot as 'move_1'
            self.assertEqual(move_2.lot, lot_1)
            self.assertEqual(move_2.from_location, storage_internal_1)
            self.assertEqual(move_3.lot, lot_2)
            self.assertEqual(move_3.from_location, storage_internal_1)
            self.assertEqual(move_4.lot, lot_3)
            self.assertEqual(move_4.from_location, storage_internal_2)


del ModuleTestCase
