import datetime
from decimal import Decimal
from trytond.pool import Pool
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.account.tests import create_chart, get_fiscalyear
from trytond.modules.account_invoice.tests import set_invoice_sequences

today = datetime.date.today()


def create_fiscalyear_and_chart(company=None, fiscalyear=None,
                                chart=True):
    'Test fiscalyear'
    pool = Pool()
    FiscalYear = pool.get('account.fiscalyear')
    if not company:
        company = create_company()
    with set_company(company):
        if chart:
            create_chart(company, tax=True)
        if not fiscalyear:
            fiscalyear = set_invoice_sequences(get_fiscalyear(company))
            fiscalyear.save()
            FiscalYear.create_period([fiscalyear])
            assert len(fiscalyear.periods) == 12
        return fiscalyear


def get_accounts(company):
    pool = Pool()
    Account = pool.get('account.account')
    accounts = {}
    accounts['revenue'] = Account.search([
        ('type.revenue', '=', True),
        ('company', '=', company.id),
    ])[0]
    accounts['expense'] = Account.search([
        ('type.expense', '=', True),
        ('company', '=', company.id),
    ])[0]

    root, = Account.search([
        ('parent', '=', None),
        ('company', '=', company.id),
    ], limit=1)
    accounts['root'] = root
    tax, = Account.search([
        ('name', '=', 'Main Tax'),
        ('company', '=', company.id),
    ], limit=1)
    accounts['tax'] = tax
    views = Account.search([
        ('name', '=', 'View'),
        ('company', '=', company.id),
    ], limit=1)
    if views:
        view, = views
    else:
        with set_company(company):
            view, = Account.create([{
                'name': 'View',
                'code': '1',
                'parent': root.id,
                'company': company.id,
            }])
    accounts['view'] = view
    return accounts


def create_production(quantity, product, unit, warehouse,
                      production_loc, company, bom=None, component2=None):
    pool = Pool()
    Production = pool.get('production')
    production, = Production.create([{
        'product': product.id,
        'subcontract_product': component2.id if component2 else product.id if not bom else None,
        'bom': bom.id if bom else None,
        'uom': unit.id,
        'quantity': quantity,
        'warehouse': warehouse.id,
        'location': production_loc.id,
        'company': company.id,
        'planned_date': today,
        'planned_start_date': today,
        'state': 'draft',
    }])
    if bom:
        production.set_moves()
    return production


def create_account_category(tax, expense, revenue, name="Bienes"):
    pool = Pool()
    ProductCategory = pool.get('product.category')
    account_category = ProductCategory(name=name)
    account_category.accounting = True
    account_category.customer_taxes = [tax]
    account_category.account_expense = expense
    account_category.account_revenue = revenue
    account_category.save()
    return account_category


def create_sequence(name, type_name, prefix=None, suffix=None,
                    type_sequence=None):
    pool = Pool()
    Sequence = pool.get('ir.sequence')
    SequenceType = pool.get('ir.sequence.type')
    sequence_type, = SequenceType.search([('name', '=', type_name)],
                                         limit=1)
    sequence, = Sequence.create([{
        'name': name,
        'sequence_type': sequence_type.id,
        'prefix': prefix if prefix else 'AA',
        'suffix': suffix if suffix else None,
        'type': type_sequence if type_sequence else 'incremental',
    }])

    return sequence


def get_sequence(name, type_name=None):
    pool = Pool()
    Sequence = pool.get('ir.sequence')
    sequence = Sequence.search([('name', '=', name)])
    if not sequence and type_name:
        new_sequence = create_sequence(name, type_name)
        sequence.append(new_sequence)

    return sequence[0].get() if sequence else None


def create_template(name, account_category=None, consumable=None,
                    sale_uom=None):
    pool = Pool()
    ProductUom = pool.get('product.uom')
    unit, = ProductUom.search([('name', '=', 'Unit')])
    ProductTemplate = pool.get('product.template')
    Tax = pool.get('account.tax')
    company = create_company()
    with set_company(company):
        create_chart(company, tax=True)
        accounts = get_accounts(company)
        revenue = accounts['revenue']
        expense = accounts['expense']
        tax, = Tax.search([], limit=1)
        template = ProductTemplate()
        template.code = get_sequence('Product Sequence', 'Product')
        template.name = name
        template.type = 'service' if account_category \
                                     and account_category.name == "Servicios" else 'goods'
        template.purchase_uom = unit
        template.default_uom = unit
        template.sale_uom = sale_uom if sale_uom else None
        template.cost_price_method = 'fixed'
        template.list_price = Decimal(30)
        template.account_category = account_category if account_category \
            else create_account_category(tax, expense, revenue)
        template.consumable = consumable if consumable else None
        template.save()

    return template


def create_product(name, account_category, consumable=None):
    pool = Pool()
    Product = pool.get('product.product')
    template = create_template(name, account_category, consumable)

    product,  = Product.create([{
        'template': template.id,
        'cost_price': Decimal(1),
    }])

    return template, product


def _create_bom(out_product, in_product, unit):
    pool = Pool()
    BOM = pool.get('production.bom')
    BOMInput = pool.get('production.bom.input')
    BOMOutput= pool.get('production.bom.output')
    bom = BOM()
    bom.name = 'Product'
    bom.start_date = today

    bom_input = BOMInput()
    bom_input.product = in_product.id
    bom_input.quantity = Decimal('1')
    bom_input.uom = unit.id

    bom_output = BOMOutput()
    bom_output.product = out_product.id
    bom_output.quantity = Decimal('1')
    bom_output.uom = unit.id

    bom.inputs = [bom_input]
    bom.outputs = [bom_output]

    return bom

def create_supplier_warehouse():
    pool = Pool()
    Location = pool.get('stock.location')
    supplier_storage = Location(name='Supplier Storage', code='STO3',
                                type='storage')
    supplier_storage.save()
    supplier_input = Location(name='Supplier Input', type='storage',
                              code='IN2')
    supplier_input.save()
    supplier_output = Location(name='Supplier Output', type='storage',
                               code='OUT2')
    supplier_output.save()
    supplier_lost_found = Location(name='Supplier Lost Foud',
                                   code='LOST2', type='lost_found')
    supplier_lost_found.save()
    supplier_production = Location(name='Supplier Production',
                                   code='PROD2', type='production')
    supplier_production.save()
    supplier_warehouse = Location()
    supplier_warehouse.type = 'warehouse'
    supplier_warehouse.name = 'Supplier Warehouse'
    supplier_warehouse.code = 'WH SUPPLIER'
    supplier_warehouse.storage_location = supplier_storage
    supplier_warehouse.input_location = supplier_input
    supplier_warehouse.output_location = supplier_output
    supplier_warehouse.lost_found_location = supplier_lost_found
    supplier_warehouse.production_location = supplier_production
    supplier_warehouse.save()

    return supplier_warehouse
