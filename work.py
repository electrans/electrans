# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class ProjectExpense(metaclass=PoolMeta):
    __name__ = 'project.work.expense'

    @classmethod
    def __setup__(cls):
        super(ProjectExpense, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
            ('description', 'ASC')]
